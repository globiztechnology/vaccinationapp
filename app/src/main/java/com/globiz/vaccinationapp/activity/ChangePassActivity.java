package com.globiz.vaccinationapp.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.globiz.vaccinationapp.R;
import com.globiz.vaccinationapp.controller.AppController;
import com.globiz.vaccinationapp.util.Constant;
import com.globiz.vaccinationapp.util.Functions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ChangePassActivity extends AppCompatActivity implements OnClickListener {
    /* access modifiers changed from: private */
    public String email;
    private EditText emailET;
    private Button forgetPassBT;
    /* access modifiers changed from: private */
    public ProgressDialog loading;
    /* access modifiers changed from: private */
    public JSONObject object;
    Toolbar toolbar;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_change_pass);
        initViews();
    }

    private void initViews() {
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(this.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle((CharSequence) "Forgot Password");
        this.toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        this.emailET = (EditText) findViewById(R.id.txtEmail);
        this.forgetPassBT = (Button) findViewById(R.id.forgetBt);
        this.forgetPassBT.setOnClickListener(this);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != 16908332) {
            return super.onOptionsItemSelected(item);
        }
        finish();
        return true;
    }

    public void onClick(View v) {
        if (this.emailET.getText().toString().isEmpty()) {
            Functions.setErrorEditText(this.emailET, "Please enter email id");
        } else if (!Functions.isValidEmail(this.emailET.getText().toString())) {
            Functions.setErrorEditText(this.emailET, "Invalid Email");
        } else if (Functions.isNetworkConnected(ChangePassActivity.this)) {
//            AlertDialog.Builder builder = new AlertDialog.Builder(this);
//            builder.setTitle((CharSequence) "Forgot Password");
//          builder.setMessage("");
//            builder.setPositiveButton((int) R.string.goToMail, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int id) {
//                }
//            });
//            builder.setNegativeButton((int) R.string.ok, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int id) {
//                    if (Functions.isNetworkConnected(ChangePassActivity.this)) {
//                        ChangePassActivity.this.forgotPassword();
//                    } else {
//                        Toast.makeText(ChangePassActivity.this, R.string.check_your_internet_connectivity, Toast.LENGTH_SHORT).show();
//                    }
//                }
//            });
//            builder.show();
            ChangePassActivity.this.forgotPassword();
        } else {
            Toast.makeText(ChangePassActivity.this, R.string.check_your_internet_connectivity, Toast.LENGTH_SHORT).show();
        }

    }

    /* access modifiers changed from: private */
    public void forgotPassword() {
        this.email = this.emailET.getText().toString().trim();
        new AsyncTask<String, String, JSONObject>() {
            /* access modifiers changed from: protected */
            public JSONObject doInBackground(String... params) {
                ChangePassActivity.this.object = new JSONObject();
                JSONArray jsonArray = new JSONArray();
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("email", ChangePassActivity.this.email);
                    jsonArray.put(jsonObject);
                    ChangePassActivity.this.object.put("user", jsonArray);
                    Log.e("response", ChangePassActivity.this.object.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return ChangePassActivity.this.object;
            }

            /* access modifiers changed from: protected */
            public void onPreExecute() {
                super.onPreExecute();
//                ChangePassActivity.this.loading = ProgressDialog.show(ChangePassActivity.this, "", "Please wait...", false, false);
                loading = new ProgressDialog(ChangePassActivity.this, R.style.DialogTheme);
                loading.setMessage("Please wait...");
                loading.setIndeterminate(false);
                loading.setCancelable(false);
                loading.show();
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(JSONObject s) {
                super.onPostExecute(s);
                if (s != null) {
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.FORGETPASSWORD, ChangePassActivity.this.object, new Listener<JSONObject>() {
                        public void onResponse(JSONObject response) {
                            try {
                                boolean status = response.getBoolean("status");
                                String msg = response.getString(NotificationCompat.CATEGORY_MESSAGE);
                                if (status) {
                                    loading.dismiss();
                                    AlertDialog.Builder builder = new AlertDialog.Builder(ChangePassActivity.this);
                                    builder.setTitle((CharSequence) "Forgot Password");

                                    builder.setMessage("A new password has been sent to your email.");
                                    builder.setPositiveButton((int) R.string.ok, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            Intent i=new Intent(ChangePassActivity.this,LoginActivity.class);
                                            startActivity(i);
                                            finish();
                                        }
                                    });

                                    builder.show();
                                 //   Button positiveButton = builder.getButton(DialogInterface.BUTTON_POSITIVE);
                                   // builder.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                                } else {
                                    loading.dismiss();
                                    AlertDialog.Builder builder = new AlertDialog.Builder(ChangePassActivity.this);
                                    builder.setTitle((CharSequence) "Forgot Password");

                                    builder.setMessage(msg);
                                    builder.setPositiveButton((int) R.string.ok, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                         return;
                                        }
                                    });

                                    builder.show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new ErrorListener() {
                        public void onErrorResponse(VolleyError error) {
                            ChangePassActivity.this.loading.dismiss();
                            // Toast.makeText(ChangePassActivity.this, "Volley Error"+error, Toast.LENGTH_LONG).show();
                            error.printStackTrace();
                        }
                    });
                    AppController.getInstance().addToRequestQueue(jsonObjectRequest);
                }
            }
        }.execute(new String[0]);
       // finish();
    }
}

