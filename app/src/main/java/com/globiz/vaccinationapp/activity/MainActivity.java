package com.globiz.vaccinationapp.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.login.LoginManager;
import com.globiz.vaccinationapp.R;
import com.globiz.vaccinationapp.customViews.CaseSensitiveLetters;
import com.globiz.vaccinationapp.customViews.CircularImageView;
import com.globiz.vaccinationapp.database.DBVaccination;
import com.globiz.vaccinationapp.fragment.AboutUsFragment;
import com.globiz.vaccinationapp.fragment.ChangePassFragment;
import com.globiz.vaccinationapp.fragment.ChildrenFragment;
import com.globiz.vaccinationapp.fragment.ContactUsFragment;
import com.globiz.vaccinationapp.fragment.FaqFragment;
import com.globiz.vaccinationapp.fragment.HelpUsFragment;
import com.globiz.vaccinationapp.fragment.HowItsWorkFragment;
import com.globiz.vaccinationapp.fragment.NotesFragment;
import com.globiz.vaccinationapp.fragment.ProfileFragment;
import com.globiz.vaccinationapp.fragment.VaccineDetailFragment;
import com.globiz.vaccinationapp.util.Constant;
import com.globiz.vaccinationapp.util.Preferences;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions.Builder;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.material.navigation.NavigationView;
import com.squareup.picasso.Picasso;
import java.io.PrintStream;

public class MainActivity extends AppCompatActivity implements OnConnectionFailedListener {
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private Context context;
    /* access modifiers changed from: private */
    public DBVaccination dbVaccination;
    boolean doubleBackToExitPressedOnce = false;
    /* access modifiers changed from: private */
    public DrawerLayout drawerLayout;
    private String email;
    /* access modifiers changed from: private */
    public String finalImageUrl;
    /* access modifiers changed from: private */
    public FragmentManager fragmentManager = getSupportFragmentManager();
    private GoogleSignInOptions googleSigninOption;
    View header;
    private boolean isPressed = false;
    private GoogleApiClient mGoogleApiClient;
    private String name;
    private NavigationView navigationView;
    /* access modifiers changed from: private */
    public TextView placeHolder;
    /* access modifiers changed from: private */
    public CircularImageView profile_image;
    /* access modifiers changed from: private */
    public Toolbar toolbar;
    /* access modifiers changed from: private */
    public TextView userName;
    CaseSensitiveLetters convert;
    public String gender;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this);
        setContentView((int) R.layout.activity_main);
        convert=new CaseSensitiveLetters();
        this.context = this;
        this.dbVaccination = new DBVaccination(this.context);
        if (Build.VERSION.SDK_INT >= 23) {
            requestPermissions(new String[]{"android.permission.WRITE_EXTERNAL_STORAGE"}, 1);
            requestPermissions(new String[]{"android.permission.READ_EXTERNAL_STORAGE"}, 1);
        }
        initViews();
        setDrawerListeners();
        if (Preferences.getUserChild(this).equals(AppEventsConstants.EVENT_PARAM_VALUE_NO)) {
            ProfileFragment profileFragment = new ProfileFragment();
            FragmentTransaction profileTransaction = this.fragmentManager.beginTransaction();
            profileTransaction.replace(R.id.container, profileFragment);
            profileTransaction.addToBackStack(null);
            profileTransaction.commit();
            this.toolbar.setTitle((CharSequence) "Profile");
            navigationView.getMenu().getItem(0).setChecked(true);
        } else {
            proceed();
        }
        setUpGoogleLoginOption();
    }

    private void proceed() {
        VaccineDetailFragment detailFragment = new VaccineDetailFragment();
        FragmentTransaction scheduleTransaction = this.fragmentManager.beginTransaction();
        scheduleTransaction.replace(R.id.container, detailFragment);
        scheduleTransaction.addToBackStack(null);
        scheduleTransaction.commit();
        getSupportActionBar().setTitle((CharSequence) "Vaccine Detail");
        navigationView.getMenu().getItem(1).setChecked(true);
    }

    private void initViews() {
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(this.toolbar);
        this.gender=Preferences.getUserGender(MainActivity.this);
        this.drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        this.navigationView = (NavigationView) findViewById(R.id.navigation_view);
        this.header = this.navigationView.getHeaderView(0);
        this.profile_image = (CircularImageView) this.header.findViewById(R.id.avatar);
        String url=Constant.BASE_IMAGE_URL;
        if(this.gender.matches("Female")){
            Picasso.with(MainActivity.this).load(url).placeholder((int) R.drawable.profile_logo1).into((ImageView) MainActivity.this.profile_image);
        }else{
            Picasso.with(MainActivity.this).load(url).placeholder((int) R.drawable.profile_logo).into((ImageView) MainActivity.this.profile_image);
        }
        this.userName = (TextView) this.header.findViewById(R.id.user_name);
        this.placeHolder = (TextView) this.header.findViewById(R.id.placeholder);

    }

    private void setDrawerListeners() {
        this.navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                MainActivity.this.drawerLayout.closeDrawers();
                switch (menuItem.getItemId()) {
                    case R.id.about /*2131230731*/:
                        AboutUsFragment aboutusFragment = new AboutUsFragment();
                        FragmentTransaction aboutTransaction = MainActivity.this.fragmentManager.beginTransaction();
                        aboutTransaction.replace(R.id.container, aboutusFragment);
                        aboutTransaction.addToBackStack(null);
                        aboutTransaction.commit();
                        MainActivity.this.toolbar.setTitle((CharSequence) "About Us");
                        return true;
                    case R.id.help /*2131230731*/:
                        HelpUsFragment helpusFragment = new HelpUsFragment();
                        FragmentTransaction helpTransaction = MainActivity.this.fragmentManager.beginTransaction();
                        helpTransaction.replace(R.id.container, helpusFragment);
                        helpTransaction.addToBackStack(null);
                        helpTransaction.commit();
                        MainActivity.this.toolbar.setTitle((CharSequence) "Help Us/Raise a Fund");
                        return true;
                    case R.id.change_password /*2131230796*/:
                        ChangePassFragment changePassFragment = new ChangePassFragment();
                        FragmentTransaction chanageTransaction = MainActivity.this.fragmentManager.beginTransaction();
                        chanageTransaction.replace(R.id.container, changePassFragment);
                        chanageTransaction.addToBackStack(null);
                        chanageTransaction.commit();
                        MainActivity.this.toolbar.setTitle((CharSequence) "Change Password");
                        return true;
                    case R.id.children /*2131230809*/:
                        ChildrenFragment childrenFragment = new ChildrenFragment();
                        FragmentTransaction childrenTransaction = MainActivity.this.fragmentManager.beginTransaction();
                        childrenTransaction.replace(R.id.container, childrenFragment);
                        childrenTransaction.addToBackStack(null);
                        childrenTransaction.commit();
                        MainActivity.this.toolbar.setTitle((CharSequence) "Children Detail");
                        return true;
                    case R.id.contact_us /*2131230823*/:
                        ContactUsFragment contactUsFragment = new ContactUsFragment();
                        FragmentTransaction contactTransaction = MainActivity.this.fragmentManager.beginTransaction();
                        contactTransaction.replace(R.id.container, contactUsFragment);
                        contactTransaction.addToBackStack(null);
                        contactTransaction.commit();
                        MainActivity.this.toolbar.setTitle((CharSequence) "Contact Us");
                        return true;
                    case R.id.faq /*2131230868*/:
                        FaqFragment faqFragment = new FaqFragment();
                        FragmentTransaction faqTransaction = MainActivity.this.fragmentManager.beginTransaction();
                        faqTransaction.replace(R.id.container, faqFragment);
                        faqTransaction.addToBackStack(null);
                        faqTransaction.commit();
                        MainActivity.this.toolbar.setTitle((CharSequence) "FAQ's");
                        return true;
                    case R.id.howItWork /*2131230884*/:
                        HowItsWorkFragment howitsFragment = new HowItsWorkFragment();
                        FragmentTransaction howitsTransaction = MainActivity.this.fragmentManager.beginTransaction();
                        howitsTransaction.replace(R.id.container, howitsFragment);
                        howitsTransaction.addToBackStack(null);
                        howitsTransaction.commit();
                        MainActivity.this.toolbar.setTitle((CharSequence) "How Its Work");
                        return true;
                    case R.id.logout /*2131230911*/:
                        int id = menuItem.getItemId();
                        String isLoggedIn = Preferences.getIsLoggedInBy(MainActivity.this);
                        if (id == R.id.logout) {
                            Preferences.resetUserDetails(MainActivity.this);
                            if (isLoggedIn.equalsIgnoreCase(Constant.FBLOGIN)) {
                                LoginManager.getInstance().logOut();
                                MainActivity.this.startActivity(new Intent(MainActivity.this, LoginActivity.class));
                                MainActivity.this.finish();
                            } else if (isLoggedIn.equalsIgnoreCase(Constant.GPLUS)) {
                                MainActivity.this.signOutFromGoogle();
                                MainActivity.this.startActivity(new Intent(MainActivity.this, LoginActivity.class));
                                MainActivity.this.finish();
                            } else {
                                MainActivity.this.dbVaccination.deleteAllTables();
                                MainActivity.this.startActivity(new Intent(MainActivity.this, LoginActivity.class));
                                MainActivity.this.finish();
                            }
                            return true;
                        }
                        break;
                    case R.id.notes /*2131230939*/:
                        NotesFragment notesFragment = new NotesFragment();
                        FragmentTransaction noteTransaction = MainActivity.this.fragmentManager.beginTransaction();
                        noteTransaction.replace(R.id.container, notesFragment);
                        noteTransaction.addToBackStack(null);
                        noteTransaction.commit();
                        MainActivity.this.toolbar.setTitle((CharSequence) "Notes");
                        return true;
                    case R.id.profile /*2131230960*/:
                        ProfileFragment profileFragment = new ProfileFragment();
                        FragmentTransaction profileTransaction = MainActivity.this.fragmentManager.beginTransaction();
                        profileTransaction.replace(R.id.container, profileFragment);
                        profileTransaction.addToBackStack(null);
                        profileTransaction.commit();
                        MainActivity.this.toolbar.setTitle((CharSequence) "Profile");
                        return true;
                    case R.id.schedule /*2131230978*/:
                        VaccineDetailFragment detailFragment = new VaccineDetailFragment();
                        FragmentTransaction scheduleTransaction = MainActivity.this.fragmentManager.beginTransaction();
                        scheduleTransaction.replace(R.id.container, detailFragment);
                        scheduleTransaction.addToBackStack(null);
                        scheduleTransaction.commit();
                        MainActivity.this.toolbar.setTitle((CharSequence) "Vaccine Detail");
                        return true;
                }
                Toast.makeText(MainActivity.this.getApplicationContext(), "Somethings Wrong", Toast.LENGTH_LONG).show();
                return true;
            }
        });
        ActionBarDrawerToggle r2 = new ActionBarDrawerToggle(this, this.drawerLayout, this.toolbar, R.string.openDrawer, R.string.closeDrawer) {
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @RequiresApi(api = 16)
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                String isLoggedInBy = Preferences.getIsLoggedInBy(MainActivity.this);
                String imageName = Preferences.getUserImageUrl(MainActivity.this);
                String gender=Preferences.getUserGender(MainActivity.this);
                if (isLoggedInBy.equalsIgnoreCase(Constant.CUSTOM)) {
                    MainActivity.this.finalImageUrl = imageName;
                } else if (isLoggedInBy.equalsIgnoreCase(Constant.GPLUS)) {
                    MainActivity.this.finalImageUrl = Preferences.getUserImageUrl(MainActivity.this);
                } else if (isLoggedInBy.equalsIgnoreCase(Constant.FBLOGIN)) {
                    MainActivity.this.finalImageUrl = Preferences.getUserImageUrl(MainActivity.this);
                }
                if (!TextUtils.isEmpty(MainActivity.this.finalImageUrl)) {
                    if (!TextUtils.isEmpty(imageName)) {
                        PrintStream printStream = System.out;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Url : ");
                        sb.append(imageName);
                        printStream.println(sb.toString());
                        MainActivity.this.profile_image.setVisibility(View.VISIBLE);
                        if (MainActivity.this.finalImageUrl.contains(Constant.BASE_IMAGE_URL)) {
                            if(MainActivity.this.gender.matches("Female")) {
                                Picasso.with(MainActivity.this).load(MainActivity.this.finalImageUrl).placeholder((int) R.drawable.profile_logo1).into((ImageView) MainActivity.this.profile_image);
                            }else{
                                Picasso.with(MainActivity.this).load(MainActivity.this.finalImageUrl).placeholder((int) R.drawable.profile_logo).into((ImageView) MainActivity.this.profile_image);
                            }
                        } else if (MainActivity.this.finalImageUrl.contains("google")) {
                            if(MainActivity.this.gender.matches("Female")) {
                                Picasso.with(MainActivity.this).load(MainActivity.this.finalImageUrl).placeholder((int) R.drawable.profile_logo1).into((ImageView) MainActivity.this.profile_image);
                            }else {
                                Picasso.with(MainActivity.this).load(MainActivity.this.finalImageUrl).placeholder((int) R.drawable.profile_logo).into((ImageView) MainActivity.this.profile_image);
                            }
                        } else if (MainActivity.this.finalImageUrl.contains("fb") || MainActivity.this.finalImageUrl.contains("facebook")) {
                            if(MainActivity.this.gender.matches("Female")) {
                                Picasso.with(MainActivity.this).load(MainActivity.this.finalImageUrl).placeholder((int) R.drawable.profile_logo1).into((ImageView) MainActivity.this.profile_image);
                            }else {
                                Picasso.with(MainActivity.this).load(MainActivity.this.finalImageUrl).placeholder((int) R.drawable.profile_logo).into((ImageView) MainActivity.this.profile_image);
                            }
                        } else {
                            MainActivity mainActivity = MainActivity.this;
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append(Constant.BASE_IMAGE_URL);
                            sb2.append("profileimg/");
                            sb2.append(MainActivity.this.finalImageUrl);
                            mainActivity.finalImageUrl = sb2.toString();
                            Log.e("GENDER INIT",MainActivity.this.gender);
                            if(MainActivity.this.gender.matches("Female")) {
                                Picasso.with(MainActivity.this).load(MainActivity.this.finalImageUrl).placeholder((int) R.drawable.profile_logo1).into((ImageView) MainActivity.this.profile_image);
                            }else {
                                Picasso.with(MainActivity.this).load(MainActivity.this.finalImageUrl).placeholder((int) R.drawable.profile_logo).into((ImageView) MainActivity.this.profile_image);
                            }
                        }
                        PrintStream printStream2 = System.out;
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("final : ");
                        sb3.append(MainActivity.this.finalImageUrl);
                        printStream2.println(sb3.toString());
                        MainActivity.this.placeHolder.setVisibility(View.GONE);
                    } else if (imageName.equals("null")) {
                        String fName = convert.caseSensitive( Character.toString(Preferences.getUserFName(MainActivity.this.getBaseContext()).charAt(0)));
                        String lName = convert.caseSensitive(Character.toString(Preferences.getUserLName(MainActivity.this.getBaseContext()).charAt(0)));
                        TextView access$700 = MainActivity.this.placeHolder;
                        StringBuilder sb4 = new StringBuilder();
                        sb4.append((fName.toUpperCase()));
                        sb4.append((lName.toUpperCase()));
                        access$700.setText((sb4.toString()));
                        MainActivity.this.profile_image.setVisibility(View.VISIBLE);
                    }
                }
                MainActivity.this.userName.setText(convert.caseSensitive(Preferences.getUserName(MainActivity.this.getBaseContext())));
            }
        };
        this.actionBarDrawerToggle = r2;
        this.drawerLayout.addDrawerListener(this.actionBarDrawerToggle);
        this.actionBarDrawerToggle.syncState();
    }

    private void setUpGoogleLoginOption() {
        this.googleSigninOption = new Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().requestProfile().requestId().build();
        this.mGoogleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, this.googleSigninOption).build();
    }

    public void onStart() {
        super.onStart();
        if (this.mGoogleApiClient != null) {
            this.mGoogleApiClient.connect();
        }
    }

    public void onStop() {
        super.onStop();
        if (this.mGoogleApiClient != null && this.mGoogleApiClient.isConnected()) {
            this.mGoogleApiClient.stopAutoManage((FragmentActivity) this.context);
            this.mGoogleApiClient.disconnect();
        }
    }

    public void onBackPressed() {
        if (!this.doubleBackToExitPressedOnce) {
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this.context, R.string.double_Tap_to_exit, Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    MainActivity.this.doubleBackToExitPressedOnce = false;
                }
            }, 1000);
            return;
        }
        finish();
    }

    /* access modifiers changed from: private */
    public void signOutFromGoogle() {
        Auth.GoogleSignInApi.signOut(this.mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
            public void onResult(Status status) {
                PrintStream printStream = System.out;
                StringBuilder sb = new StringBuilder();
                sb.append("Aman : ");
                sb.append(status.getStatus());
                printStream.println(sb.toString());
            }
        });
    }

    public void onConnectionFailed(ConnectionResult connectionResult) {
    }
}

