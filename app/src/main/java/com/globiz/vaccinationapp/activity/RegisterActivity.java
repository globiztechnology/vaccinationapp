package com.globiz.vaccinationapp.activity;


import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import com.facebook.appevents.AppEventsConstants;
import com.facebook.share.internal.ShareConstants;
import com.globiz.vaccinationapp.R;
import com.globiz.vaccinationapp.controller.AppController;
import com.globiz.vaccinationapp.model.RegisterData;
import com.globiz.vaccinationapp.util.Constant;
import com.globiz.vaccinationapp.util.Functions;
import com.globiz.vaccinationapp.util.Preferences;
import com.google.firebase.analytics.FirebaseAnalytics.Event;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import java.io.PrintStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RegisterActivity extends AppCompatActivity implements OnClickListener {
    private String aadhar;
    private EditText aadharET;
    private String address;
    /* access modifiers changed from: private */
    public EditText addressET;
    /* access modifiers changed from: private */
    public RadioButton btnFemale;
    /* access modifiers changed from: private */
    public RadioButton btnMale;
    public RadioButton btnFather;
    public RadioButton btnMother;
    public RadioButton btnGuardian;
    private EditText childET;
    /* access modifiers changed from: private */
    public SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy");
    private DatePickerDialog datePickerDialog;
    private String dob;
    /* access modifiers changed from: private */
    public EditText dobET;
    public String ageDob;
    private String email;
    /* access modifiers changed from: private */
    public EditText emailET;
    /* access modifiers changed from: private */
    public EditText etOTP;
    private String fName;
    /* access modifiers changed from: private */
    public EditText fNameET;
    private String lName;
    /* access modifiers changed from: private */
    public EditText lNameET;
    private Button loginBT;
    private ProgressDialog mProgressDialog;
    private String mobile;
    /* access modifiers changed from: private */
    public EditText mobileET;
    private String noOfchild;
    private JSONObject object;
    public ImageButton aadharInfo;
    private String password;
    /* access modifiers changed from: private */
    public EditText passwordET;
    /* access modifiers changed from: private */

    /* renamed from: pd */
    public ProgressDialog f43pd;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog;
    private RadioGroup radioGroup;
    private  RadioGroup radioRelation;
    String radiobutton_status = "";

    private BroadcastReceiver receiver;
    private Button registerBT;
    private String token;
    Calendar newCalendar;
    /* access modifiers changed from: private */
    public Button verifyOTPBT;
    private String radiobutton_relation="";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_register);
        newCalendar = Calendar.getInstance();
        this.receiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equalsIgnoreCase("otp")) {
                    String message = intent.getStringExtra(ShareConstants.WEB_DIALOG_PARAM_MESSAGE);
                    PrintStream printStream = System.out;
                    StringBuilder sb = new StringBuilder();
                    sb.append("mnb  : ");
                    sb.append(message);
                    printStream.println(sb.toString());
                    RegisterActivity.this.etOTP.setText(message);
                }
            }
        };
        initViews();
        this.token = FirebaseInstanceId.getInstance().getToken();
    }

    private void initViews() {
        this.fNameET = (EditText) findViewById(R.id.fNameET);
        this.lNameET = (EditText) findViewById(R.id.lNameET);
        this.emailET = (EditText) findViewById(R.id.emailET);
        this.passwordET = (EditText) findViewById(R.id.passwordET);
        this.mobileET = (EditText) findViewById(R.id.mobileET);
        this.mobileET.setOnEditorActionListener(new OnEditorActionListener() {
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == 5) {
                    RegisterActivity.this.datePickerDialog();
                }
                return false;
            }
        });
        this.dobET = (EditText) findViewById(R.id.dobET);
        this.addressET = (EditText) findViewById(R.id.addressET);
        this.aadharET = (EditText) findViewById(R.id.aadharET);
        this.aadharET.addTextChangedListener(new TextWatcher() {
            private static final char space = ' ';
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (start == 0) {
                    if (s.toString().equalsIgnoreCase("0") || s.toString().equalsIgnoreCase("1")) {
                        Functions.setErrorEditText(RegisterActivity.this.aadharET, "Aadhar Number should not start with 0 and 1");
                    }
                } else if (start != 13) {
                    Functions.setErrorEditText(RegisterActivity.this.aadharET, "Aadhar Number can't be Empty. Upto 12 digits");
                }
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {

                // Remove all spacing char
                int pos = 0;

                while (true) {
                    if (pos >= s.length()) break;
                    if (space == s.charAt(pos) && (((pos + 1) % 5) != 0 || pos + 1 == s.length())) {
                        s.delete(pos, pos + 1);
                    } else {
                        pos++;
                    }
                }

                // Insert char where needed.
                pos = 4;
                while (true) {
                    if (pos >= s.length()) break;
                    final char c = s.charAt(pos);

                    // Only if its a digit where there should be a space we insert a space
                    if ("0123456789".indexOf(c) >= 0) {
                        s.insert(pos, "" + space);
                    }
                    pos += 5;
                }
            }
        });
        this.btnMale = (RadioButton) findViewById(R.id.btnMale);
        this.btnFemale = (RadioButton) findViewById(R.id.btnFemale);
        this.radioGroup = (RadioGroup) findViewById(R.id.genderGroup);
        this.radioRelation = (RadioGroup) findViewById(R.id.relationGroup);
        this.btnFather = (RadioButton) findViewById(R.id.btnFather);
        this.btnMother = (RadioButton) findViewById(R.id.btnMother);
        this.btnGuardian = (RadioButton) findViewById(R.id.btnGuardian);
        this.aadharInfo = findViewById(R.id.aadharInfo);
        this.registerBT = (Button) findViewById(R.id.registerBT);
        this.loginBT = (Button) findViewById(R.id.loginBT);
        this.registerBT.setOnClickListener(this);
        this.loginBT.setOnClickListener(this);
        this.aadharInfo.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Toast.makeText(RegisterActivity.this, "Please enter your Aadhar number -- it is just to track all children's vaccines for future purpose", Toast.LENGTH_SHORT).show();
                return true;
            }
        });
        this.btnFather.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                RegisterActivity.this.btnFather.setChecked(true);
                RegisterActivity.this.btnMother.setChecked(false);
                RegisterActivity.this.radiobutton_relation = "father";
                RegisterActivity.this.btnMale.setChecked(true);
                RegisterActivity.this.btnFemale.setChecked(false);
                RegisterActivity.this.radiobutton_status = AppEventsConstants.EVENT_PARAM_VALUE_NO;
            }
        });
        this.btnMother.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                RegisterActivity.this.btnFather.setChecked(false);
                RegisterActivity.this.btnMother.setChecked(true);
                RegisterActivity.this.radiobutton_relation = "mother";
                RegisterActivity.this.btnMale.setChecked(false);
                RegisterActivity.this.btnFemale.setChecked(true);
                RegisterActivity.this.radiobutton_status = AppEventsConstants.EVENT_PARAM_VALUE_YES;
            }
        });
        this.btnGuardian.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                RegisterActivity.this.btnFather.setChecked(false);
                RegisterActivity.this.btnMother.setChecked(false);
                RegisterActivity.this.btnMale.setChecked(false);
                RegisterActivity.this.btnFemale.setChecked(false);
                RegisterActivity.this.btnGuardian.setChecked(true);
                RegisterActivity.this.radiobutton_relation = "guardian";
            }
        });

        this.btnMale.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                RegisterActivity.this.btnMale.setChecked(true);
                RegisterActivity.this.btnFemale.setChecked(false);
                RegisterActivity.this.radiobutton_status = AppEventsConstants.EVENT_PARAM_VALUE_NO;
//                if(RegisterActivity.this.radiobutton_relation==null){
//                    RegisterActivity.this.radiobutton_relation = "guardian";
//                }
                if(RegisterActivity.this.radiobutton_relation.equalsIgnoreCase("guardian")){
//                    RegisterActivity.this.btnFather.setChecked(false);
//                    RegisterActivity.this.btnMother.setChecked(false);
//                    RegisterActivity.this.btnGuardian.setChecked(true);
//                    RegisterActivity.this.radiobutton_relation = "guardian";
                }else{
                    RegisterActivity.this.btnFather.setChecked(true);
                    RegisterActivity.this.btnMother.setChecked(false);
                    RegisterActivity.this.btnGuardian.setChecked(false);
                    RegisterActivity.this.radiobutton_relation = "father";
                }
            }
        });
        this.btnFemale.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                RegisterActivity.this.btnMale.setChecked(false);
                RegisterActivity.this.btnFemale.setChecked(true);
                RegisterActivity.this.radiobutton_status = AppEventsConstants.EVENT_PARAM_VALUE_YES;
                if(RegisterActivity.this.radiobutton_relation.equalsIgnoreCase("guardian")){
                    //                    RegisterActivity.this.btnFather.setChecked(false);
//                    RegisterActivity.this.btnMother.setChecked(false);
//                    RegisterActivity.this.btnGuardian.setChecked(true);
//                    RegisterActivity.this.radiobutton_relation = "guardian";
                }else {
                    RegisterActivity.this.btnFather.setChecked(false);
                    RegisterActivity.this.btnMother.setChecked(true);
                    RegisterActivity.this.btnGuardian.setChecked(false);
                    RegisterActivity.this.radiobutton_relation = "mother";
                }
            }
        });
        this.dobET.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() != 1) {
                    return false;
                }
                if (event.getRawX() >= ((float) (RegisterActivity.this.dobET.getRight() - RegisterActivity.this.dobET.getCompoundDrawables()[2].getBounds().width()))) {
                    RegisterActivity.this.datePickerDialog();
                }
                return true;
            }
        });
    }

    /* access modifiers changed from: private */
    public void datePickerDialog() {

        DatePickerDialog datePickerDialog2 = new DatePickerDialog(this, R.style.DialogTheme, new OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                newCalendar.set(year, monthOfYear, dayOfMonth);
                String selectdate = RegisterActivity.this.dateFormatter.format(newCalendar.getTime());
                String currentDateandTime = new SimpleDateFormat("dd-MMM-yyyy").format(new Date());
                String formatDate = new SimpleDateFormat("dd-MM-yyyy").format(newCalendar.getTime());
                try {
                    if (RegisterActivity.this.dateFormatter.parse(selectdate).before(RegisterActivity.this.dateFormatter.parse(currentDateandTime))) {
                        RegisterActivity.this.dobET.setText(RegisterActivity.this.dateFormatter.format(newCalendar.getTime()));
                        RegisterActivity.this.ageDob=formatDate;
                    } else if (RegisterActivity.this.dateFormatter.parse(selectdate).equals(RegisterActivity.this.dateFormatter.parse(currentDateandTime))) {
                        RegisterActivity.this.dobET.setText(RegisterActivity.this.dateFormatter.format(newCalendar.getTime()));
                        RegisterActivity.this.ageDob=formatDate;
                    } else if (RegisterActivity.this.dateFormatter.parse(selectdate).after(RegisterActivity.this.dateFormatter.parse(currentDateandTime))) {
                        final Dialog dialog = new Dialog(RegisterActivity.this);
                        dialog.setContentView(R.layout.dob_validate_dialog);
                        ((Button) dialog.findViewById(R.id.okBtn)).setOnClickListener(new OnClickListener() {
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                        dialog.show();
                    }
                    PrintStream printStream = System.out;
                    StringBuilder sb = new StringBuilder();
                    sb.append("selectDate");
                    sb.append(selectdate);
                    sb.append(" : ");
                    sb.append(currentDateandTime);
                    printStream.println(sb.toString());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        this.datePickerDialog = datePickerDialog2;
        this.datePickerDialog.show();
    }
    public boolean isAlpha(String s) {
        return s != null && s.matches("^[a-zA-z]+([\\s][a-zA-Z]+)*$");
    }
    private boolean isValidInput() {
        this.fNameET.setError(null);
        this.lNameET.setError(null);
        this.emailET.setError(null);
        this.passwordET.setError(null);
        this.mobileET.setError(null);
        this.dobET.setError(null);
        this.addressET.setError(null);
        this.aadharET.setError(null);
        this.fName = this.fNameET.getText().toString().trim();
        this.lName = this.lNameET.getText().toString().trim();
        this.email = this.emailET.getText().toString().trim();
        this.password = this.passwordET.getText().toString().trim();
        this.mobile = this.mobileET.getText().toString().trim();
        this.dob = this.dobET.getText().toString().trim();
        this.address = this.addressET.getText().toString().trim();
        this.aadhar = this.aadharET.getText().toString().trim();
        char ch=' ';
        Period p = null;
        if(!TextUtils.isEmpty(this.ageDob)){
            String arr[] = this.ageDob.split("-");
            int d = Integer.parseInt(arr[0]);
            int m = Integer.parseInt(arr[1]);
            int y = Integer.parseInt(arr[2]);
            LocalDate d1 = LocalDate.of(y, m, d);
            LocalDate d2 = LocalDate.now();
             p= Period.between(d1, d2);
        }

        if(!TextUtils.isEmpty(this.aadhar)){
            ch=this.aadharET.getText().toString().charAt(0);
        }
        if (TextUtils.isEmpty(this.fName)) {
            Functions.setErrorEditText(this.fNameET, "First Name can't be Empty");
            return false;
        } else if (!isAlpha(this.fName)) {
            Functions.setErrorEditText(this.fNameET, "Invalid Names");
            return false;
        } else if (TextUtils.isEmpty(this.lName)) {
            Functions.setErrorEditText(this.lNameET, "Last Name can't be Empty");
            return false;
        }else if (!isAlpha(this.lName)) {
            Functions.setErrorEditText(this.lNameET, "Invalid Name");
            return false;
        } else if (TextUtils.isEmpty(this.email)) {
            Functions.setErrorEditText(this.emailET, "Email con't be Empty");
            return false;
        } else if (!Functions.isValidEmail(this.email)) {
            Functions.setErrorEditText(this.emailET, "Invalid Email");
            return false;
        } else if (TextUtils.isEmpty(this.password)) {
            Functions.setErrorEditText(this.passwordET, "Password can't be Empty");
            return false;
        } else if (this.password.length() < 6) {
            Functions.setErrorEditText(this.passwordET, "Password should be 6 digit");
            return false;
        } else if (TextUtils.isEmpty(this.mobile)) {
            Functions.setErrorEditText(this.mobileET, "Mobile can't be Empty");
            return false;
        } else if (!Functions.isValidMobile(this.mobile)) {
            Functions.setErrorEditText(this.mobileET, "Mobile no should be 10 digits");
            return false;
        } else if (TextUtils.isEmpty(this.dob)) {
            Functions.setErrorEditText(this.dobET, "DOB can't be Empty");
            return false;
        } else if (TextUtils.isEmpty(this.address)) {
            Functions.setErrorEditText(this.addressET, "Address can't be Empty");
            return false;
        }  else if (ch=='0' || ch== '1') {
            Functions.setErrorEditText(this.aadharET, "Aadhar Number should not start with 0 and 1");
            return false;
        }

        else if (!TextUtils.isEmpty(this.aadhar) && this.aadhar.length() != 14) {
            Functions.setErrorEditText(this.aadharET, "Aadhar Number can't be Empty. Upto 12 digits");
            return false;
        }




        else if (this.radioRelation.getCheckedRadioButtonId() == -1) {
            Toast.makeText(getApplicationContext(), "Please select your relationship with child", Toast.LENGTH_SHORT).show();
            return false;
        } else if (this.radioGroup.getCheckedRadioButtonId() != -1) {
            Boolean value =true;
            if(this.radiobutton_status == "0" && p.getYears() < 21){
                Functions.setErrorEditText(this.dobET, "Your Age should be 21 or more than 21.");
                return false;
            }else if(this.radiobutton_status == "1" && p.getYears() < 18){
                Functions.setErrorEditText(this.dobET, "Your Age should be 18 or more than 18.");
                return false;
            }else {
                return  true;
            }
        } else {
            Toast.makeText(getApplicationContext(), "Please select Gender", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private void registerUser() {
        if (isValidInput()) {
            this.progressDialog = new ProgressDialog(this,R.style.DialogTheme);
            this.progressDialog.setMessage("Loading...");
            this.progressDialog.show();
            this.object = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("firstname", this.fName);
                jsonObject.put("lastname", this.lName);
                jsonObject.put("email", this.email);
                jsonObject.put("password", this.password);
                jsonObject.put("mobile", this.mobile);
                jsonObject.put("dob", this.dob);
                jsonObject.put("address", this.address);
                jsonObject.put("aadhar", this.aadhar);
                jsonObject.put("gender", this.radiobutton_status);
                jsonObject.put("relation",this.radiobutton_relation);
                jsonArray.put(jsonObject);
                this.object.put("user", jsonArray);
                Log.e("response", this.object.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.USER_REGISTRATION, this.object, new Listener<JSONObject>() {
                public void onResponse(JSONObject response) {
                    PrintStream printStream = System.out;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Aman : ");
                    sb.append(response);
                    printStream.println(sb.toString());
                    RegisterData registerData = (RegisterData) new Gson().fromJson(response.toString(), RegisterData.class);
                    boolean status = registerData.getStatus().booleanValue();
                    String msg = registerData.getMsg();
                    if (status) {
                        RegisterActivity.this.progressDialog.dismiss();
                        RegisterActivity.this.fNameET.setHint("Fisrt Name");
                        RegisterActivity.this.lNameET.setHint("Last Name");
                        RegisterActivity.this.emailET.setHint("Email");
                        RegisterActivity.this.passwordET.setHint("Password");
                        RegisterActivity.this.dobET.setHint("DOB");
                        RegisterActivity.this.mobileET.setHint("Mobile");
                        RegisterActivity.this.addressET.setHint("Address");
                        RegisterActivity.this.startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                        Toast.makeText(RegisterActivity.this, msg, Toast.LENGTH_LONG).show();
                        RegisterActivity.this.finish();
                        return;
                    }
                    RegisterActivity.this.progressDialog.dismiss();
                    Toast.makeText(RegisterActivity.this, msg, Toast.LENGTH_LONG).show();
                }
            }, new ErrorListener() {
                public void onErrorResponse(VolleyError error) {
                    String errortype = null;
                    if ((error instanceof TimeoutError) || (error instanceof NoConnectionError)) {
                        errortype = "No Internet Access, check your internet connection.";
                    } else if (error instanceof AuthFailureError) {
                        errortype = "Authentication error, please try again.";
                    } else if (error instanceof ServerError) {
                        errortype = "Server error, please try again.";
                    } else if (error instanceof NetworkError) {
                        errortype = "Network error, please try again.";
                    } else if (error instanceof ParseError) {
                        errortype = "Internal error, please try again.";
                    }
                    Toast.makeText(RegisterActivity.this, errortype, Toast.LENGTH_SHORT).show();
                    RegisterActivity.this.progressDialog.dismiss();
                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjectRequest);
        }
    }

    public void sendOTPURL(String phone) {
        this.f43pd = ProgressDialog.show(this, "", "Please Wait...", true);
        JSONObject jsonObject1 = new JSONObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("mobile", phone);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(jsonObject);
            jsonObject1.putOpt("user", jsonArray);
            Log.e("user", jsonObject1.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.SENDOTP_URL, jsonObject1, new Listener<JSONObject>() {
            public void onResponse(final JSONObject response) {
                Log.d("response :", response.toString());
                RegisterActivity.this.f43pd.dismiss();
                try {
                    boolean status = response.getBoolean("status");
                    String msg = response.getString(NotificationCompat.CATEGORY_MESSAGE);
                    if (status) {
                        final Dialog dialog = new Dialog(RegisterActivity.this);
                        dialog.setContentView(R.layout.custom_otp);
                        dialog.setTitle("OTP");
                        dialog.setCancelable(false);
                        LayoutParams lp = new LayoutParams();
                        lp.copyFrom(dialog.getWindow().getAttributes());
                        lp.width = -1;
                        lp.height = -2;
                        RegisterActivity.this.etOTP = (EditText) dialog.findViewById(R.id.enterOTP);
                        final String otpCode = RegisterActivity.this.etOTP.getText().toString().trim();
                        RegisterActivity.this.verifyOTPBT = (Button) dialog.findViewById(R.id.verifyOTP);
                        RegisterActivity.this.verifyOTPBT.setOnClickListener(new OnClickListener() {
                            public void onClick(View v) {
                                try {
                                    if (!Functions.isNetworkConnected(RegisterActivity.this)) {
                                        Toast.makeText(RegisterActivity.this, RegisterActivity.this.getString(R.string.check_your_internet_connectivity), Toast.LENGTH_SHORT).show();
                                    } else if (otpCode.length() != 0) {
                                        Toast.makeText(RegisterActivity.this, "OTP Cannot be empty", Toast.LENGTH_SHORT).show();
                                    } else if (RegisterActivity.this.etOTP.getText().toString().equalsIgnoreCase(response.getString("otp"))) {
                                        Toast.makeText(RegisterActivity.this, "OTP Verified", Toast.LENGTH_SHORT).show();
                                        dialog.dismiss();
                                        RegisterActivity.this.startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                                        RegisterActivity.this.finish();
                                    } else {
                                        Toast.makeText(RegisterActivity.this, "Invalid OTP. Please check the Code.", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        dialog.show();
                        dialog.getWindow().setAttributes(lp);
                        return;
                    }
                    Toast.makeText(RegisterActivity.this, msg, Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", "ERROR");
                RegisterActivity.this.f43pd.dismiss();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    private void showProgressDialog() {
        if (this.mProgressDialog == null) {
            this.mProgressDialog = new ProgressDialog(this,R.style.DialogTheme);
            this.mProgressDialog.setMessage(getString(R.string.loading));
            this.mProgressDialog.setIndeterminate(true);
        }
        this.mProgressDialog.show();
    }

    /* access modifiers changed from: private */
    public void hideProgressDialog() {
        if (this.mProgressDialog != null && this.mProgressDialog.isShowing()) {
            this.mProgressDialog.hide();
        }
    }


    private void loginUser() {
        if (isValidInput()) {
            showProgressDialog();
            JSONObject object2 = new JSONObject();
            try {
                JSONArray jsonArray = new JSONArray();
                JSONObject object1 = new JSONObject();
                object1.putOpt("email", this.email);
                object1.putOpt("password", this.password);
                object1.putOpt("token", this.token);
                jsonArray.put(object1);
                object2.put(Event.LOGIN, jsonArray);
                Log.e("response", object2.toString());
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.USER_LOGIN, object2, new Listener<JSONObject>() {
                    public void onResponse(JSONObject response) {
                        try {
                            RegisterActivity.this.hideProgressDialog();
                            boolean status = response.getBoolean("status");
                            String msg = response.getString(NotificationCompat.CATEGORY_MESSAGE);
                            if (status) {
                                PrintStream printStream = System.out;
                                StringBuilder sb = new StringBuilder();
                                sb.append("Login 1");
                                sb.append(response);
                                printStream.println(sb.toString());
                                JSONObject jsonObject = response.getJSONObject("User");
                                Preferences.setUserId(RegisterActivity.this, jsonObject.getString(ShareConstants.WEB_DIALOG_PARAM_ID));
                                Preferences.setUserEmail(RegisterActivity.this, jsonObject.getString("email"));
                                Preferences.setUserPassword(RegisterActivity.this, jsonObject.getString("password"));
                                RegisterActivity registerActivity = RegisterActivity.this;
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append(jsonObject.getString("firstname"));
                                sb2.append(" ");
                                sb2.append(jsonObject.getString("lastname"));
                                Preferences.setUserName(registerActivity, sb2.toString());
                                Preferences.setUserFName(RegisterActivity.this, jsonObject.getString("firstname"));
                                Preferences.setUserLName(RegisterActivity.this, jsonObject.getString("lastname"));
                                Preferences.setUserPhone(RegisterActivity.this, jsonObject.getString("phone"));
                                Preferences.setUserAddress(RegisterActivity.this, jsonObject.getString("address"));
                                if (!jsonObject.getString("aadhar").equalsIgnoreCase("null")) {
                                    Preferences.setUserAadhar(RegisterActivity.this, jsonObject.getString("aadhar"));
                                } else {
                                    Preferences.setUserAadhar(RegisterActivity.this, "");
                                }
                                Preferences.setUserImageUrl(RegisterActivity.this, jsonObject.getString("image"));
                                Preferences.setUserChild(RegisterActivity.this, jsonObject.getString("no_of_child"));
                                Preferences.setIsLoggedInBy(RegisterActivity.this, Constant.CUSTOM);
                                RegisterActivity.this.startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                                RegisterActivity.this.finish();
                            } else if (msg.equalsIgnoreCase("Please varify your account")) {
                                Toast.makeText(RegisterActivity.this, "Please verify your account", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(RegisterActivity.this, msg, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        RegisterActivity.this.hideProgressDialog();
                    }
                });
                AppController.getInstance().addToRequestQueue(jsonObjectRequest);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.loginBT) {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        } else if (id == R.id.registerBT) {
            if (Functions.isNetworkConnected(this)) {
                registerUser();
            } else {
                Toast.makeText(this, R.string.check_your_internet_connectivity, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
