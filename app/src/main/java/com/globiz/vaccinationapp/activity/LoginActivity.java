package com.globiz.vaccinationapp.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import com.facebook.FacebookSdk;


import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;

import com.facebook.GraphRequest;
import com.facebook.GraphRequest.GraphJSONObjectCallback;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.internal.ShareConstants;
import com.globiz.vaccinationapp.R;
import com.globiz.vaccinationapp.controller.AppController;
import com.globiz.vaccinationapp.util.Constant;
import com.globiz.vaccinationapp.util.Functions;
import com.globiz.vaccinationapp.util.Preferences;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions.Builder;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics.Event;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity implements OnClickListener, OnConnectionFailedListener {
    public static GoogleApiClient mGoogleApiClient;
    private int RC_SIGN_IN = 1;
    private CallbackManager callbackManager;
    private String email;
    private EditText emailET;
    private ImageView faceBookLoginBT;
    /* access modifiers changed from: private */
    public String fbEmail;
    /* access modifiers changed from: private */
    public String fbFName;
    /* access modifiers changed from: private */
    public String fbId;
    /* access modifiers changed from: private */
    public String fbLName;
    private TextView forgetPassTV;
    private String googleEmail;
    private String googleId;
    private ImageView googleLoginBt;
    private String googleName;
    private GoogleSignInOptions googleSigninOption;
    Button loginBt;
    private ProgressDialog mProgressDialog;
    private JSONObject object;
    private String password;
    private EditText passwordET;
    /* access modifiers changed from: private */
    public String profile_image;
    Button registerBt;
    private String token;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
//        AppEventsLogger.activateApp(this);
        setContentView((int) R.layout.activity_login);
        FirebaseApp.initializeApp(this);
        initViews();
        setUpGoogleLoginOption();
        this.token = FirebaseInstanceId.getInstance().getToken();
        String str = "Message";
        Log.d(str, getString(R.string.msg_token_fmt, new Object[]{this.token}));
    }

    private void setUpGoogleLoginOption() {
        this.googleSigninOption = new Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile().requestId().build();
        mGoogleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, this.googleSigninOption).build();
    }

    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    public void onStop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    private void initViews() {
        this.emailET = (EditText) findViewById(R.id.emailET);
        this.passwordET = (EditText) findViewById(R.id.passwordET);
        this.forgetPassTV = (TextView) findViewById(R.id.forgetPassTV);
        this.loginBt = (Button) findViewById(R.id.loginBT);
        this.registerBt = (Button) findViewById(R.id.registerBT);
        this.googleLoginBt = (ImageView) findViewById(R.id.googleLoginBT);
        this.faceBookLoginBT = (ImageView) findViewById(R.id.fbSignInBT);
        this.forgetPassTV.setOnClickListener(this);
        this.loginBt.setOnClickListener(this);
        this.registerBt.setOnClickListener(this);
        this.googleLoginBt.setOnClickListener(this);
        this.faceBookLoginBT.setOnClickListener(this);
    }

    private boolean isValidInput() {
        this.emailET.setError(null);
        this.passwordET.setError(null);
        this.email = this.emailET.getText().toString().trim();
        this.password = this.passwordET.getText().toString().trim();
        if (TextUtils.isEmpty(this.email)) {
            Functions.setErrorEditText(this.emailET, "Email can not be Empty");
            return false;
        } else if (!Functions.isValidEmail(this.email)) {
            Functions.setErrorEditText(this.emailET, "Invalid Email");
            return false;
        } else if (TextUtils.isEmpty(this.password)) {
            Functions.setErrorEditText(this.passwordET, "Please enter password");
            return false;
        } else if (Functions.isValidPassword(this.password)) {
            return true;
        } else {
            Functions.setErrorEditText(this.passwordET, "Invalid Password : 6 digit");
            return false;
        }
    }

    private void showProgressDialog() {
        if (this.mProgressDialog == null) {
            this.mProgressDialog = new ProgressDialog(this,R.style.DialogTheme);
            this.mProgressDialog.setMessage(getString(R.string.loading));
            this.mProgressDialog.setIndeterminate(true);
            return;
        }
        this.mProgressDialog.show();
    }

    /* access modifiers changed from: private */
    public void hideProgressDialog() {
        if (this.mProgressDialog != null && this.mProgressDialog.isShowing()) {
            this.mProgressDialog.hide();
        }
    }

    private void loginUser() {
        if (isValidInput()) {
            showProgressDialog();
            JSONObject object2 = new JSONObject();
            try {
                JSONArray jsonArray = new JSONArray();
                JSONObject object1 = new JSONObject();
                object1.putOpt("email", this.email);
                object1.putOpt("password", this.password);
                object1.putOpt("token", this.token);
                jsonArray.put(object1);
                object2.put(Event.LOGIN, jsonArray);
                Log.e("response Login", object2.toString());
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.USER_LOGIN, object2, new Listener<JSONObject>()
                {
                    public void onResponse(JSONObject response) {
                        try {
                            LoginActivity.this.hideProgressDialog();
                            boolean status = response.getBoolean("status");
                            String msg = response.getString(NotificationCompat.CATEGORY_MESSAGE);

                            if (status) {
                                PrintStream printStream = System.out;
                                StringBuilder sb = new StringBuilder();
                                sb.append("Login 1");
                                sb.append(response);
                                printStream.println(sb.toString());
                                JSONObject jsonObject = response.getJSONObject("User");
                                Preferences.setUserId(LoginActivity.this, jsonObject.getString(ShareConstants.WEB_DIALOG_PARAM_ID));
                                Preferences.setUserEmail(LoginActivity.this, jsonObject.getString("email"));
                                Preferences.setUserPassword(LoginActivity.this, jsonObject.getString("password"));
                                LoginActivity loginActivity = LoginActivity.this;
                                StringBuilder sb2 = new StringBuilder();

                                sb2.append(jsonObject.getString("firstname"));
                                sb2.append(" ");
                                sb2.append(jsonObject.getString("lastname"));
                                Preferences.setUserName(loginActivity, sb2.toString());
                                Preferences.setUserFName(LoginActivity.this, jsonObject.getString("firstname"));
                                Preferences.setUserLName(LoginActivity.this, jsonObject.getString("lastname"));
                                Preferences.setUserPhone(LoginActivity.this, jsonObject.getString("phone"));
                                Preferences.setUserAddress(LoginActivity.this, jsonObject.getString("address"));
                                Preferences.setUserGender(LoginActivity.this,jsonObject.getString("gender"));
                                if (!jsonObject.getString("aadhar").equalsIgnoreCase("null")) {
                                    Preferences.setUserAadhar(LoginActivity.this, jsonObject.getString("aadhar"));
                                } else {
                                    Preferences.setUserAadhar(LoginActivity.this, "");
                                }
                                Preferences.setUserImageUrl(LoginActivity.this, jsonObject.getString("image"));
                                Preferences.setUserChild(LoginActivity.this, jsonObject.getString("no_of_child"));
                                Preferences.setIsLoggedInBy(LoginActivity.this, Constant.CUSTOM);
                                LoginActivity.this.startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                LoginActivity.this.finish();
                            } else if (msg.equalsIgnoreCase("Please varify your account")) {
                                Toast.makeText(LoginActivity.this, "Please verify your account", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        LoginActivity.this.hideProgressDialog();
                    }
                });
                AppController.getInstance().addToRequestQueue(jsonObjectRequest);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fbSignInBT /*2131230870*/:
                if (Functions.isNetworkConnected(this)) {
                    LoginManager.getInstance().logOut();
                    facebookSignIn();
                    return;
                } else {
                    Toast.makeText(this, R.string.check_your_internet_connectivity, Toast.LENGTH_SHORT).show();
                    return;
                }
            case R.id.forgetPassTV /*2131230877*/:
                startActivity(new Intent(this, ChangePassActivity.class));
                return;
            case R.id.googleLoginBT /*2131230881*/:
                if (Functions.isNetworkConnected(this)) {
                    googleSignIn();
                    return;
                } else {
                    Toast.makeText(this, R.string.check_your_internet_connectivity, Toast.LENGTH_SHORT).show();
                    return;
                }
            case R.id.loginBT /*2131230910*/:
                if (!Functions.isNetworkConnected(this)) {
                    Toast.makeText(this, R.string.check_your_internet_connectivity, Toast.LENGTH_SHORT).show();
                    return;
                } else if (!TextUtils.isEmpty(this.token)) {
                    loginUser();
                    return;
                } else {
                    this.token = FirebaseInstanceId.getInstance().getToken();
                    return;
                }
            case R.id.registerBT /*2131230966*/:
                startActivity(new Intent(this, RegisterActivity.class));
                finish();
                return;
            default:
                return;
        }
    }

    private void googleSignIn() {
            startActivityForResult(Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient), this.RC_SIGN_IN);
            showProgressDialog();
    }

    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        StringBuilder sb = new StringBuilder();
        sb.append("onConnectionFailed:");
        sb.append(connectionResult);
        Log.d(" ", sb.toString());
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        hideProgressDialog();
        if (requestCode == this.RC_SIGN_IN) {
            handleSignInResult(Auth.GoogleSignInApi.getSignInResultFromIntent(data));

        } else if (this.callbackManager != null) {
            this.callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleSignInResult(GoogleSignInResult completedTask) {
        StringBuilder sb = new StringBuilder();
        sb.append("Handle SignIn Result:");
        sb.append(completedTask.isSuccess());
        hideProgressDialog();
        try {
            if (completedTask.isSuccess()) {
                GoogleSignInAccount account = completedTask.getSignInAccount();
                
                if (account != null) {
                    if (!TextUtils.isEmpty(account.getDisplayName())) {
                        this.googleName = account.getDisplayName();
                    }
                    this.googleId = account.getId();
                    this.googleEmail = account.getEmail();
                    if (account.getPhotoUrl() != null) {
                        this.profile_image = account.getPhotoUrl().toString();
                    } else {
                        this.profile_image = String.valueOf(getResources().getDrawable(R.drawable.profile_logo));
                    }
                    checkUserExsist(this.googleId, this.googleName, this.googleEmail);
                   // sendGoogleDetailToServer(this.googleId, this.googleName, this.googleEmail);

                }

            }
            else{

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkUserExsist(String id, String googleName, String googleId) {
        this.object = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONObject object1 = new JSONObject();
        showProgressDialog();
        try {
            object1.put("email", googleId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.USER_EXIST, object1, new Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                LoginActivity.this.hideProgressDialog();
                PrintStream printStream = System.out;
                StringBuilder sb = new StringBuilder();
                sb.append("Aman 1 : ");
                sb.append(response);
                printStream.println(sb.toString());
                try {
                    if (response.getBoolean("status")) {
                        String string = response.getString(NotificationCompat.CATEGORY_MESSAGE);
                        JSONObject jsonObject = response.getJSONObject("User");
                        Preferences.setUserId(LoginActivity.this, jsonObject.getString(ShareConstants.WEB_DIALOG_PARAM_ID));
                        Preferences.setUserEmail(LoginActivity.this, jsonObject.getString("email"));
                        LoginActivity loginActivity = LoginActivity.this;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(jsonObject.getString("firstname"));
                        sb2.append(" ");
                        sb2.append(jsonObject.getString("lastname"));
                        Preferences.setUserName(loginActivity, sb2.toString());
                        Preferences.setUserChild(LoginActivity.this, jsonObject.getString("no_of_child"));
                       Preferences.setIsLoggedInBy(LoginActivity.this, Constant.GPLUS);
                        LoginActivity.this.startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        LoginActivity.this.finish();
                    }else{
                        sendGoogleDetailToServer(id,googleName,googleId);
                        Preferences.setUserImageUrl(getApplicationContext(), LoginActivity.this.profile_image);
//                        PrintStream printStream = System.out;
//                        StringBuilder sb2 = new StringBuilder();
//                        sb2.append("image");
//                        sb2.append(this.profile_image);
//                        printStream.println(sb2.toString());
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new ErrorListener() {

            public void onErrorResponse(VolleyError error) {
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    public void sendGoogleDetailToServer(String googleId2, String googleName2, String googleEmail2) {
        this.object = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONObject object1 = new JSONObject();
        showProgressDialog();
        try {
            object1.put("socialid", googleId2);
            object1.put("fname", googleName2);
            object1.put("lname", ".");
            object1.put("email", googleEmail2);
            object1.put("token", this.token);
            object1.put("image", this.profile_image);
            jsonArray.put(object1);
            this.object.put("user", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.SOCIAL_LOGIN_URL, this.object, new Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                LoginActivity.this.hideProgressDialog();
                PrintStream printStream = System.out;
                StringBuilder sb = new StringBuilder();
                sb.append("Aman 1 : ");
                sb.append(response);
                printStream.println(sb.toString());
                try {
                    if (response.getBoolean("status")) {
                        String string = response.getString(NotificationCompat.CATEGORY_MESSAGE);
                        JSONObject jsonObject = response.getJSONObject("User");
                        Preferences.setUserId(LoginActivity.this, jsonObject.getString(ShareConstants.WEB_DIALOG_PARAM_ID));
                        Preferences.setUserEmail(LoginActivity.this, jsonObject.getString("email"));
                        LoginActivity loginActivity = LoginActivity.this;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(jsonObject.getString("firstname"));
                        sb2.append(" ");
                        sb2.append(jsonObject.getString("lastname"));
                        Preferences.setUserName(loginActivity, sb2.toString());
                        Preferences.setUserChild(LoginActivity.this, jsonObject.getString("no_of_child"));
                        Preferences.setIsLoggedInBy(LoginActivity.this, Constant.GPLUS);
                        LoginActivity.this.startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        LoginActivity.this.finish();
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new ErrorListener() {

            public void onErrorResponse(VolleyError error) {
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    private void facebookSignIn() {
//        this.faceBookLoginBT.setReadPermissions(Arrays.asList("email"));

        this.callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(this.callbackManager, new FacebookCallback<LoginResult>() {
            public void onSuccess(LoginResult loginResult) {
                Log.e("Successfully", String.valueOf(loginResult));
                LoginActivity.this.getFBInformation(loginResult.getAccessToken());
            }

            public void onCancel() {
            }

            public void onError(FacebookException exception) {
                Log.e("Error", String.valueOf(exception));
//                if (exception instanceof FacebookAuthorizationException) {
//                    if (AccessToken.getCurrentAccessToken() != null) {
//                        LoginManager.getInstance().logOut();
//                    }
//                }
            }
        });
        LoginManager.getInstance().logInWithReadPermissions((Activity) this,
                (Collection<String>) Arrays.asList(new String[]{"email", "public_profile"}));
//        LoginManager.getInstance().logInWithReadPermissions(
//               (Activity) this,
//                Arrays.asList("email"));
    }

    /* access modifiers changed from: private */
    public void getFBInformation(AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphJSONObjectCallback() {
            public void onCompleted(JSONObject object, GraphResponse response) {
                if (object != null) {
                    Log.e("Profile Information===", object.toString());
                    if (object.has("first_name")) {
                        try {
                            LoginActivity.this.fbFName = object.getString("first_name");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    if (object.has("last_name")) {
                        try {
                            LoginActivity.this.fbLName = object.getString("last_name");
                        } catch (JSONException e2) {
                            e2.printStackTrace();
                        }
                    }
                    if (object.has("email")) {
                        try {
                            LoginActivity.this.fbEmail = object.getString("email");
                        } catch (JSONException e3) {
                            e3.printStackTrace();
                        }
                    }
                    if (object.has(ShareConstants.WEB_DIALOG_PARAM_ID)) {
                        try {
                            LoginActivity.this.fbId = object.getString(ShareConstants.WEB_DIALOG_PARAM_ID);
                        } catch (JSONException e4) {
                            e4.printStackTrace();
                        }
                    }
                    try {
                        LoginActivity loginActivity = LoginActivity.this;
                        StringBuilder sb = new StringBuilder();
                        sb.append("https://graph.facebook.com/");
                        sb.append(LoginActivity.this.fbId);
                        sb.append("/picture?type=large");
                        loginActivity.profile_image = String.valueOf(new URL(sb.toString()));
                    } catch (MalformedURLException e5) {
                        e5.printStackTrace();
                    }
                    LoginActivity.this.sendFBDetailToServer(LoginActivity.this.fbId, LoginActivity.this.fbFName, LoginActivity.this.fbLName, LoginActivity.this.fbEmail, LoginActivity.this.profile_image);
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(LoginActivity.this.fbFName);
                    sb2.append(" ");
                    sb2.append(LoginActivity.this.fbLName);
                    sb2.append(" ");
                    sb2.append(LoginActivity.this.fbEmail);
                    sb2.append(" ");
                    sb2.append(LoginActivity.this.fbId);
                    Log.e("Profile Information", sb2.toString());
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString(GraphRequest.FIELDS_PARAM, "id, first_name, last_name, email,gender, birthday, location");
        request.setParameters(parameters);
        request.executeAsync();
    }

    /* access modifiers changed from: private */
    public void sendFBDetailToServer(String fbId2, String fbFName2, String fbLName2, String fbEmail2, final String profile_img) {
        this.object = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONObject object1 = new JSONObject();
        try {
            object1.put("socialid", fbId2);
            object1.put("fname", fbFName2);
            object1.put("lname", fbLName2);
            object1.put("image", profile_img);
            if (fbEmail2 != null) {
                object1.put("email", fbEmail2);
            } else {
                object1.put("email", "");
            }
            object1.put("token", this.token);
            jsonArray.put(object1);
            this.object.put("user", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.SOCIAL_LOGIN_URL, this.object, new Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                PrintStream printStream = System.out;
                StringBuilder sb = new StringBuilder();
                sb.append("Aman : ");
                sb.append(response);
                printStream.println(sb.toString());
                try {
                    if (response.getBoolean("status")) {
                        String string = response.getString(NotificationCompat.CATEGORY_MESSAGE);
                        JSONObject jsonObject = response.getJSONObject("User");
                        Preferences.setUserId(LoginActivity.this, jsonObject.getString(ShareConstants.WEB_DIALOG_PARAM_ID));
                        Preferences.setUserEmail(LoginActivity.this, jsonObject.getString("email"));
                        LoginActivity loginActivity = LoginActivity.this;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(jsonObject.getString("firstname"));
                        sb2.append(" ");
                        sb2.append(jsonObject.getString("lastname"));
                        Preferences.setUserName(loginActivity, sb2.toString());
                        Preferences.setUserChild(LoginActivity.this, jsonObject.getString("no_of_child"));
                        Preferences.setIsLoggedInBy(LoginActivity.this, Constant.FBLOGIN);
                        Preferences.setUserImageUrl(LoginActivity.this, profile_img);
                        PrintStream printStream2 = System.out;
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("aaaaaaaaa :");
                        sb3.append(profile_img);
                        printStream2.println(sb3.toString());;
                        LoginActivity.this.startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        LoginActivity.this.finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new ErrorListener() {
            public void onErrorResponse(VolleyError error) {
            }

        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }
}
