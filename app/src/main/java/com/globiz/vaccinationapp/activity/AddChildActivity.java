package com.globiz.vaccinationapp.activity;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore.Images.Media;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.ItemTouchHelper;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.internal.ServerProtocol;
import com.facebook.share.internal.ShareConstants;

import com.globiz.vaccinationapp.R;
import com.globiz.vaccinationapp.controller.AppController;
import com.globiz.vaccinationapp.customViews.CircularImageView;
import com.globiz.vaccinationapp.util.Constant;
import com.globiz.vaccinationapp.util.Functions;
import com.globiz.vaccinationapp.util.Preferences;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AddChildActivity extends AppCompatActivity implements OnClickListener {
    private int PICK_IMAGE_REQUEST = 1;
    /* access modifiers changed from: private */
    public Bitmap bitmap;
    CircularImageView changePic;
    /* access modifiers changed from: private */
    public String childAadhar;
    private EditText childAadharET;
    public Spinner bloodGroup;
    public String bloodGroupValue;
    /* access modifiers changed from: private */
    public String childDOB;
    /* access modifiers changed from: private */
    public EditText childDobET;
    /* access modifiers changed from: private */
    public String childFName;
    private EditText childFNameET;
    RadioButton childFemale;
    CircularImageView childImage;
    public RadioButton btnFather;
    public RadioButton btnMother;
    /* access modifiers changed from: private */
    public String childLName;
    private EditText childLNameET;
    public ImageButton aadharInfo;
    /* access modifiers changed from: private */
    public String childMName;
    private EditText childMNameET;
    RadioButton childMale;
    /* access modifiers changed from: private */
    public SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
    /* access modifiers changed from: private */
    public DatePickerDialog datePickerDialog;
    /* access modifiers changed from: private */
    public ProgressDialog loading;
    /* access modifiers changed from: private */
    public JSONObject object;
    /* access modifiers changed from: private */
    public String placeBirth;
    private EditText placeBirthET;
    private RadioGroup radioGroup;
    private  RadioGroup radioRelation;
    /* access modifiers changed from: private */
    public String radiobutton_status = "";
    String radiobutton_relation = "";
    Button saveBT;
    private Toolbar toolbar;
    Calendar newCalendar;
    public  List<String> list=new ArrayList<>();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_add_child);
       newCalendar = Calendar.getInstance();
        initViews();
    }

    private void initViews() {
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(this.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle((CharSequence) "Add Child");
        this.childImage = (CircularImageView) findViewById(R.id.childProfilePic);
        this.changePic = (CircularImageView) findViewById(R.id.changePic);
        this.childFNameET = (EditText) findViewById(R.id.edtFName);
        this.childLNameET = (EditText) findViewById(R.id.edtLName);
        this.bloodGroup = (Spinner) findViewById(R.id.bloodGroup);
        this.childMNameET = (EditText) findViewById(R.id.motherName);
        this.placeBirthET = (EditText) findViewById(R.id.placeBirth);
        this.placeBirthET.setOnEditorActionListener(new OnEditorActionListener() {
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == 5) {
                    AddChildActivity.this.datePickerDialog();
                }
                return false;
            }
        });
        this.childDobET = (EditText) findViewById(R.id.edtBirthdate);
        this.childAadharET = (EditText) findViewById(R.id.childAadharET);
        this.aadharInfo = findViewById(R.id.aadharInfo);
//        this.childAadharET.setFormat("---- ---- ----");
        this.childAadharET.addTextChangedListener(new TextWatcher() {
            private static final char space = ' ';
            private static final char ch = ' ';
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (start == 0){
                    if( s.toString().equalsIgnoreCase("0") || s.toString().equalsIgnoreCase("1")){
                        Functions.setErrorEditText(AddChildActivity.this.childAadharET, "Aadhar Number should not start with 0 and 1");
                    }
                }else if(start != 13){
                    Functions.setErrorEditText(AddChildActivity.this.childAadharET, "Aadhar Number can't be Empty. Upto 12 digits");
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {

                // Remove all spacing char
                int pos = 0;

                while (true) {
                    Log.e("POSTION OF CHAR", String.valueOf(s));
                    if (pos >= s.length()) break;
                    if (space == s.charAt(pos) && (((pos + 1) % 5) != 0 || pos + 1 == s.length())) {
                        s.delete(pos, pos + 1);
                    } else {
                        pos++;
                    }
                }

                // Insert char where needed.
                pos = 4;
                while (true) {
                    if (pos >= s.length()) break;
                    final char c = s.charAt(pos);

                    // Only if its a digit where there should be a space we insert a space
                    if ("0123456789".indexOf(c) >= 0) {
                        s.insert(pos, "" + space);
                    }
                    pos += 5;
                }
            }
        });
        this.saveBT = (Button) findViewById(R.id.btnSave);
        this.childMale = (RadioButton) findViewById(R.id.btnAddChildMale);
        this.childFemale = (RadioButton) findViewById(R.id.btnAddChildFemale);
        this.radioGroup = (RadioGroup) findViewById(R.id.genderGroupAddChild);

        this.changePic.setOnClickListener(this);
        this.saveBT.setOnClickListener(this);
        this.childDobET.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() != 1) {
                    return false;
                }
                if (event.getRawX() >= ((float) (AddChildActivity.this.childDobET.getRight() - AddChildActivity.this.childDobET.getCompoundDrawables()[2].getBounds().width()))) {
                    AddChildActivity.this.datePickerDialog();
                }
                return true;
            }
        });
        this.aadharInfo.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
               aadharInfo.setBackgroundTintList(getResources().getColorStateList(R.color.white));


                Toast.makeText(AddChildActivity.this, "Please enter child Aadhar number -- it is just to track all children's vaccines for future purpose", Toast.LENGTH_SHORT).show();
                return true;
            }
        });
//        this.childAadharET.setOnTouchListener(new OnTouchListener() {
//            public boolean onTouch(View v, MotionEvent event) {
////                if (event.getAction() != 1) {
////                    return false;
////                }
//                if (event.getRawX() >= ((float) (AddChildActivity.this.childDobET.getRight() - AddChildActivity.this.childDobET.getCompoundDrawables()[2].getBounds().width()))) {
//
//                }
//                return true;
//            }
//        });

        this.getList();

        this.childMale.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                AddChildActivity.this.childMale.setChecked(true);
                AddChildActivity.this.childFemale.setChecked(false);
                AddChildActivity.this.radiobutton_status = AppEventsConstants.EVENT_PARAM_VALUE_NO;
            }
        });
        this.childFemale.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                AddChildActivity.this.childMale.setChecked(false);
                AddChildActivity.this.childFemale.setChecked(true);
                AddChildActivity.this.radiobutton_status = AppEventsConstants.EVENT_PARAM_VALUE_YES;
            }
        });
    }

    private void getList() {

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, Constant.CHILD_BLOOD_GROUPS, null ,new Listener<JSONObject>() {
                public void onResponse(JSONObject response) {
                    try {
                        boolean status = response.getBoolean("status");
                        String string = response.getString(NotificationCompat.CATEGORY_MESSAGE);
                        if (status) {
                            JSONArray jsonArray =response.getJSONArray("blood_groups");
                            ArrayList<JSONObject> bloodDetail = new ArrayList<>();
                            for(int i = 0; i< jsonArray.length(); i++){
                                JSONObject  jsonObject = jsonArray.getJSONObject(i);
                                bloodDetail.add(jsonObject.getJSONObject("BloodGroup"));
                            }
                            AddChildActivity.this.list.add("Select Blood Group");
                            for(int i = 0; i< bloodDetail.size(); i++){
                                JSONObject  jsonObject = bloodDetail.get(i);
                                AddChildActivity.this.list.add(jsonObject.getString("name"));
                            }
                            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(AddChildActivity.this,
                                    R.layout.simple_spinner_dropdown_item2, AddChildActivity.this.list);
                            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            AddChildActivity.this.bloodGroup.setAdapter(spinnerArrayAdapter);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new ErrorListener() {
                public void onErrorResponse(VolleyError error) {
                    String errortype = null;
                    if ((error instanceof TimeoutError) || (error instanceof NoConnectionError)) {
                        errortype = "No Internet Access, check your internet connection.";
                    } else if (error instanceof AuthFailureError) {
                        errortype = "Authentication error, please try again.";
                    } else if (error instanceof ServerError) {
                        errortype = "Server error, please try again.";
                    } else if (error instanceof NetworkError) {
                        errortype = "Network error, please try again.";
                    } else if (error instanceof ParseError) {
                        errortype = "Internal error, please try again.";
                    }
                    Toast.makeText(getApplicationContext(), errortype, Toast.LENGTH_SHORT).show();
                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjectRequest);
        }


    /* access modifiers changed from: private */
    public void datePickerDialog() {

        DatePickerDialog datePickerDialog2 = new DatePickerDialog(this,R.style.DialogTheme, new OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                AddChildActivity.this.datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                newCalendar.set(year, monthOfYear, dayOfMonth);
                String selectdate = AddChildActivity.this.dateFormatter.format(newCalendar.getTime());
                String currentDateandTime = new SimpleDateFormat("dd-MMM-yyyy").format(new Date());
                try {
                    if (AddChildActivity.this.dateFormatter.parse(selectdate).before(AddChildActivity.this.dateFormatter.parse(currentDateandTime))) {
                        AddChildActivity.this.childDobET.setText(AddChildActivity.this.dateFormatter.format(newCalendar.getTime()));
                    } else if (AddChildActivity.this.dateFormatter.parse(selectdate).equals(AddChildActivity.this.dateFormatter.parse(currentDateandTime))) {
                        AddChildActivity.this.childDobET.setText(AddChildActivity.this.dateFormatter.format(newCalendar.getTime()));
                    } else if (AddChildActivity.this.dateFormatter.parse(selectdate).after(AddChildActivity.this.dateFormatter.parse(currentDateandTime))) {
                        final Dialog dialog = new Dialog(AddChildActivity.this);
                        dialog.setContentView(R.layout.dob_validate_dialog);
                        ((Button) dialog.findViewById(R.id.okBtn)).setOnClickListener(new OnClickListener() {
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                        dialog.show();
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        this.datePickerDialog = datePickerDialog2;
        this.datePickerDialog.show();
    }
    public boolean isAlpha(String s) {
        return s != null && s.matches("^[a-zA-z]+([\\s][a-zA-Z]+)*$");
    }
    private boolean isValidInput() {
        this.childFNameET.setError(null);
        this.childLNameET.setError(null);
        this.childMNameET.setError(null);
        this.childDobET.setError(null);
        this.placeBirthET.setError(null);
        this.childAadharET.setError(null);
        this.childFName = this.childFNameET.getText().toString().trim();
        this.childLName = this.childLNameET.getText().toString().trim();
        this.childMName = this.childMNameET.getText().toString().trim();
        this.childDOB = this.childDobET.getText().toString().trim();
        this.placeBirth = this.placeBirthET.getText().toString().trim();
        this.childAadhar = this.childAadharET.getText().toString().trim();
       if(this.bloodGroup.getSelectedItem().toString()!="Select Blood Group"){
           this.bloodGroupValue =   this.bloodGroup.getSelectedItem().toString();
        }else {  this.bloodGroupValue ="";}
        char ch=' ';
        if(!TextUtils.isEmpty(this.childAadhar)){
            ch=this.childAadharET.getText().toString().charAt(0);
        }
        if (TextUtils.isEmpty(this.childFName)) {
            Functions.setErrorEditText(this.childFNameET, "Child First Name can't be Empty");
            return false;
        }else if (!isAlpha(this.childFName)) {
            Functions.setErrorEditText(this.childFNameET, "Invalid Name");
            return false;
        } else if (TextUtils.isEmpty(this.childLName)) {
            Functions.setErrorEditText(this.childLNameET, "Child Last Name can't be Empty");
            return false;
        } else if (!isAlpha(this.childLName)) {
            Functions.setErrorEditText(this.childLNameET, "Invalid Name");
            return false;
        } else if (TextUtils.isEmpty(this.childMName)) {
            Functions.setErrorEditText(this.childMNameET, "Child Mother Name can't be Empty");
            return false;
        } else if (!isAlpha(this.childMName)) {
            Functions.setErrorEditText(this.childMNameET, "Invalid Name");
            return false;
        } else if (TextUtils.isEmpty(this.placeBirth)) {
            Functions.setErrorEditText(this.placeBirthET, "Place of Birth can't be Empty");
            return false;
        }else if (!isAlpha(this.placeBirth)) {
            Functions.setErrorEditText(this.placeBirthET, "Invalid Name");
            return false;
        } else if (TextUtils.isEmpty(this.childDOB)) {
            Functions.setErrorEditText(this.childDobET, "Child DOB can't be Empty");
            return false;
        } else if (ch=='0' || ch== '1') {
            Functions.setErrorEditText(this.childAadharET, "Aadhar Number should not start with 0 and 1");
            return false;
        }

        else if (!TextUtils.isEmpty(this.childAadhar) && this.childAadhar.length() != 14) {
            Functions.setErrorEditText(this.childAadharET, "Aadhar Number can't be Empty. Upto 12 digits");
            return false;
        }

        else if (TextUtils.isEmpty(this.bloodGroupValue)) {
            Toast.makeText(getApplicationContext(), "Please select the blood group of child", Toast.LENGTH_SHORT).show();
            return false;
        }  else if (this.radioGroup.getCheckedRadioButtonId() != -1) {
            return true;
        } else {
            Toast.makeText(getApplicationContext(), "Please select Gender", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    public String getStringImage(Bitmap bmp) {
        Bitmap bmp2;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if (bmp != null) {
            bmp.compress(CompressFormat.JPEG, 100, baos);
            byte[] imageBytes = baos.toByteArray();
            String encodedImage = Base64.encodeToString(imageBytes, 0);
            bmp2 = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
            if (bmp2.getHeight() <= 400 && bmp2.getWidth() <= 400) {
                return encodedImage;
            }
        } else {
            bmp2 = ((BitmapDrawable) this.childImage.getDrawable()).getBitmap();
        }
        Bitmap bmp3 = Bitmap.createScaledBitmap(bmp2, 500, 500, false);
        byte[] b = baos.toByteArray();
        System.gc();
        return Base64.encodeToString(b, 2);
    }

    public void addChild() {
        if (isValidInput()) {

            new AsyncTask<String, String, String>() {
                /* access modifiers changed from: protected */
                public String doInBackground(String... params) {
                    AddChildActivity.this.object = new JSONObject();
                    JSONArray jsonArray = new JSONArray();
                    JSONObject jsonObject = new JSONObject();
                    String image = AddChildActivity.this.getStringImage(AddChildActivity.this.bitmap);
                    try {
                        jsonObject.put("first_name", AddChildActivity.this.childFName);
                        jsonObject.put("last_name", AddChildActivity.this.childLName);
                        jsonObject.put("mother_name", AddChildActivity.this.childMName);
                        jsonObject.put("place_birth", AddChildActivity.this.placeBirth);
                        jsonObject.put("parent_id", Preferences.getUserId(AddChildActivity.this));
                        jsonObject.put("dob", AddChildActivity.this.childDOB);
                        jsonObject.put("gender", AddChildActivity.this.radiobutton_status);
                        jsonObject.put("aadhar", AddChildActivity.this.childAadhar);
                        jsonObject.put("blood_group",AddChildActivity.this.bloodGroupValue);
                        jsonObject.put("image", image);
                        jsonArray.put(jsonObject);
                        AddChildActivity.this.object.put("child", jsonArray);
                        Log.e("response : ", AddChildActivity.this.object.toString());
                        PrintStream printStream = System.out;
                        StringBuilder sb = new StringBuilder();
                        sb.append("response1 : ");
                        sb.append(AddChildActivity.this.object.toString());
                        printStream.println(sb.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return image;
                }

                /* access modifiers changed from: protected */
                public void onPreExecute() {
                    super.onPreExecute();
                    AddChildActivity.this.loading = new ProgressDialog(AddChildActivity.this,R.style.DialogTheme);
//                    AddChildActivity.this.loading.setMessage("Please wait...");
                    AddChildActivity.this.loading.setMessage("Loading...");
                    AddChildActivity.this.loading.show();
                }

                /* access modifiers changed from: protected */
                public void onPostExecute(String s) {
                    super.onPostExecute(s);
                    Log.e("ADD CHILD REQUEST", String.valueOf(AddChildActivity.this.object));
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.ADD_CHILD_URL, AddChildActivity.this.object, new Listener<JSONObject>() {
                        public void onResponse(JSONObject response) {
                            try {
                                boolean status = response.getBoolean("status");
                                String msg = response.getString(NotificationCompat.CATEGORY_MESSAGE);
                                if (status) {
                                    AddChildActivity.this.loading.dismiss();
                                    PrintStream printStream = System.out;
                                    StringBuilder sb = new StringBuilder();
                                    sb.append("Response 1 :");
                                    sb.append(response);
                                    printStream.println(sb.toString());
                                  Toast.makeText(AddChildActivity.this, msg, Toast.LENGTH_LONG).show();
                                    Preferences.setChildId(AddChildActivity.this, response.getString(ShareConstants.WEB_DIALOG_PARAM_ID));
                                    Preferences.setChildFName(AddChildActivity.this, response.getString("first_name"));
                                    Preferences.setChildFName(AddChildActivity.this, response.getString("last_name"));
                                    Preferences.setChildDob(AddChildActivity.this, response.getString("dob"));
                                    Preferences.setChildGender(AddChildActivity.this, response.getString("gender"));
                                    if (response.getString("image_name").equals(" ")) {
                                        Preferences.setChildImageUrl(AddChildActivity.this, response.getString("image_name"));
                                    }
                                    AddChildActivity.this.finish();
                                    AddChildActivity.this.saveBT.setVisibility(View.VISIBLE);
                                    return;
                                }else{
                                    AddChildActivity.this.loading.dismiss();
                                    AddChildActivity.this.saveBT.setVisibility(View.VISIBLE);
                                    Toast.makeText(AddChildActivity.this, msg, Toast.LENGTH_LONG).show();
                                }
                            //    Toast.makeText(AddChildActivity.this, msg, Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new ErrorListener() {
                        public void onErrorResponse(VolleyError error) {
                            AddChildActivity.this.loading.dismiss();
                           Toast.makeText(AddChildActivity.this, (CharSequence) error, Toast.LENGTH_LONG).show();
                            error.printStackTrace();
                        }
                    });
                    AppController.getInstance().addToRequestQueue(jsonObjectRequest);
                }

            }.execute(new String[0]);
        }else {
            AddChildActivity.this.saveBT.setVisibility(View.VISIBLE);
        }
    }

    private void showFileChooser() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{"android.permission.WRITE_EXTERNAL_STORAGE"}, 1);
                requestPermissions(new String[]{"android.permission.READ_EXTERNAL_STORAGE"}, 1);
            } else if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_DENIED) {

                Intent intent = new Intent("android.intent.action.PICK", Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                intent.putExtra("crop", ServerProtocol.DIALOG_RETURN_SCOPES_TRUE);
                intent.putExtra("outputX", ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION);
                intent.putExtra("outputY", ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION);
                intent.putExtra("aspectX", 1);
                intent.putExtra("aspectY", 1);
                intent.putExtra("scale", true);
                intent.putExtra("output", true);
                intent.putExtra("outputFormat", CompressFormat.JPEG.toString());
                intent.setAction("android.intent.action.GET_CONTENT");
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), this.PICK_IMAGE_REQUEST);
            }}
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == this.PICK_IMAGE_REQUEST && resultCode == -1 && data != null && data.getData() != null) {
            try {
                this.bitmap = Media.getBitmap(getContentResolver(), data.getData());
                this.childImage.setImageBitmap(this.bitmap);
                Toast.makeText(AddChildActivity.this, "Photo Added Successfully", Toast.LENGTH_LONG).show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void onClick(View v) {
        int id = v.getId();
        if (id != R.id.btnSave) {
            if (id == R.id.changePic) {
                if (Functions.isNetworkConnected(this)) {
                    showFileChooser();
                } else {
                    Toast.makeText(this, getString(R.string.check_your_internet_connectivity), Toast.LENGTH_SHORT).show();
                }
            }
        } else if (Functions.isNetworkConnected(this)) {
            this.saveBT.setVisibility(View.GONE);
            addChild();
        } else {
            Toast.makeText(this, R.string.check_your_internet_connectivity, Toast.LENGTH_SHORT).show();
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 16908332) {
            finish();
        }
        return true;
    }
}
