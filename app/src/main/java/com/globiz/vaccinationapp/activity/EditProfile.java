package com.globiz.vaccinationapp.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore.Images.Media;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.ItemTouchHelper;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import com.android.volley.toolbox.Volley;
import com.facebook.internal.ServerProtocol;
import com.facebook.share.internal.ShareConstants;
import com.globiz.vaccinationapp.R;
import com.globiz.vaccinationapp.controller.AppController;
import com.globiz.vaccinationapp.customViews.CaseSensitiveLetters;
import com.globiz.vaccinationapp.customViews.CircularImageView;
import com.globiz.vaccinationapp.util.Constant;
import com.globiz.vaccinationapp.util.Functions;
import com.globiz.vaccinationapp.util.Preferences;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class EditProfile extends AppCompatActivity implements OnClickListener {
    private int PICK_IMAGE_REQUEST = 100;
    private String aadhar;
    EditText aadharET;
   // EditText aadharET;
    String address;
    EditText addressET;
    private Bitmap bitmap;
    String contact;
    CaseSensitiveLetters convert;
    public RadioButton btnFather;
    public RadioButton btnMother;
    public RadioButton btnGuardian;
    private  RadioGroup radioRelation;
    /* access modifiers changed from: private */
    public Context context;
    private String email;
    private EditText emailET;
    private String fName;
    private String finalImageUrl;
    private String lName;
    private Dialog loading;
    private ProgressDialog mProgressDialog;
    String name;
    private EditText no_of_ChildET;
    private String no_of_child;
    private JSONObject object;
    private EditText phoneNoET;
    private Button saveBT;
    private String selectedFilePath;
    private Toolbar toolbar;
    CircularImageView uploadImage;
    CircularImageView userImage;
    private EditText userLNameET;
    private EditText userNameET;
    public  String gender;
    private String radiobutton_relation;
    private ImageButton aadharInfo;

    /* access modifiers changed from: protected */
    @SuppressLint("LongLogTag")
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_edit_profile);
        this.context = this;
        convert=new CaseSensitiveLetters();

        initViews();
        this.finalImageUrl = Preferences.getUserImageUrl(this);
//        Toast.makeText(context, "PROFILE IMAGE URL"+this.finalImageUrl, Toast.LENGTH_SHORT).show();
        if (TextUtils.isEmpty(Preferences.getUserImageUrl(this.context))) {
            return;
        }
        if (this.finalImageUrl.contains(Constant.BASE_IMAGE_URL)) {
            if(EditProfile.this.gender.matches("Female")){
                Picasso.with(this).load(this.finalImageUrl).placeholder((int) R.drawable.profile_logo1).into((ImageView) this.userImage);
            }else {
                Picasso.with(this).load(this.finalImageUrl).placeholder((int) R.drawable.profile_logo).into((ImageView) this.userImage);
            }
        } else if (this.finalImageUrl.contains("google")) {
            if(EditProfile.this.gender.matches("Female")){
                Picasso.with(this).load(this.finalImageUrl).placeholder((int) R.drawable.profile_logo1).into((ImageView) this.userImage);
            }else {
                Picasso.with(this).load(this.finalImageUrl).placeholder((int) R.drawable.profile_logo).into((ImageView) this.userImage);
            }
        } else if (this.finalImageUrl.contains("fb") || this.finalImageUrl.contains("facebook")) {
                if(EditProfile.this.gender.matches("Female")){
                    Picasso.with(this).load(this.finalImageUrl).placeholder((int) R.drawable.profile_logo1).into((ImageView) this.userImage);
            }else {
                    Picasso.with(this).load(this.finalImageUrl).placeholder((int) R.drawable.profile_logo).into((ImageView) this.userImage);
                }
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append(Constant.BASE_IMAGE_URL);
            sb.append("profileimg/");
            sb.append(this.finalImageUrl);
            this.finalImageUrl = sb.toString();
            if(EditProfile.this.gender.matches("Female")) {
                Picasso.with(this).load(this.finalImageUrl).placeholder((int) R.drawable.profile_logo1).into((ImageView) this.userImage);
            }else{
                Picasso.with(this).load(this.finalImageUrl).placeholder((int) R.drawable.profile_logo).into((ImageView) this.userImage);
            }
        }
    }

    private void initViews() {
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(this.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle((CharSequence) "Edit Profile");
        this.userNameET = (EditText) findViewById(R.id.userlnameET);
        this.userNameET.setText(convert.caseSensitive(Preferences.getUserFName(this)));
        this.userLNameET = (EditText) findViewById(R.id.usernameET);
        this.userLNameET.setText(convert.caseSensitive(Preferences.getUserLName(this)));
        this.emailET = (EditText) findViewById(R.id.emailET);
        this.emailET.setText(Preferences.getUserEmail(this));
        this.phoneNoET = (EditText) findViewById(R.id.primareMnoET);
        this.phoneNoET.setText(Preferences.getUserPhone(this));
        this.addressET = (EditText) findViewById(R.id.addressET);
        this.addressET.setText(Preferences.getUserAddress(this));
        this.aadharET = (EditText) findViewById(R.id.aadharET);
        this.aadharET.setText(Preferences.getUserAadhar(this));
        this.aadharET.addTextChangedListener(new TextWatcher() {
            private static final char space = ' ';
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (start == 0){
                    if( s.toString().equalsIgnoreCase("0") || s.toString().equalsIgnoreCase("1")){
                        Functions.setErrorEditText(EditProfile.this.aadharET, "Aadhar Number should not start with 0 and 1");
                    }
                }else if(start != 13){
                    Functions.setErrorEditText(EditProfile.this.aadharET, "Aadhar Number can't be Empty. Upto 12 digits");
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {

                // Remove all spacing char
                int pos = 0;

                while (true) {

                    if (pos >= s.length()) break;
                    if (space == s.charAt(pos) && (((pos + 1) % 5) != 0 || pos + 1 == s.length())) {
                        s.delete(pos, pos + 1);
                    } else {
                        pos++;
                    }

                }

                // Insert char where needed.
                pos = 4;
                while (true) {
                    if (pos >= s.length()) break;
                    final char c = s.charAt(pos);

                    // Only if its a digit where there should be a space we insert a space
                    if ("0123456789".indexOf(c) >= 0) {
                        s.insert(pos, "" + space);
                    }
                    pos += 5;
                }
            }
        });

        this.no_of_ChildET = (EditText) findViewById(R.id.no_of_child);
        this.no_of_ChildET.setText(Preferences.getUserChild(this));
        this.gender=Preferences.getUserGender(this);
        this.saveBT = (Button) findViewById(R.id.saveBT);
        this.userImage = (CircularImageView) findViewById(R.id.userProfilePic);
        this.uploadImage = (CircularImageView) findViewById(R.id.changeDisplayPic);
        this.uploadImage.setOnClickListener(this);
        this.radioRelation = (RadioGroup) findViewById(R.id.relationGroupP);
        this.aadharInfo = (ImageButton) findViewById(R.id.imgIcon);
        this.btnFather = (RadioButton) findViewById(R.id.btnFatherP);
        this.btnMother = (RadioButton) findViewById(R.id.btnMotherP);
        this.btnGuardian = (RadioButton) findViewById(R.id.btnGuardianP);
        this.radiobutton_relation = Preferences.getUserRelation(this);
        if((this.radiobutton_relation).equalsIgnoreCase("father")){
            EditProfile.this.btnFather.setChecked(true);
           EditProfile.this.btnMother.setChecked(false);
            EditProfile.this.btnGuardian.setChecked(false);
            EditProfile.this.radiobutton_relation = "father";
        }else if(this.radiobutton_relation.equalsIgnoreCase("mother")){
           EditProfile.this.btnFather.setChecked(false);
            EditProfile.this.btnMother.setChecked(true);
            EditProfile.this.btnGuardian.setChecked(false);
            EditProfile.this.radiobutton_relation = "mother";
        }else if(this.radiobutton_relation.equalsIgnoreCase("guardian")){
            EditProfile.this.btnFather.setChecked(false);
            EditProfile.this.btnMother.setChecked(false);
            EditProfile.this.btnGuardian.setChecked(true);
            EditProfile.this.radiobutton_relation = "guardian";
        }else{
            EditProfile.this.btnFather.setChecked(false);
            EditProfile.this.btnMother.setChecked(false);
        }
        this.btnFather.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                EditProfile.this.btnFather.setChecked(true);
                EditProfile.this.btnMother.setChecked(false);
                EditProfile.this.btnGuardian.setChecked(false);
                EditProfile.this.radiobutton_relation = "father";
            }
        });
        this.btnMother.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                EditProfile.this.btnFather.setChecked(false);
                EditProfile.this.btnMother.setChecked(true);
                EditProfile.this.btnGuardian.setChecked(false);
                EditProfile.this.radiobutton_relation = "mother";
            }
        });
        this.btnGuardian.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                EditProfile.this.btnFather.setChecked(false);
                EditProfile.this.btnMother.setChecked(false);
                EditProfile.this.btnGuardian.setChecked(true);
                EditProfile.this.radiobutton_relation = "guardian";
            }
        });
        this.aadharInfo.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Toast.makeText(EditProfile.this, "Please enter your Aadhar number -- it is just to track all children's vaccines for future purpose", Toast.LENGTH_SHORT).show();
                return true;
            }
        });
        this.saveBT.setOnClickListener(this);

    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, 0);
        Bitmap bmp2 = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
        if (bmp2.getHeight() <= 400 && bmp2.getWidth() <= 400) {
            return encodedImage;
        }
        Bitmap bmp3 = Bitmap.createScaledBitmap(bmp2, 500, 500, false);
        byte[] b = baos.toByteArray();
        System.gc();
        return Base64.encodeToString(b, 2);
    }

    private void showProgressDialog() {
        if (this.mProgressDialog == null) {
            this.mProgressDialog = new ProgressDialog(this,R.style.DialogTheme);
            this.mProgressDialog.setMessage(getString(R.string.loading));
            this.mProgressDialog.setIndeterminate(true);
        }
        this.mProgressDialog.show();
    }

    /* access modifiers changed from: private */
    public void hideProgressDialog() {
        if (this.mProgressDialog != null && this.mProgressDialog.isShowing()) {
            this.mProgressDialog.hide();
        }
    }

    private void uploadImage() {
        showProgressDialog();
        this.object = new JSONObject();
        String image = getStringImage(this.bitmap);
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("image", image);
            jsonObject.put(ShareConstants.WEB_DIALOG_PARAM_ID, Preferences.getUserId(this));
            jsonArray.put(jsonObject);
            this.object.put("user", jsonArray);
            Log.e("response : ", this.object.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.UPLOADIMAGE_URL, this.object, new Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                try {
                    boolean status = response.getBoolean("status");
                    String msg = response.getString(NotificationCompat.CATEGORY_MESSAGE);
                    if (status) {
                        EditProfile.this.hideProgressDialog();
                        PrintStream printStream = System.out;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Response s :");
                        sb.append(response);
                        printStream.println(sb.toString());
                        Preferences.setUserImageUrl(EditProfile.this, response.getString("image_name"));
                        Toast.makeText(EditProfile.this, "Photo Updated Successfully", Toast.LENGTH_LONG).show();
                        return;
                    }
                    Toast.makeText(EditProfile.this, msg, Toast.LENGTH_LONG).show();
                    EditProfile.this.hideProgressDialog();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                EditProfile.this.hideProgressDialog();
                Toast.makeText(EditProfile.this, error.getMessage(), Toast.LENGTH_LONG).show();
                error.printStackTrace();
            }
        });
        Volley.newRequestQueue(this).add(jsonObjectRequest);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(20000, 0, 1.0f));
    }

    /* access modifiers changed from: private */
    public void deleteImage() {
        showProgressDialog();
        this.object = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(ShareConstants.WEB_DIALOG_PARAM_ID, Preferences.getUserId(this));
            jsonArray.put(jsonObject);
            this.object.put("user", jsonArray);
            Log.e("response : ", this.object.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.DELETE_PROFILE_PICTURE, this.object, new Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                try {
                    boolean status = response.getBoolean("status");
                    String msg = response.getString(NotificationCompat.CATEGORY_MESSAGE);
                    if (status) {
                        EditProfile.this.hideProgressDialog();
                        PrintStream printStream = System.out;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Response s :");
                        sb.append(response);
                        printStream.println(sb.toString());
                        Preferences.setUserImageUrl(EditProfile.this, EditProfile.this.getURLForResource(R.drawable.profile_logo));
                        EditProfile.this.userImage.setImageDrawable(ContextCompat.getDrawable(EditProfile.this, R.drawable.profile_logo));
                        if(EditProfile.this.gender.matches("Female")) {
                            Picasso.with(EditProfile.this).load((int) R.drawable.profile_logo1).into((ImageView) EditProfile.this.userImage);
                        }else{
                            Picasso.with(EditProfile.this).load((int) R.drawable.profile_logo).into((ImageView) EditProfile.this.userImage);
                        }

                        Toast.makeText(EditProfile.this, "Photo deleted successfully", Toast.LENGTH_LONG).show();
                        return;
                    }
                    Toast.makeText(EditProfile.this, msg, Toast.LENGTH_LONG).show();
                    EditProfile.this.hideProgressDialog();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                EditProfile.this.hideProgressDialog();
                Toast.makeText(EditProfile.this, error.getMessage(), Toast.LENGTH_LONG).show();
                error.printStackTrace();
            }
        });
        Volley.newRequestQueue(this).add(jsonObjectRequest);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(20000, 0, 1.0f));
    }

    private String getFinalImageUrl() {
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream("drawable://2131165389");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        byte[] buffer = new byte[8192];
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        while (true) {
            try {
                int read = inputStream.read(buffer);
                int bytesRead = read;
                if (read == -1) {
                    break;
                }
                output.write(buffer, 0, bytesRead);
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        return Base64.encodeToString(output.toByteArray(), 0);
    }

    /* access modifiers changed from: private */
    public void showFileChooser() {
        if (Build.VERSION.SDK_INT >= 23) {
            if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
                requestPermissions(new String[]{"android.permission.WRITE_EXTERNAL_STORAGE"}, 1);
                requestPermissions(new String[]{"android.permission.READ_EXTERNAL_STORAGE"}, 1);
            }else if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_DENIED){
                        Intent intent = new Intent("android.intent.action.PICK", Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        intent.putExtra("crop", ServerProtocol.DIALOG_RETURN_SCOPES_TRUE);
        intent.putExtra("outputX", ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION);
        intent.putExtra("outputY", ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION);
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("scale", true);
        intent.putExtra("output", true);
        intent.putExtra("outputFormat", CompressFormat.JPEG.toString());
        intent.setAction("android.intent.action.GET_CONTENT");
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), this.PICK_IMAGE_REQUEST);
            }
        }

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == this.PICK_IMAGE_REQUEST && resultCode == -1 && data != null && data.getData() != null) {
            try {
                this.bitmap = Media.getBitmap(getContentResolver(), data.getData());
                this.userImage.setImageBitmap(this.bitmap);
                if (Functions.isNetworkConnected(this)) {
                    uploadImage();
                } else {
                    Toast.makeText(this, getString(R.string.check_your_internet_connectivity), Toast.LENGTH_LONG).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//        else{
//            Toast.makeText(this, "Permission Required For Upload Image", Toast.LENGTH_LONG).show();
//        }
    }

    private void dialogDeleteUpload() {
        String[] options = {"Upload Image", "Delete Image"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((CharSequence) "Choose an Option");
        builder.setCancelable(true);
        builder.setItems((CharSequence[]) options, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    EditProfile.this.showFileChooser();
                } else if (which == 1) {
                    EditProfile.this.deleteImage();
                }
            }
        });
        builder.show();
    }

    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.changeDisplayPic) {
            dialogDeleteUpload();
        } else if (id != R.id.saveBT || !Functions.isNetworkConnected(this)) {
        } else {
            if (isValidInput()) {
                saveEditProfileData();
            } else if (isValidInput()) {
                Toast.makeText(this, R.string.check_your_internet_connectivity, Toast.LENGTH_LONG).show();
            }
        }
    }
    public boolean isAlpha(String s) {
        return s != null && s.matches("^[a-zA-z]+([\\s][a-zA-Z]+)*$");
    }
    private boolean isValidInput() {
        this.userNameET.setError(null);
        this.userLNameET.setError(null);
        this.phoneNoET.setError(null);
        this.no_of_ChildET.setError(null);
        this.addressET.setError(null);
        this.aadharET.setError(null);
        this.name = this.userNameET.getText().toString().trim();
        this.lName = this.userLNameET.getText().toString().trim();
        this.email = this.emailET.getText().toString().trim();
        this.address = this.addressET.getText().toString().trim();
        this.aadhar = this.aadharET.getText().toString().trim();
        this.contact = this.phoneNoET.getText().toString().trim();
        this.no_of_child = this.no_of_ChildET.getText().toString().trim();
        char ch=' ';
        if(!TextUtils.isEmpty(this.aadhar)) {
            ch = this.aadharET.getText().toString().charAt(0);
        }
        if (TextUtils.isEmpty(this.name)) {
            Functions.setErrorEditText(this.userNameET, "First Name can't be Empty");
            return false;
        }else if (!isAlpha(this.name)) {
            Functions.setErrorEditText(this.userNameET, "Invalid Name");
            return false;
        } else if (TextUtils.isEmpty(this.lName)) {
            Functions.setErrorEditText(this.userLNameET, "Last Name can't be Empty");
            return false;
        }if (!isAlpha(this.lName)) {
            Functions.setErrorEditText(this.userLNameET, "Invalid Name");
            return false;
        } else if (TextUtils.isEmpty(this.email)) {
            Functions.setErrorEditText(this.emailET, "Email con't be Empty");
            return false;
        } else if (!Functions.isValidEmail(this.email)) {
            Functions.setErrorEditText(this.emailET, "Invalid Email");
            return false;
        } else if (TextUtils.isEmpty(this.contact)) {
            Functions.setErrorEditText(this.phoneNoET, "Mobile can't be Empty");
            return false;
        } else if (!Functions.isValidMobile(this.contact)) {
            Functions.setErrorEditText(this.phoneNoET, "Mobile no should be 10 digits");
            return false;
        } else if (TextUtils.isEmpty(this.address)) {
            Functions.setErrorEditText(this.addressET, "Address can't be Empty");
            return false;
        }else if (this.radioRelation.getCheckedRadioButtonId() == -1) {
            Toast.makeText(getApplicationContext(), "Please select your relationship with child", Toast.LENGTH_SHORT).show();
            return false;
        } else if (ch=='0' || ch== '1') {
            Functions.setErrorEditText(this.aadharET, "Aadhar Number should not start with 0 and 1");
            return false;
        }
        else if (TextUtils.isEmpty(this.aadhar) || this.aadharET.length() == 14) {
            return true;
        } else {
            Functions.setErrorEditText(this.aadharET, "Aadhar Number can't be Empty. Upto 12 digits");
            return false;
        }
    }

    private void saveEditProfileData() {
        JSONObject object2 = new JSONObject();
        try {
            JSONArray jsonArray = new JSONArray();
            JSONObject object1 = new JSONObject();
            object1.putOpt(ShareConstants.WEB_DIALOG_PARAM_ID, Preferences.getUserId(this));
            object1.putOpt("fname", this.name);
            object1.putOpt("lname", this.lName);
            object1.putOpt("email", this.email);
            object1.putOpt("address", this.address);
            object1.putOpt("phone", this.contact);
            object1.putOpt("no_of_child", this.no_of_child);
            object1.putOpt("aadhar", this.aadhar);
            object1.putOpt("gender",this.gender);
            object1.putOpt("relation",this.radiobutton_relation);
            Log.e("response", object1.toString());
            jsonArray.put(object1);
            object2.put("user", jsonArray);
            Log.e("response", object2.toString());
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.EDITPROFILE, object2, new Listener<JSONObject>() {
                public void onResponse(JSONObject response) {
                    try {
                        if (response.getBoolean("status")) {
                            System.out.println(response);
                            Toast.makeText(EditProfile.this, response.getString(NotificationCompat.CATEGORY_MESSAGE), Toast.LENGTH_LONG).show();
                            JSONObject jsonObject = response.getJSONObject("User");
                            Preferences.setUserId(EditProfile.this, jsonObject.getString(ShareConstants.WEB_DIALOG_PARAM_ID));
                            Preferences.setUserEmail(EditProfile.this, jsonObject.getString("email"));
                            EditProfile editProfile = EditProfile.this;
                            StringBuilder sb = new StringBuilder();
                            sb.append(jsonObject.getString("firstname"));
                            sb.append(" ");
                            sb.append(jsonObject.getString("lastname"));
                            Preferences.setUserName(editProfile, sb.toString());
                            Preferences.setUserPhone(EditProfile.this, jsonObject.getString("phone"));
                            Preferences.setUserFName(EditProfile.this, jsonObject.getString("firstname"));
                            Preferences.setUserLName(EditProfile.this, jsonObject.getString("lastname"));
                            Preferences.setUserAddress(EditProfile.this, jsonObject.getString("address"));
                            Preferences.setUserImageUrl(EditProfile.this, jsonObject.getString("image"));
                            Preferences.setUserChild(EditProfile.this, jsonObject.getString("no_of_child"));
                            if (!jsonObject.getString("aadhar").equalsIgnoreCase("null")) {
                                Preferences.setUserAadhar(EditProfile.this, jsonObject.getString("aadhar"));
                            } else {
                                Preferences.setUserAadhar(EditProfile.this, "");
                            }
                            EditProfile.this.finish();
                            return;
                        }
                        Toast.makeText(EditProfile.this, response.getString(NotificationCompat.CATEGORY_MESSAGE), Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new ErrorListener() {
                public void onErrorResponse(VolleyError error) {
                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjectRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 16908332) {
            finish();
        }
        return true;
    }

    public String getURLForResource(int resourceId) {
        StringBuilder sb = new StringBuilder();
        sb.append("android.resource://");
        sb.append(R.class.getPackage().getName());
        sb.append("/");
        sb.append(resourceId);
        return Uri.parse(sb.toString()).toString();
    }
}

