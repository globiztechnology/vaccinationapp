package com.globiz.vaccinationapp.activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import com.facebook.appevents.AppEventsConstants;
import com.globiz.vaccinationapp.R;
import com.globiz.vaccinationapp.adapter.ChildDetailAdapter;
import com.globiz.vaccinationapp.adapter.CompleteVaccineAdapter;
import com.globiz.vaccinationapp.controller.AppController;
import com.globiz.vaccinationapp.customViews.CircularImageView;
import com.globiz.vaccinationapp.database.DBVaccination;
import com.globiz.vaccinationapp.model.ChildDetailModel;
import com.globiz.vaccinationapp.model.ScheduleModel;
import com.globiz.vaccinationapp.util.Constant;
import com.globiz.vaccinationapp.util.Functions;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import java.io.PrintStream;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ChildDetailActivity extends AppCompatActivity {
    String aadharNumber;
    TextView aadharTV, bloodTV, relationTV;
    TextView childDOB;
    ChildDetailAdapter childDetailAdapter;
    private ArrayList<ChildDetailModel> childDetailList = new ArrayList<>();
    CircularImageView childImage;
    TextView childName;
    TextView childPlace;
    Context context;
    DBVaccination dbVaccination;
    String dob;

    /* renamed from: id */
    String f42id;
    String image;
    String gender;
    String relation;
    String bloodGroup;
    /* access modifiers changed from: private */
    public ProgressDialog loading;
    String mNAme;
    TextView motherName;
    String name;
    /* access modifiers changed from: private */
    public JSONObject object;
    String placeBirth;
    /* access modifiers changed from: private */
    public CompleteVaccineAdapter scheduleAdapter;
    /* access modifiers changed from: private */
    public ArrayList<ScheduleModel> scheduleArrayList = new ArrayList<>();
    ArrayList<ScheduleModel> tempScheduleModelArrayList = new ArrayList<>();
    private Toolbar toolbar;
    /* access modifiers changed from: private */
    public ListView vaccineLV;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_child_detail);
        this.context = this;
        this.dbVaccination = new DBVaccination(this.context);
        initViews();
    }

    private void initViews() {
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(this.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle((CharSequence) "Child Detail");
        this.childImage = (CircularImageView) findViewById(R.id.childImage);
        this.childName = (TextView) findViewById(R.id.nameTV);
        this.motherName = (TextView) findViewById(R.id.mNameTV);
        this.childDOB = (TextView) findViewById(R.id.dobTV);
        this.childPlace = (TextView) findViewById(R.id.placeTV);
        this.aadharTV = (TextView) findViewById(R.id.aadharTV);
        this.bloodTV = (TextView) findViewById(R.id.bloodTV);
        this.vaccineLV = (ListView) findViewById(R.id.listView);
        Intent intent = getIntent();
        this.f42id = intent.getStringExtra("child_id");
        this.name = intent.getStringExtra("child_name");
        this.mNAme = intent.getStringExtra("mother_name");
        this.dob = intent.getStringExtra("child_dob");
        this.placeBirth = intent.getStringExtra("place_birth");
        this.aadharNumber = intent.getStringExtra("child_aadhar");
        this.bloodGroup = intent.getStringExtra("blood_group");
        this.relation = intent.getStringExtra("relation");
        this.image = intent.getStringExtra("image");
        this.gender=intent.getStringExtra("gender");

        StringBuilder sb = new StringBuilder();
        sb.append("https://globizdevserver.com/Vaccination/img/childimg/");
        sb.append(this.image);
        if(this.gender.matches("Female")){
            Picasso.with(this).load(sb.toString()).placeholder((int) R.drawable.profile_logo1).into((ImageView) this.childImage);
        }else{
            Picasso.with(this).load(sb.toString()).placeholder((int) R.drawable.profile_logo).into((ImageView) this.childImage);
        }
        this.childName.setText(this.name);
        this.motherName.setText(this.mNAme);
        this.childDOB.setText(this.dob);
        this.childPlace.setText(this.placeBirth);
        this.bloodTV.setText(this.bloodGroup);
        if(!this.aadharNumber.isEmpty()){
            this.aadharTV.setText(""+this.aadharNumber);
        }
        if (Functions.isNetworkConnected(this.context)) {
            completeVaccine(this.f42id);
            return;
        }
        int i = 0;
        if (this.dbVaccination.getVaccine(Integer.valueOf(this.f42id).intValue()).size() > 0) {
            this.tempScheduleModelArrayList = this.dbVaccination.getVaccine(Integer.valueOf(this.f42id).intValue());
            while (true) {
                int i2 = i;
                if (i2 <= 4) {
                    if (!((ScheduleModel) this.tempScheduleModelArrayList.get(i2)).getStatus().equals(AppEventsConstants.EVENT_PARAM_VALUE_NO)) {
                        ScheduleModel scheduleModel = new ScheduleModel(((ScheduleModel) this.tempScheduleModelArrayList.get(i2)).getId(), ((ScheduleModel) this.tempScheduleModelArrayList.get(i2)).getExpire_date(), ((ScheduleModel) this.tempScheduleModelArrayList.get(i2)).getStatus(), ((ScheduleModel) this.tempScheduleModelArrayList.get(i2)).getVaccine_name(), ((ScheduleModel) this.tempScheduleModelArrayList.get(i2)).getChild_id(), ((ScheduleModel) this.tempScheduleModelArrayList.get(i2)).getVaccine_description(), "",((ScheduleModel) this.tempScheduleModelArrayList.get(i2)).getVaccine_date(),((ScheduleModel) this.tempScheduleModelArrayList.get(i2)).getChild_name());
                        this.scheduleArrayList.add(scheduleModel);
                        this.scheduleAdapter = new CompleteVaccineAdapter(this.context, this.scheduleArrayList);
                        this.vaccineLV.setAdapter(this.scheduleAdapter);
                        this.scheduleAdapter.notifyDataSetChanged();
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        } else {
            Toast.makeText(this.context, R.string.no_data, Toast.LENGTH_SHORT).show();
        }
    }

    private void completeVaccine(final String id) {
        new AsyncTask<String, String, String>() {
            /* access modifiers changed from: protected */
            public String doInBackground(String... params) {
                ChildDetailActivity.this.object = new JSONObject();
                JSONArray jsonArray = new JSONArray();
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("child_id", id);
                    jsonArray.put(jsonObject);
                    ChildDetailActivity.this.object.put("vaccine", jsonArray);
                    Log.e("response : ", ChildDetailActivity.this.object.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            /* access modifiers changed from: protected */
            public void onPreExecute() {
                super.onPreExecute();
                ChildDetailActivity.this.loading = new ProgressDialog(ChildDetailActivity.this,R.style.DialogTheme);
                ChildDetailActivity.this.loading.setMessage("Loading...");
                ChildDetailActivity.this.loading.show();
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(String s) {
                super.onPostExecute(s);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.COMPLETE_VACCINE_DETAIL_URL, ChildDetailActivity.this.object, new Listener<JSONObject>() {
                    public void onResponse(JSONObject response) {
                        try {
                            boolean status = response.getBoolean("status");
                            String string = response.getString(NotificationCompat.CATEGORY_MESSAGE);
                            if (status) {
                                ChildDetailActivity.this.loading.dismiss();
                                PrintStream printStream = System.out;
                                StringBuilder sb = new StringBuilder();
                                sb.append("Response :");
                                sb.append(response);
                                printStream.println(sb.toString());
                                try {
                                    JSONArray jsonArray = response.getJSONArray("Complete Vaccine");
                                    ArrayList<String> scheduleDetail = new ArrayList<>();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        ChildDetailActivity.this.scheduleArrayList.add(new Gson().fromJson(jsonArray.getJSONObject(i).toString(), ScheduleModel.class));
                                        scheduleDetail.add(((ScheduleModel) ChildDetailActivity.this.scheduleArrayList.get(i)).getId());
                                        scheduleDetail.add(((ScheduleModel) ChildDetailActivity.this.scheduleArrayList.get(i)).getVaccine_name());
                                        scheduleDetail.add(((ScheduleModel) ChildDetailActivity.this.scheduleArrayList.get(i)).getExpire_date());
                                    }
                                    ChildDetailActivity.this.scheduleAdapter = new CompleteVaccineAdapter(ChildDetailActivity.this, ChildDetailActivity.this.scheduleArrayList);
                                    ChildDetailActivity.this.vaccineLV.setAdapter(ChildDetailActivity.this.scheduleAdapter);
                                    ChildDetailActivity.this.scheduleAdapter.notifyDataSetChanged();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                return;
                            }
                            ChildDetailActivity.this.loading.dismiss();
                        } catch (JSONException e2) {
                            e2.printStackTrace();
                        }
                    }
                }, new ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                    }
                });
                AppController.getInstance().addToRequestQueue(jsonObjectRequest);
            }
        }.execute(new String[0]);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 16908332) {
            finish();
        }
        return true;
    }
}

