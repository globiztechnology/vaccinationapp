package com.globiz.vaccinationapp.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore.Images.Media;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.ItemTouchHelper;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import com.facebook.appevents.AppEventsConstants;
import com.facebook.internal.ServerProtocol;
import com.facebook.share.internal.ShareConstants;
import com.globiz.vaccinationapp.R;
import com.globiz.vaccinationapp.controller.AppController;
import com.globiz.vaccinationapp.customViews.CaseSensitiveLetters;
import com.globiz.vaccinationapp.customViews.CircularImageView;
import com.globiz.vaccinationapp.util.Constant;
import com.globiz.vaccinationapp.util.Functions;
import com.globiz.vaccinationapp.util.Preferences;
import com.squareup.picasso.Picasso;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class EditChildActivity extends AppCompatActivity implements OnClickListener {
    private int PICK_IMAGE_REQUEST = 1;
    private String aadhar;
    /* access modifiers changed from: private */
    public Bitmap bitmap;
    /* access modifiers changed from: private */
    public String childAadhar;
    private EditText childAadharET;
    /* access modifiers changed from: private */
    public String childDOB;
    public String gender;
    /* access modifiers changed from: private */
    public EditText childDobET;
    /* access modifiers changed from: private */
    public String childFName;
    private EditText childFNameET;
    private RadioButton childFemale;
    CircularImageView childImage;
    /* access modifiers changed from: private */
    public String childLName;
    private EditText childLNameET;
    /* access modifiers changed from: private */
    public String childMName;
    private EditText childMNameET;
    private RadioButton childMale;
    private String childName;
    /* access modifiers changed from: private */
    public String child_id;
    private String child_image;
    /* access modifiers changed from: private */
    public SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
    /* access modifiers changed from: private */
    public DatePickerDialog datePickerDialog;
    private String finalImageUrl;
    public ImageButton aadharInfo;
    /* access modifiers changed from: private */
    public ProgressDialog loading;
    private ProgressDialog mProgressDialog;
    /* access modifiers changed from: private */
    public JSONObject object;
    /* access modifiers changed from: private */
    public String placeBirth;
    private EditText placeBirthET;
    private RadioGroup radioGroup;
    /* access modifiers changed from: private */
    public String radiobutton_status = "";
    private Button saveBT;
    private Toolbar toolbar;
    CircularImageView uploadChildImage;
    CaseSensitiveLetters convert;
    private ArrayList<String> list= new ArrayList<>();
    public Spinner bloodGroup;
    public String bloodGroupValue;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_edit_child);
        convert=new CaseSensitiveLetters();
        initViews();
    }

    private void initViews() {
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(this.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle((CharSequence) "Edit Child Profile");
        Intent intent = getIntent();
        this.childFNameET = (EditText) findViewById(R.id.childFName);
        this.childFNameET.setText(convert.caseSensitive(intent.getStringExtra("first_name")) );
        this.childLNameET = (EditText) findViewById(R.id.childLName);
        this.childLNameET.setText(convert.caseSensitive(intent.getStringExtra("last_name")));
        this.childMNameET = (EditText) findViewById(R.id.motherName);
        this.childMNameET.setText(convert.caseSensitive(intent.getStringExtra("mother_name")));
        this.placeBirthET = (EditText) findViewById(R.id.placeBirth);
        this.placeBirthET.setText(convert.caseSensitive(intent.getStringExtra("place_birth")));


        this.placeBirthET.setOnEditorActionListener(new OnEditorActionListener() {
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                return false;
            }
        });
        this.childDobET = (EditText) findViewById(R.id.edtBirthdate);
        this.childDobET.setText(intent.getStringExtra("child_dob"));
        this.childAadharET = (EditText) findViewById(R.id.childAadharET);
        this.childAadharET.setText(intent.getStringExtra("child_aadhar"));
        this.aadharInfo = findViewById(R.id.aadharInfo);
        this.childAadharET.addTextChangedListener(new TextWatcher() {
            private static final char space = ' ';
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (start == 0){
                    if( s.toString().equalsIgnoreCase("0") || s.toString().equalsIgnoreCase("1")){
                        Functions.setErrorEditText(EditChildActivity.this.childAadharET, "Aadhar Number should not start with 0 and 1");
                    }
                }else if(start != 13){
                    Functions.setErrorEditText(EditChildActivity.this.childAadharET, "Aadhar Number can't be Empty. Upto 12 digits");
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {

                // Remove all spacing char
                int pos = 0;

                while (true) {
                    Log.e("POSTION OF CHAR", String.valueOf(s));
                    if (pos >= s.length()) break;
                    if (space == s.charAt(pos) && (((pos + 1) % 5) != 0 || pos + 1 == s.length())) {
                        s.delete(pos, pos + 1);
                    } else {
                        pos++;
                    }
                }

                // Insert char where needed.
                pos = 4;
                while (true) {
                    if (pos >= s.length()) break;
                    final char c = s.charAt(pos);

                    // Only if its a digit where there should be a space we insert a space
                    if ("0123456789".indexOf(c) >= 0) {
                        s.insert(pos, "" + space);
                    }
                    pos += 5;
                }
            }
        });
        this.aadharInfo.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                aadharInfo.setBackgroundTintList(getResources().getColorStateList(R.color.white));
                Toast.makeText(EditChildActivity.this, "Please enter child Aadhar number -- it is just to track all children's vaccines for future purpose", Toast.LENGTH_SHORT).show();
                return true;
            }
        });
        this.childImage = (CircularImageView) findViewById(R.id.childEditPic);
        this.uploadChildImage = (CircularImageView) findViewById(R.id.changePic);
        this.childMale = (RadioButton) findViewById(R.id.btnAddChildMale);
        this.childFemale = (RadioButton) findViewById(R.id.btnAddChildFemale);
        this.child_image = intent.getStringExtra("image");
        this.child_id = intent.getStringExtra("child_id");
        this.gender = intent.getStringExtra("gender");
        this.aadhar = intent.getStringExtra("child_aadhar");
        this.childAadharET.setText(this.aadhar);
        StringBuilder sb = new StringBuilder();
        sb.append("https://heedforyou.com/vaccination/img/childimg/");

        sb.append(this.child_image);
        Log.e("Image",sb.toString());
        this.finalImageUrl = sb.toString();
        if (!TextUtils.isEmpty(this.finalImageUrl)) {
//            PrintStream printStream = System.out;
//            StringBuilder sb2 = new StringBuilder();
//            sb2.append("image");
//            sb2.append(this.finalImageUrl);
//            printStream.println(sb2.toString());
            if(this.gender.matches("Female")) {
                Picasso.with(this).load(this.finalImageUrl).placeholder((int) R.drawable.profile_logo1).into((ImageView) this.childImage);
            }
            else {
                Picasso.with(this).load(this.finalImageUrl).placeholder((int) R.drawable.profile_logo).into((ImageView) this.childImage);
            }
        }
        this.childDobET.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() != 1) {
                    return false;
                }
                event.getRawX();
                EditChildActivity.this.childDobET.getRight();
                EditChildActivity.this.childDobET.getCompoundDrawables()[2].getBounds().width();
                return true;
            }
        });
        if (this.gender.equalsIgnoreCase("Male")) {
            this.childMale.setChecked(true);
            this.childFemale.setChecked(false);
            this.radiobutton_status = AppEventsConstants.EVENT_PARAM_VALUE_NO;
        } else {
            this.childFemale.setChecked(true);
            this.childMale.setChecked(false);
            this.radiobutton_status = AppEventsConstants.EVENT_PARAM_VALUE_YES;
        }
        this.bloodGroup = (Spinner) findViewById(R.id.bloodGroup);
        this.getList();
        this.saveBT = (Button) findViewById(R.id.btnSave);
        this.uploadChildImage.setOnClickListener(this);
        this.childMale.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                EditChildActivity.this.childMale.setChecked(true);
               EditChildActivity.this.childFemale.setChecked(false);
                EditChildActivity.this.radiobutton_status = AppEventsConstants.EVENT_PARAM_VALUE_NO;
                Picasso.with(EditChildActivity.this).load(EditChildActivity.this.finalImageUrl).placeholder((int) R.drawable.profile_logo).into((ImageView) EditChildActivity.this.childImage);
            }
        });
        this.childFemale.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                EditChildActivity.this.childMale.setChecked(false);
                EditChildActivity.this.childFemale.setChecked(true);
               EditChildActivity.this.radiobutton_status = AppEventsConstants.EVENT_PARAM_VALUE_YES;
                Picasso.with(EditChildActivity.this).load(EditChildActivity.this.finalImageUrl).placeholder((int) R.drawable.profile_logo1).into((ImageView) EditChildActivity.this.childImage);
            }
        });
        this.saveBT.setOnClickListener(this);
    }

    private void getList() {

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, Constant.CHILD_BLOOD_GROUPS, null ,new Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                try {
                    boolean status = response.getBoolean("status");
                    String string = response.getString(NotificationCompat.CATEGORY_MESSAGE);
                    if (status) {
                        JSONArray jsonArray =response.getJSONArray("blood_groups");
                        ArrayList<JSONObject> bloodDetail = new ArrayList<>();
                        for(int i = 0; i< jsonArray.length(); i++){
                            JSONObject  jsonObject = jsonArray.getJSONObject(i);
                            bloodDetail.add(jsonObject.getJSONObject("BloodGroup"));
                        }
                        EditChildActivity.this.list.add("Select Blood Group");
                        for(int i = 0; i< bloodDetail.size(); i++){
                            JSONObject  jsonObject = bloodDetail.get(i);
                            EditChildActivity.this.list.add(jsonObject.getString("name"));
                        }
                        Intent intent = getIntent();
                        // ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(EditChildActivity.this,
                        //         android.R.layout.simple_spinner_item, EditChildActivity.this.list);
                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(EditChildActivity.this,
                                R.layout.simple_spinner_dropdown_item2, EditChildActivity.this.list);
                        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        EditChildActivity.this.bloodGroup.setAdapter(spinnerArrayAdapter);
                        EditChildActivity.this.bloodGroup.setSelection(spinnerArrayAdapter.getPosition(intent.getStringExtra("blood_group")));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                String errortype = null;
                if ((error instanceof TimeoutError) || (error instanceof NoConnectionError)) {
                    errortype = "No Internet Access, check your internet connection.";
                } else if (error instanceof AuthFailureError) {
                    errortype = "Authentication error, please try again.";
                } else if (error instanceof ServerError) {
                    errortype = "Server error, please try again.";
                } else if (error instanceof NetworkError) {
                    errortype = "Network error, please try again.";
                } else if (error instanceof ParseError) {
                    errortype = "Internal error, please try again.";
                }
                Toast.makeText(getApplicationContext(), errortype, Toast.LENGTH_SHORT).show();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }


    private void datePickerDialog() {
        Calendar newCalendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog2 = new DatePickerDialog(this,R.style.DialogTheme, new OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                EditChildActivity.this.datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                String selectdate = EditChildActivity.this.dateFormatter.format(newDate.getTime());
                String currentDateandTime = new SimpleDateFormat("dd-MMM-yyyy").format(new Date());
                try {
                    if (EditChildActivity.this.dateFormatter.parse(selectdate).before(EditChildActivity.this.dateFormatter.parse(currentDateandTime))) {
                        EditChildActivity.this.childDobET.setText(EditChildActivity.this.dateFormatter.format(newDate.getTime()));
                    } else if (EditChildActivity.this.dateFormatter.parse(selectdate).equals(EditChildActivity.this.dateFormatter.parse(currentDateandTime))) {
                        EditChildActivity.this.childDobET.setText(EditChildActivity.this.dateFormatter.format(newDate.getTime()));
                    } else if (EditChildActivity.this.dateFormatter.parse(selectdate).after(EditChildActivity.this.dateFormatter.parse(currentDateandTime))) {
                        final Dialog dialog = new Dialog(EditChildActivity.this);
                        dialog.setContentView(R.layout.dob_validate_dialog);
                        ((Button) dialog.findViewById(R.id.okBtn)).setOnClickListener(new OnClickListener() {
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                        dialog.show();
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        this.datePickerDialog = datePickerDialog2;
        this.datePickerDialog.show();
    }

    public String getStringImage(Bitmap bmp) {

        Bitmap bmp2 = ((BitmapDrawable) this.childImage.getDrawable()).getBitmap();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp2.compress(CompressFormat.JPEG, 100, baos);
        return Base64.encodeToString(baos.toByteArray(), 0);
    }

    /* access modifiers changed from: private */
    public void showFileChooser() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{"android.permission.WRITE_EXTERNAL_STORAGE"}, 1);
                requestPermissions(new String[]{"android.permission.READ_EXTERNAL_STORAGE"}, 1);
            } else if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_DENIED) {

                Intent intent = new Intent("android.intent.action.PICK", Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                intent.putExtra("crop", ServerProtocol.DIALOG_RETURN_SCOPES_TRUE);
                intent.putExtra("outputX", ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION);
                intent.putExtra("outputY", ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION);
                intent.putExtra("aspectX", 1);
                intent.putExtra("aspectY", 1);
                intent.putExtra("scale", true);
                intent.putExtra("output", true);
                intent.putExtra("outputFormat", CompressFormat.JPEG.toString());
                intent.setAction("android.intent.action.GET_CONTENT");
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), this.PICK_IMAGE_REQUEST);
            }}
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == this.PICK_IMAGE_REQUEST && resultCode == -1 && data != null && data.getData() != null) {
            try {
                this.bitmap = Media.getBitmap(getContentResolver(), data.getData());
                this.childImage.setImageBitmap(this.bitmap);
                this.child_image="Not Null";
                Toast.makeText(EditChildActivity.this, "Photo Updated Successfully", Toast.LENGTH_LONG).show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 16908332) {
            finish();
        }
        return true;
    }

    public void onClick(View v) {
        int id = v.getId();
        if (id != R.id.btnSave) {
            if (id == R.id.changePic) {
                dialogDeleteUpload();
            }
        } else if (Functions.isNetworkConnected(this)) {
            saveEditChildData();
        } else {
            Toast.makeText(this, R.string.check_your_internet_connectivity, Toast.LENGTH_SHORT).show();
        }
    }
    public boolean isAlpha(String s) {
        return s != null && s.matches("^[a-zA-z]+([\\s][a-zA-Z]+)*$");
    }
    private boolean isValidInput() {
        this.childFNameET.setError(null);
        this.childLNameET.setError(null);
        this.childMNameET.setError(null);
        this.childDobET.setError(null);
        this.childAadharET.setError(null);
        this.placeBirthET.setError(null);
        this.childFName = this.childFNameET.getText().toString().trim();
        this.childLName = this.childLNameET.getText().toString().trim();
        this.childMName = this.childMNameET.getText().toString().trim();
        this.childDOB = this.childDobET.getText().toString().trim();
        this.childAadhar = this.childAadharET.getText().toString().trim();

        this.placeBirth = this.placeBirthET.getText().toString().trim();
        if(this.bloodGroup.getSelectedItem().toString()!="Select Blood Group"){
            this.bloodGroupValue =   this.bloodGroup.getSelectedItem().toString();
        }else {  this.bloodGroupValue ="";}
        char ch=' ';
        if(!TextUtils.isEmpty(this.childAadhar)) {
            ch = this.childAadharET.getText().toString().charAt(0);
        }
        if (TextUtils.isEmpty(this.childFName)) {
            Functions.setErrorEditText(this.childFNameET, "Child First Name can't be Empty");
            return false;
        }else if (!isAlpha(this.childFName)) {
            Functions.setErrorEditText(this.childFNameET, "Invalid Name");
            return false;
        } else if (TextUtils.isEmpty(this.childLName)) {
            Functions.setErrorEditText(this.childLNameET, "Child Last Name can't be Empty");
            return false;
        }else if (!isAlpha(this.childLName)) {
            Functions.setErrorEditText(this.childLNameET, "Invalid Name");
            return false;
        } else if (TextUtils.isEmpty(this.childMName)) {
            Functions.setErrorEditText(this.childMNameET, "Child Mother Name can't be Empty");
            return false;
        } else if (!isAlpha(this.childMName)) {
            Functions.setErrorEditText(this.childMNameET, "Invalid Name");
            return false;
        } else if (TextUtils.isEmpty(this.placeBirth)) {
            Functions.setErrorEditText(this.placeBirthET, "Place of Birth can't be Empty");
            return false;
        } else if (!isAlpha(this.placeBirth)) {
            Functions.setErrorEditText(this.placeBirthET, "Invalid Name");
            return false;
        } else if (TextUtils.isEmpty(this.childDOB)) {
            Functions.setErrorEditText(this.childDobET, "Child DOB can't be Empty");
            return false;
        }else if (TextUtils.isEmpty(this.bloodGroupValue)) {
            Toast.makeText(getApplicationContext(), "Please select the blood group of child", Toast.LENGTH_SHORT).show();
            return false;
        } else
            if (ch=='0' || ch== '1') {
            Functions.setErrorEditText(this.childAadharET, "Aadhar Number should not start with 0 and 1");
            return false;
        } /*else {
            return  true;
        }*/
        else if (TextUtils.isEmpty(this.childAadhar) || this.childAadhar.length() == 14) {
            return true;
        } else {
            Functions.setErrorEditText(this.childAadharET, "Aadhar Number can't be Empty. Upto 12 digits");
            return false;
        }


    }

    private void saveEditChildData() {
        if (isValidInput()) {
            new AsyncTask<String, String, String>() {
                /* access modifiers changed from: protected */
                public String doInBackground(String... params) {
                    EditChildActivity.this.object = new JSONObject();
                    JSONArray jsonArray = new JSONArray();
                    JSONObject jsonObject = new JSONObject();
                    String image = EditChildActivity.this.getStringImage(EditChildActivity.this.bitmap);
                    try {
                        jsonObject.put("first_name", EditChildActivity.this.childFName);
                        jsonObject.put("last_name", EditChildActivity.this.childLName);
                        jsonObject.put("mother_name", EditChildActivity.this.childMName);
                        jsonObject.put("place_birth", EditChildActivity.this.placeBirth);
                        jsonObject.put("aadhar", EditChildActivity.this.childAadhar);

                        jsonObject.put("parent_id", Preferences.getUserId(EditChildActivity.this));
                        jsonObject.put("blood_group", EditChildActivity.this.bloodGroupValue);
                        jsonObject.put("dob", EditChildActivity.this.childDOB);
                        jsonObject.put(ShareConstants.WEB_DIALOG_PARAM_ID, EditChildActivity.this.child_id);
                        jsonObject.put("gender", EditChildActivity.this.radiobutton_status);
                        jsonObject.put("image", image);
                        if (EditChildActivity.this.child_image != null ) {
                            jsonObject.put("image", image);
                        } else {
                            jsonObject.put("image", "");
                        }
                        jsonArray.put(jsonObject);
                        EditChildActivity.this.object.put("child", jsonArray);
                        Log.e("EDIT CHILD", String.valueOf(EditChildActivity.this.object));
                        PrintStream printStream = System.out;
                        StringBuilder sb = new StringBuilder();
                        sb.append("response1 : ");
                        sb.append(EditChildActivity.this.object.toString());
                        printStream.println(sb.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return image;
                }
                /* access modifiers changed from: protected */
                public void onPreExecute() {
                    super.onPreExecute();
                    EditChildActivity.this.loading =  new ProgressDialog(EditChildActivity.this,R.style.DialogTheme);
                    EditChildActivity.this.loading.setMessage("Please wait...");
                    EditChildActivity.this.loading.show( );
                }

                /* access modifiers changed from: protected */
                public void onPostExecute(String s) {
                    super.onPostExecute(s);
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.EDIT_CHILD, EditChildActivity.this.object, new Listener<JSONObject>() {
                        public void onResponse(JSONObject response) {
                            try {
                                boolean status = response.getBoolean("status");
                                String msg = response.getString(NotificationCompat.CATEGORY_MESSAGE);
                                if (status) {
                                    EditChildActivity.this.loading.dismiss();
                                    PrintStream printStream = System.out;
                                    StringBuilder sb = new StringBuilder();
                                    sb.append("Response 1 :");
                                    sb.append(response);
                                    printStream.println(sb.toString());
                                    EditChildActivity.this.finish();
                                    return;
                                }
                                Toast.makeText(EditChildActivity.this, msg, Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new ErrorListener() {
                        public void onErrorResponse(VolleyError error) {
                            EditChildActivity.this.loading.dismiss();
                            Toast.makeText(EditChildActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                            error.printStackTrace();
                        }
                    });
                    AppController.getInstance().addToRequestQueue(jsonObjectRequest);
                }
            }.execute(new String[0]);
        }
    }

    private void dialogDeleteUpload() {
        String[] options = {"Upload Image", "Delete Image"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((CharSequence) "Choose an Option");
        builder.setCancelable(true);
        builder.setItems((CharSequence[]) options, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    EditChildActivity.this.showFileChooser();
                } else if (which == 1) {
                    EditChildActivity.this.deleteImage();
                }
            }
        });
        builder.show();
    }

    /* access modifiers changed from: private */
    public void deleteImage() {
        showProgressDialog();
        this.object = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(ShareConstants.WEB_DIALOG_PARAM_ID, this.child_id);
            jsonArray.put(jsonObject);
            this.object.put("child", jsonArray);
            Log.e("response : ", this.object.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.DELETE_CHILD_PROFILE_PICTURE, this.object, new Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                try {
                    boolean status = response.getBoolean("status");
                  //  String msg = response.getString(NotificationCompat.CATEGORY_MESSAGE);
                    Log.e("response1 : ", response.toString());
                    if (status) {
                        EditChildActivity.this.hideProgressDialog();
                        PrintStream printStream = System.out;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Response s :");
                        sb.append(response);
                        printStream.println(sb.toString());
                        Preferences.setUserImageUrl(EditChildActivity.this, EditChildActivity.this.getURLForResource(R.drawable.profile_logo));
                        EditChildActivity.this.childImage.setImageDrawable(ContextCompat.getDrawable(EditChildActivity.this, R.drawable.profile_logo));
                        if(EditChildActivity.this.gender.matches("Female")) {
                            Picasso.with(EditChildActivity.this).load((int) R.drawable.profile_logo1).into((ImageView) EditChildActivity.this.childImage);
                        }else {
                            Picasso.with(EditChildActivity.this).load((int) R.drawable.profile_logo).into((ImageView) EditChildActivity.this.childImage);
                        }
                        EditChildActivity.this.child_image=null;
                        Toast.makeText(EditChildActivity.this, "Photo Deleted Successfully", Toast.LENGTH_LONG).show();
                        return;
                    }
                    EditChildActivity.this.hideProgressDialog();
                   // Toast.makeText(EditChildActivity.this, msg, Toast.LENGTH_LONG).show();
                    EditChildActivity.this.hideProgressDialog();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                EditChildActivity.this.hideProgressDialog();
                Toast.makeText(EditChildActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                error.printStackTrace();
            }
        });
        Volley.newRequestQueue(this).add(jsonObjectRequest);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(20000, 0, 1.0f));
    }

    private void showProgressDialog() {
        if (this.mProgressDialog == null) {
            this.mProgressDialog = new ProgressDialog(this,R.style.DialogTheme);
            this.mProgressDialog.setMessage(getString(R.string.loading));
            this.mProgressDialog.setIndeterminate(true);
        }
        this.mProgressDialog.show();
    }

    /* access modifiers changed from: private */
    public void hideProgressDialog() {
        if (this.mProgressDialog != null && this.mProgressDialog.isShowing()) {
            this.mProgressDialog.hide();
        }
    }

    public String getURLForResource(int resourceId) {
        StringBuilder sb = new StringBuilder();
        sb.append("android.resource://");
        sb.append(R.class.getPackage().getName());
        sb.append("/");
        sb.append(resourceId);
        return Uri.parse(sb.toString()).toString();
    }
}

