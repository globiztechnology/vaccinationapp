package com.globiz.vaccinationapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.globiz.vaccinationapp.R;
import com.globiz.vaccinationapp.util.Preferences;

public class SplashActivity extends AppCompatActivity {
    ImageView dropIV;
    private String email;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(1);
        setContentView((int) R.layout.activity_splash);
        new Thread() {
            public void run() {
                try {
                    sleep(2000);
                    if (!SplashActivity.this.isLogin()) {
                        SplashActivity.this.startActivity(new Intent(SplashActivity.this.getBaseContext(), LoginActivity.class));
                        SplashActivity.this.finish();
                        return;
                    }
                    SplashActivity.this.startActivity(new Intent(SplashActivity.this.getBaseContext(), MainActivity.class));
                    SplashActivity.this.finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public boolean isLogin() {
        this.email = Preferences.getUserEmail(this);
        if (this.email == null || this.email.length() <= 0) {
            return false;
        }
        return true;
    }
}

