package com.globiz.vaccinationapp.activity;

import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import com.facebook.share.internal.ShareConstants;
import com.globiz.vaccinationapp.R;
import com.globiz.vaccinationapp.controller.AppController;
import com.globiz.vaccinationapp.util.Constant;
import com.globiz.vaccinationapp.util.Functions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class EditNotesActivity extends AppCompatActivity implements OnClickListener {
    private Button bt_edit_note, bt_delete_note;
    private EditText et_note;
    private String note;
    private String noteId;
    private String complete_vacc_id;
    private String parent_id;
    private Toolbar toolbar;
    private  String delete=Constant.DELETENOTES;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_edit_notes);
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(this.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle((CharSequence) "Edit Note");
        this.et_note = (EditText) findViewById(R.id.et_note);
        this.bt_edit_note = (Button) findViewById(R.id.Editnotes);
        this.bt_edit_note.setOnClickListener(this);
        this.bt_delete_note= findViewById(R.id.Deletenotes);
        this.bt_delete_note.setOnClickListener(this);
        Intent intent = getIntent();
        this.noteId = intent.getStringExtra(ShareConstants.WEB_DIALOG_PARAM_ID);
        this.note = intent.getStringExtra(NotificationCompat.CATEGORY_MESSAGE);
        this.complete_vacc_id=intent.getStringExtra("complete_vacc_id");
        this.parent_id=intent.getStringExtra("parent_id");
        this.et_note.setText(this.note);
    }

    private void editNote(String noteId2, String noteMsg) {
        JSONObject jsonObject1 = new JSONObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("note", noteMsg);
            jsonObject.put(ShareConstants.WEB_DIALOG_PARAM_ID, noteId2);
//            jsonObject.put()
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(jsonObject);
            jsonObject1.putOpt("note", jsonArray);
            Log.e("note", jsonObject1.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.EDITNOTES, jsonObject1, new Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                try {
                    boolean status = response.getBoolean("status");
                    String msg = response.getString(NotificationCompat.CATEGORY_MESSAGE);
                    if (status) {
                        Toast.makeText(EditNotesActivity.this, msg, Toast.LENGTH_LONG).show();
                        EditNotesActivity.this.finish();
                        return;
                    }
                    Toast.makeText(EditNotesActivity.this, msg, Toast.LENGTH_LONG).show();
                    EditNotesActivity.this.finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", "ERROR");
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);

    }


    private void deleteNote(String noteId2) {
        JSONObject jsonObject1 = new JSONObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ShareConstants.WEB_DIALOG_PARAM_ID, noteId2);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(jsonObject);
            jsonObject1.putOpt("note", jsonArray);
            Log.e("note--------", jsonObject1.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    //   Toast.makeText(EditNotesActivity.this, Constant.DELETENOTES, Toast.LENGTH_LONG).show();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.DELETENOTES, jsonObject1, new Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                try {

                    boolean status = response.getBoolean("status");
                    String msg = response.getString(NotificationCompat.CATEGORY_MESSAGE);
                    if (status) {
                        Log.e("ReSPONSE NOTE--",response.toString());
                        Toast.makeText(EditNotesActivity.this, "Note deleted successfully", Toast.LENGTH_LONG).show();
                        EditNotesActivity.this.finish();
                        return;
                    }
                    Toast.makeText(EditNotesActivity.this, msg, Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", "ERROR");
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 16908332) {
            finish();
        }
        return true;
    }

    public void onClick(View v) {
        if (v.getId() == R.id.Editnotes) {
            if (this.et_note.getText().toString().isEmpty()) {
                Toast.makeText(this, "Plaese Enter note", Toast.LENGTH_SHORT).show();
            } else if (Functions.isNetworkConnected(this)) {
                editNote(this.noteId, this.et_note.getText().toString());
            } else {
                Toast.makeText(this, R.string.check_your_internet_connectivity, Toast.LENGTH_SHORT).show();
            }
        }
        if (v.getId() == R.id.Deletenotes){
            if (Functions.isNetworkConnected(this)) {
                deleteNote(this.noteId);
            } else {
                Toast.makeText(this, R.string.check_your_internet_connectivity, Toast.LENGTH_SHORT).show();
            }
        }
    }
}

