package com.globiz.vaccinationapp.fragment;
import android.app.ProgressDialog;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import androidx.fragment.app.Fragment;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.globiz.vaccinationapp.R;
import com.globiz.vaccinationapp.adapter.FAQAdapter;
import com.globiz.vaccinationapp.controller.AppController;
import com.globiz.vaccinationapp.model.FaqModel;
import com.globiz.vaccinationapp.util.Constant;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FaqFragment extends Fragment {
    /* access modifiers changed from: private */
    public ExpandableListView expandableListView;
    /* access modifiers changed from: private */
    public FAQAdapter faqAdapter;
    /* access modifiers changed from: private */
    public ArrayList<FaqModel> list = new ArrayList<>();
    /* access modifiers changed from: private */

    /* renamed from: pd */
    public ProgressDialog f44pd;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_faq, container, false);
//        DisplayMetrics metrics = new DisplayMetrics();
//        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
//        int width = metrics.widthPixels;
        this.expandableListView = (ExpandableListView) view.findViewById(R.id.faqList);
//        this.expandableListView.setIndicatorBounds(width - GetPixelFromDips(50), width - GetPixelFromDips(10));


        this.f44pd = new ProgressDialog(getActivity(),R.style.DialogTheme);
        this.f44pd.setMessage("Loading...");
        this.f44pd.setCancelable(false);
        makejsonobjreq();
        return view;
    }
    public int GetPixelFromDips(float pixels){
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }
    private void makejsonobjreq() {
        this.f44pd.show();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.FAQ, null, new Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                try {
                    JSONArray ja = response.getJSONArray("Faq");
                    for (int i = 0; i < ja.length(); i++) {
                        JSONObject jo = ja.getJSONObject(i);
                        FaqFragment.this.list.add(new FaqModel(jo.getString("question"), jo.getString("answer")));
                    }
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
                FaqFragment.this.faqAdapter = new FAQAdapter(FaqFragment.this.getActivity(), FaqFragment.this.list);
                FaqFragment.this.expandableListView.setAdapter(FaqFragment.this.faqAdapter);
                FaqFragment.this.f44pd.dismiss();
            }
        }, new ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                FaqFragment.this.f44pd.dismiss();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "jreq");
    }
}
