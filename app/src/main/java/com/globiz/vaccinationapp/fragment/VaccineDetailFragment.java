package com.globiz.vaccinationapp.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.globiz.vaccinationapp.adapter.ChildRVAdapter.OnItemClickListener;

import com.facebook.share.internal.ShareConstants;
import com.globiz.vaccinationapp.R;
import com.globiz.vaccinationapp.adapter.ChildDetailAdapter;
import com.globiz.vaccinationapp.adapter.ChildRVAdapter;
import com.globiz.vaccinationapp.adapter.ScheduleAdapter;
import com.globiz.vaccinationapp.controller.AppController;
import com.globiz.vaccinationapp.database.DBVaccination;
import com.globiz.vaccinationapp.model.ChildDetailModel;
import com.globiz.vaccinationapp.model.ScheduleModel;
import com.globiz.vaccinationapp.util.Constant;
import com.globiz.vaccinationapp.util.Functions;
import com.globiz.vaccinationapp.util.Preferences;
import com.google.gson.Gson;

import java.io.PrintStream;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class VaccineDetailFragment extends Fragment implements OnItemClickListener {
    private ArrayList<String> chilDetail;
    private ChildDetailAdapter childDetailAdapter;
    private ArrayList<ChildDetailModel> childDetailList = new ArrayList<>();
    private LinearLayout childNameLL;
    private RecyclerView childRV;
    private Context context;
    DBVaccination dbVaccination;
    /* access modifiers changed from: private */
    public ProgressDialog loading;
    private ChildRVAdapter mAdapter;
    private TextView[] obj;
    private TextView[] obj1;
    /* access modifiers changed from: private */
    public JSONObject object;
    /* access modifiers changed from: private */
    public ScheduleAdapter scheduleAdapter;
    /* access modifiers changed from: private */
    public ArrayList<ScheduleModel> scheduleArrayList = new ArrayList<>();
    /* access modifiers changed from: private */
    public ListView scheduleLV;
    private int selectedChildId = 0;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vaccine_detail, container, false);
        this.context = getActivity();
        this.dbVaccination = new DBVaccination(this.context);
        initViews(view);
        if (Functions.isNetworkConnected(this.context)) {
            this.childDetailList.clear();
            getChild();
        } else if (this.dbVaccination.getAllChilds().size() > 0 && this.dbVaccination.getAllTheSchedule().size() > 0) {
            this.childDetailList = this.dbVaccination.getAllChilds();
            this.mAdapter = new ChildRVAdapter(this.context, this.childDetailList, this);
            this.scheduleArrayList = this.dbVaccination.getVaccine(Integer.valueOf(((ChildDetailModel) this.childDetailList.get(0)).getId()).intValue());
            this.childRV.setAdapter(this.mAdapter);
            this.scheduleAdapter = new ScheduleAdapter(this.context, this.scheduleArrayList,VaccineDetailFragment.this);
            this.scheduleLV.setAdapter(this.scheduleAdapter);
        }
        this.mAdapter = new ChildRVAdapter(this.context, this.childDetailList, this);
        this.childRV.setAdapter(this.mAdapter);
        return view;
    }

    private void initViews(View view) {
        this.scheduleLV = (ListView) view.findViewById(R.id.list);
        this.childRV = (RecyclerView) view.findViewById(R.id.rv_horizontal);
        this.childRV.setHasFixedSize(true);
        this.childRV.setLayoutManager(new LinearLayoutManager(getActivity().getBaseContext(), RecyclerView.HORIZONTAL, false));
    }

    public void getSchedule() {
        new AsyncTask<String, String, String>() {
            /* access modifiers changed from: protected */
            public String doInBackground(String... params) {
                VaccineDetailFragment.this.object = new JSONObject();
                JSONArray jsonArray = new JSONArray();
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put(ShareConstants.WEB_DIALOG_PARAM_ID, Constant.ChildId);
                    jsonArray.put(jsonObject);
                    VaccineDetailFragment.this.object.put("child", jsonArray);
                    Log.e("response vaccine- ", VaccineDetailFragment.this.object.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            /* access modifiers changed from: protected */
            public void onPreExecute() {
                super.onPreExecute();
                VaccineDetailFragment.this.loading =  new ProgressDialog(VaccineDetailFragment.this.getActivity(),R.style.DialogTheme);
                VaccineDetailFragment.this.loading.setMessage("Please wait...");
              //  VaccineDetailFragment.this.loading.setTitle("Uploading...");
                VaccineDetailFragment.this.loading.show( );

            }

            /* access modifiers changed from: protected */
            public void onPostExecute(String s) {
                super.onPostExecute(s);
                scheduleArrayList.clear();
                VaccineDetailFragment.this.dbVaccination.deleteTables();
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.VACCINE_DETAIL_URL, VaccineDetailFragment.this.object, new Listener<JSONObject>() {
                    public void onResponse(JSONObject response) {
                        JSONObject jSONObject = response;
                        try {
                            boolean status = jSONObject.getBoolean("status");
                            String string = jSONObject.getString(NotificationCompat.CATEGORY_MESSAGE);
                            if (status) {
                                VaccineDetailFragment.this.loading.dismiss();
                                PrintStream printStream = System.out;
                                StringBuilder sb = new StringBuilder();
                                sb.append("Response :");
                                sb.append(jSONObject);
                                printStream.println(sb.toString());
                                try {
                                    VaccineDetailFragment.this.loading.dismiss();
                                    Log.e("response Vaccine ", jSONObject.getJSONArray("Complete Vaccine").toString());
                                    JSONArray jsonArray = jSONObject.getJSONArray("Complete Vaccine");
                                    ArrayList<String> scheduleDetail = new ArrayList<>();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject object1 = jsonArray.getJSONObject(i);
                                        VaccineDetailFragment.this.scheduleArrayList.add(new Gson().fromJson(object1.toString(), ScheduleModel.class));
                                       if (!VaccineDetailFragment.this.dbVaccination.checkIsDataAlreadyInDBorNot(object1.getString(ShareConstants.WEB_DIALOG_PARAM_ID))) {
                                            ScheduleModel scheduleModel = new ScheduleModel(object1.getString(ShareConstants.WEB_DIALOG_PARAM_ID), object1.getString("expire_date"), object1.getString("status"), object1.getString("vaccine_name"), object1.getString("child_id"), object1.getString("vaccine_description"), object1.getString("vaccine_image"), object1.getString("vaccine_date"), object1.getString("child_name"));
                                            VaccineDetailFragment.this.dbVaccination.addVaccinations(scheduleModel);
                                        }
                                        scheduleDetail.add(((ScheduleModel) VaccineDetailFragment.this.scheduleArrayList.get(i)).getId());
                                        scheduleDetail.add(((ScheduleModel) VaccineDetailFragment.this.scheduleArrayList.get(i)).getVaccine_name());
                                        scheduleDetail.add(((ScheduleModel) VaccineDetailFragment.this.scheduleArrayList.get(i)).getExpire_date());
                                        scheduleDetail.add(((ScheduleModel) VaccineDetailFragment.this.scheduleArrayList.get(i)).getVaccine_description());
                                        scheduleDetail.add(((ScheduleModel) VaccineDetailFragment.this.scheduleArrayList.get(i)).getChild_name());
                                    }
                                    VaccineDetailFragment.this.loading.dismiss();
                                  //  Collections.sort(VaccineDetailFragment.this.scheduleArrayList);
                                    VaccineDetailFragment.this.scheduleAdapter = new ScheduleAdapter(VaccineDetailFragment.this.getActivity(), VaccineDetailFragment.this.scheduleArrayList,VaccineDetailFragment.this);
                                    VaccineDetailFragment.this.scheduleLV.setAdapter(VaccineDetailFragment.this.scheduleAdapter);
                                    VaccineDetailFragment.this.scheduleAdapter.notifyDataSetChanged();
                                    VaccineDetailFragment.this.loading.dismiss();

                                    return;
                                } catch (JSONException e) {
                                    VaccineDetailFragment.this.loading.dismiss();
                                    e.printStackTrace();
                                }
                                return;
                            }
                            VaccineDetailFragment.this.loading.dismiss();
                        } catch (JSONException e2) {
                            VaccineDetailFragment.this.loading.dismiss();
                            Log.e("response Vaccinedd ", e2.toString());

                            e2.printStackTrace();
                        }
                    }
                }, new ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        VaccineDetailFragment.this.loading.dismiss();
                        Log.e("response Vaccine ", error.toString());
                    }
                });
                AppController.getInstance().addToRequestQueue(jsonObjectRequest);
            }
        }.execute(new String[0]);
    }

    public void getChild() {
        new AsyncTask<String, String, String>() {
            /* access modifiers changed from: protected */
            public String doInBackground(String... params) {
                VaccineDetailFragment.this.object = new JSONObject();
                JSONArray jsonArray = new JSONArray();
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("parent_id", Preferences.getUserId(VaccineDetailFragment.this.getActivity()));
                    jsonArray.put(jsonObject);
                    VaccineDetailFragment.this.object.put("childs", jsonArray);
                    Log.e("response12 : ", VaccineDetailFragment.this.object.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            /* access modifiers changed from: protected */
            public void onPreExecute() {
                super.onPreExecute();
                VaccineDetailFragment.this.loading = new ProgressDialog(VaccineDetailFragment.this.getActivity(),R.style.DialogTheme);
                VaccineDetailFragment.this.loading.setMessage("Please wait...");
               // VaccineDetailFragment.this.loading.setTitle("Uploading...");
                VaccineDetailFragment.this.loading.show( );
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(String s) {
                super.onPostExecute(s);
               // Log.e("API CALL: ", s);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.CHILD_DETAIL_URL, VaccineDetailFragment.this.object, new Listener<JSONObject>() {
                    public void onResponse(JSONObject response) {
                        try {
                            boolean status = response.getBoolean("status");
                            String string = response.getString(NotificationCompat.CATEGORY_MESSAGE);
                            Log.e("AStatus: ", String.valueOf(status));
                            if (status) {
                                VaccineDetailFragment.this.loading.dismiss();
                                PrintStream printStream = System.out;
                                StringBuilder sb = new StringBuilder();
                                sb.append("Response :");
                                sb.append(response);
                                printStream.println(sb.toString());
                                VaccineDetailFragment.this.setData(response, null);
                                return;
                            }
                            VaccineDetailFragment.this.loading.dismiss();
                            final Dialog dialogReminder = new Dialog(VaccineDetailFragment.this.getActivity());
                            dialogReminder.setContentView(R.layout.child_notification_dialog);
                            dialogReminder.getWindow().setLayout(-1, -2);
                            TextView textView = (TextView) dialogReminder.findViewById(R.id.child_reminderTV);
                            ((Button) dialogReminder.findViewById(R.id.btn_OK)).setOnClickListener(new OnClickListener() {
                                public void onClick(View v) {
                                    dialogReminder.dismiss();
                                }
                            });
                            dialogReminder.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                            dialogReminder.show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        VaccineDetailFragment.this.loading.dismiss();
                        Toast.makeText(VaccineDetailFragment.this.getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
                        error.printStackTrace();
                    }
                });
                AppController.getInstance().addToRequestQueue(jsonObjectRequest);
            }
        }.execute(new String[0]);
    }

    /* access modifiers changed from: 0000 */
    public void setData(JSONObject response, String pos) {
        try {
            Log.e("AStatus: ", String.valueOf(response));
            JSONArray jsonArray = response.getJSONArray("Childs");
            this.chilDetail = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object1 = jsonArray.getJSONObject(i);
                this.childDetailList.add(new Gson().fromJson(object1.toString(), ChildDetailModel.class));
                this.chilDetail.add(((ChildDetailModel) this.childDetailList.get(i)).getFirst_name());
                this.chilDetail.add(((ChildDetailModel) this.childDetailList.get(i)).getLast_name());
                this.chilDetail.add(((ChildDetailModel) this.childDetailList.get(i)).getId());
                if (!this.dbVaccination.checkIsChildAlreadyInDBorNot(object1.getString(ShareConstants.WEB_DIALOG_PARAM_ID))) {
                    ChildDetailModel childDetailModel = new ChildDetailModel(object1.getString(ShareConstants.WEB_DIALOG_PARAM_ID), object1.getString("first_name"), object1.getString("last_name"), object1.getString("image"), object1.getString("dob"), object1.getString("mother_name"), object1.getString("place_birth"), object1.getString("gender"), object1.getString("aadhar"),object1.getString("relation"),object1.getString("blood_group"));
                    Log.e("Set Values to db", String.valueOf(childDetailModel));
                    this.dbVaccination.addAllChilds(childDetailModel);
                    Log.e("Set Values to db2", String.valueOf(childDetailModel));
                }
                this.mAdapter = new ChildRVAdapter(this.context, this.childDetailList, this);
                this.childRV.setAdapter(this.mAdapter);
                this.mAdapter.notifyDataSetChanged();

            }
            Constant.ChildId = ((ChildDetailModel) this.childDetailList.get(0)).getId();
            getSchedule();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onResume() {
        super.onResume();

    }

    public void onItemClick(int position) {
    }

    public void inItemClick(int position) {
        childRV.scrollToPosition(position);
        if (Functions.isNetworkConnected(this.context)) {
            Constant.ChildId = ((ChildDetailModel) this.childDetailList.get(position)).getId();
            this.scheduleArrayList.clear();
            getSchedule();
            return;
        }
        this.scheduleAdapter = new ScheduleAdapter(getActivity(), this.dbVaccination.getVaccine(Integer.valueOf(((ChildDetailModel) this.childDetailList.get(position)).getId()).intValue()),VaccineDetailFragment.this);
        this.scheduleLV.setAdapter(this.scheduleAdapter);
    }
}

