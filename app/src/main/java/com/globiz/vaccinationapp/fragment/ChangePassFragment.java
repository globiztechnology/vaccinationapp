package com.globiz.vaccinationapp.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.share.internal.ShareConstants;
import com.globiz.vaccinationapp.R;
import com.globiz.vaccinationapp.activity.LoginActivity;
import com.globiz.vaccinationapp.controller.AppController;
import com.globiz.vaccinationapp.util.Constant;
import com.globiz.vaccinationapp.util.Functions;
import com.globiz.vaccinationapp.util.Preferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ChangePassFragment extends Fragment implements OnClickListener {
    private Button changePasswordBt;
    /* access modifiers changed from: private */
    public ProgressDialog loading;
    /* access modifiers changed from: private */
    public String newPass = "";
    /* access modifiers changed from: private */
    public JSONObject object;
    private EditText txtNewPassword;
    private EditText txtOldPassword;
    /* access modifiers changed from: private */
    public String userId = "";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_pass, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        this.txtOldPassword = (EditText) view.findViewById(R.id.txtOldPassword);
        this.txtNewPassword = (EditText) view.findViewById(R.id.txtNewPassword);
        this.changePasswordBt = (Button) view.findViewById(R.id.changePasswordBt);
        this.changePasswordBt.setOnClickListener(this);
    }

    public void onClick(View v) {
        if (this.txtOldPassword.getText().toString().trim().length() <= 5) {
            Functions.setErrorEditText(this.txtOldPassword, "Password must be 6 characters or more");
        } else if (!this.txtOldPassword.getText().toString().equals(Preferences.getUserPassword(getActivity()))) {
            Functions.setErrorEditText(this.txtOldPassword, "Current password does not match");
        } else if (this.txtNewPassword.getText().toString().trim().length() <= 5) {
            Functions.setErrorEditText(this.txtNewPassword, "New Password must be 6 characters or more");
        } else {
            final Dialog dialogReminder = new Dialog(ChangePassFragment.this.getActivity());
            dialogReminder.setContentView(R.layout.change_password_dialog);
            dialogReminder.getWindow().setLayout(-1, -2);
            Button noBtn = (Button) dialogReminder.findViewById(R.id.btn_no);
            ((Button) dialogReminder.findViewById(R.id.btn_yes)).setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    if (Functions.isNetworkConnected(ChangePassFragment.this.getActivity())) {
                        ChangePassFragment.this.forgotPassword();
                    } else {
                        Toast.makeText(ChangePassFragment.this.getActivity(), R.string.check_your_internet_connectivity, Toast.LENGTH_SHORT).show();
                    }
                    dialogReminder.dismiss();
                }});
            noBtn.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    dialogReminder.dismiss();
                }
            });
            dialogReminder.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            dialogReminder.show();
        }
//            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//            builder.setTitle((CharSequence) "Change Password?");
//            builder.setMessage((CharSequence) "Would You like to change your Password!");
//            builder.setCancelable(false);
//            builder.setPositiveButton((int) R.string.no, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int id) {
//                    dialog.dismiss();
//                }
//            });
//            builder.setNegativeButton((int) R.string.yes, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int id) {
//
//                }
//            });
//            builder.show();
//        }
    }

    /* access modifiers changed from: private */
    public void forgotPassword() {
        this.newPass = this.txtNewPassword.getText().toString().trim();
        this.userId = Preferences.getUserId(getActivity());
        new AsyncTask<String, String, JSONObject>() {
            /* access modifiers changed from: protected */
            public JSONObject doInBackground(String... params) {
                ChangePassFragment.this.object = new JSONObject();
                JSONArray jsonArray = new JSONArray();
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put(ShareConstants.WEB_DIALOG_PARAM_ID, ChangePassFragment.this.userId);
                    jsonObject.put("password", ChangePassFragment.this.newPass);
                    jsonArray.put(jsonObject);
                    ChangePassFragment.this.object.put("User", jsonArray);
                    Log.e("response", ChangePassFragment.this.object.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return ChangePassFragment.this.object;
            }

            /* access modifiers changed from: protected */
            public void onPreExecute() {
                super.onPreExecute();
                ChangePassFragment.this.loading = new ProgressDialog(ChangePassFragment.this.getActivity(),R.style.DialogTheme);
                ChangePassFragment.this.loading.setMessage("Please wait...");
                ChangePassFragment.this.loading.setTitle("Uploading...");
                ChangePassFragment.this.loading.show( );
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(JSONObject s) {
                super.onPostExecute(s);
                if (s != null) {
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.CHANGE_PASSWORD, ChangePassFragment.this.object, new Listener<JSONObject>() {
                        public void onResponse(JSONObject response) {
                            try {
                                boolean status = response.getBoolean("status");
                                String msg = response.getString(NotificationCompat.CATEGORY_MESSAGE);
                                if (status) {
                                    Toast.makeText(ChangePassFragment.this.getActivity(), msg, Toast.LENGTH_LONG).show();
                                    Preferences.resetUserDetails(ChangePassFragment.this.getActivity());
                                    ChangePassFragment.this.startActivity(new Intent(ChangePassFragment.this.getActivity(), LoginActivity.class));
                                    ChangePassFragment.this.getActivity().finish();
                                } else {
                                    Toast.makeText(ChangePassFragment.this.getActivity(), msg, Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            ChangePassFragment.this.loading.dismiss();
                        }
                    }, new ErrorListener() {
                        public void onErrorResponse(VolleyError error) {
                            ChangePassFragment.this.loading.dismiss();
                            error.printStackTrace();
                        }
                    });
                    AppController.getInstance().addToRequestQueue(jsonObjectRequest);
                }
            }
        }.execute(new String[0]);
    }
}

