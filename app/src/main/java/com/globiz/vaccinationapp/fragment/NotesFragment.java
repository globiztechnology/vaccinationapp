package com.globiz.vaccinationapp.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import com.facebook.share.internal.ShareConstants;
import com.globiz.vaccinationapp.R;
import com.globiz.vaccinationapp.activity.EditNotesActivity;
import com.globiz.vaccinationapp.adapter.NoteDetailAdapter;
import com.globiz.vaccinationapp.controller.AppController;
import com.globiz.vaccinationapp.database.DBVaccination;
import com.globiz.vaccinationapp.model.NoteDetailModel;
import com.globiz.vaccinationapp.util.Constant;
import com.globiz.vaccinationapp.util.Functions;
import com.globiz.vaccinationapp.util.Preferences;
import com.google.gson.Gson;
import java.io.PrintStream;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class NotesFragment extends Fragment {
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public DBVaccination dbVaccination;
    /* access modifiers changed from: private */
    public ProgressDialog loading;
    /* access modifiers changed from: private */
    public NoteDetailAdapter noteDetailAdapter;
    public ArrayList<NoteDetailModel> noteDetailList;
    /* access modifiers changed from: private */
    public ListView notesLV;
    /* access modifiers changed from: private */
    public JSONObject object;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notes, container, false);
        this.dbVaccination = new DBVaccination(getActivity());
        this.notesLV = (ListView) view.findViewById(R.id.notesListView);
//        if (Functions.isNetworkConnected(getActivity())) {
//            getNotes();
//        } else if (this.dbVaccination.getAllNotes().size() > 0) {
//            this.noteDetailList = this.dbVaccination.getAllNotes();
//            this.noteDetailAdapter = new NoteDetailAdapter(getActivity(), this.noteDetailList);
//            this.notesLV.setAdapter(this.noteDetailAdapter);
//        } else {
//            Toast.makeText(getActivity(), getString(R.string.no_data), Toast.LENGTH_SHORT).show();
//        }
        this.notesLV.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                String noteId = ((NoteDetailModel) NotesFragment.this.noteDetailList.get(position)).getId();
                String msg = ((NoteDetailModel) NotesFragment.this.noteDetailList.get(position)).getNote();
                String complete_vaccineId=NotesFragment.this.noteDetailList.get(position).getComplete_vacc_id();
                String parent_id=NotesFragment.this.noteDetailList.get(position).getParentId();
                Intent intent = new Intent(NotesFragment.this.getActivity(), EditNotesActivity.class);
                intent.putExtra(ShareConstants.WEB_DIALOG_PARAM_ID, noteId);
                intent.putExtra("complete_vacc_id", complete_vaccineId);
                intent.putExtra("parent_id", parent_id);
                intent.putExtra(NotificationCompat.CATEGORY_MESSAGE, msg);
                NotesFragment.this.startActivity(intent);
            }
        });
        return view;
    }

    private void getNotes() {
        new AsyncTask<String, String, String>() {
            /* access modifiers changed from: protected */
            public String doInBackground(String... params) {
                NotesFragment.this.object = new JSONObject();
                JSONArray jsonArray = new JSONArray();
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("parent_id", Preferences.getUserId(NotesFragment.this.getActivity()));
                    jsonArray.put(jsonObject);
                    NotesFragment.this.object.put("note", jsonArray);
                    Log.e("response : ", NotesFragment.this.object.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            /* access modifiers changed from: protected */
            public void onPreExecute() {
                super.onPreExecute();
                NotesFragment.this.loading = new ProgressDialog(NotesFragment.this.getActivity(),R.style.DialogTheme);
                NotesFragment.this.loading.setMessage("Please wait...");
                NotesFragment.this.loading.setTitle("Loading...");
                NotesFragment.this.loading.show();
//                        ProgressDialog.show(NotesFragment.this.getActivity(), "Loading...", "Please wait...", false, false);
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(String s) {
                super.onPostExecute(s);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.NOTE_DETAIL_URL, NotesFragment.this.object, new Listener<JSONObject>() {
                    public void onResponse(JSONObject response) {
                        try {
                            boolean status = response.getBoolean("status");
                            String string = response.getString(NotificationCompat.CATEGORY_MESSAGE);
                            if (status) {
                                NotesFragment.this.loading.dismiss();
                                PrintStream printStream = System.out;
                                StringBuilder sb = new StringBuilder();
                                sb.append("Response :");
                                sb.append(response);
                                printStream.println(sb.toString());
                                Log.e("response : ", sb.toString());
                                try {
                                    JSONArray jsonArray = response.getJSONArray("ParentNotes");
                                    ArrayList<String> noteDetail = new ArrayList<>();
                                    NotesFragment.this.noteDetailList = new ArrayList<>();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject object1 = jsonArray.getJSONObject(i);
                                        if (!TextUtils.isEmpty(object1.getString("child_first_name"))) {
                                            NoteDetailModel noteDetailModel = new NoteDetailModel();
                                            noteDetailModel.setId(object1.getString(ShareConstants.WEB_DIALOG_PARAM_ID));
                                            noteDetailModel.setComplete_vacc_id(object1.getString("complete_vacc_id"));
                                            noteDetailModel.setParentId(object1.getString("parent_id"));
                                            noteDetailModel.setVaccine_name(object1.getString("vaccine_name"));
                                            noteDetailModel.setChild_first_name(object1.getString("child_first_name"));
                                            noteDetailModel.setChild_last_name(object1.getString("child_last_name"));
                                            noteDetailModel.setNote(object1.getString("note"));
                                            NotesFragment.this.dbVaccination.addNote(noteDetailModel);
                                            NotesFragment.this.noteDetailList.add(new Gson().fromJson(object1.toString(), NoteDetailModel.class));
                                            noteDetail.add(((NoteDetailModel) NotesFragment.this.noteDetailList.get(i)).getNote());
                                        }
                                    }
                                    NotesFragment.this.noteDetailAdapter = new NoteDetailAdapter(NotesFragment.this.context, NotesFragment.this.noteDetailList);
                                    NotesFragment.this.notesLV.setAdapter(NotesFragment.this.noteDetailAdapter);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                return;
                            }
                            NotesFragment.this.loading.dismiss();
                            NotesFragment.this.notesLV.setAdapter(null);
                            final Dialog dialog = new Dialog(NotesFragment.this.getActivity());
                            dialog.setContentView(R.layout.notes_notify_dialog);
                            dialog.getWindow().setLayout(-1, -2);
                            TextView textView = (TextView) dialog.findViewById(R.id.note_text);
                            ((Button) dialog.findViewById(R.id.btnOk)).setOnClickListener(new View.OnClickListener() {
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                            dialog.show();
                        } catch (JSONException e2) {
                            e2.printStackTrace();
                        }
                    }
                }, new ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        NotesFragment.this.loading.dismiss();
                        error.printStackTrace();
                    }
                });
                AppController.getInstance().addToRequestQueue(jsonObjectRequest);
            }
        }.execute(new String[0]);
    }

    public void onResume() {
        super.onResume();
        if (Functions.isNetworkConnected(getActivity())) {
            getNotes();
        } else if (this.dbVaccination.getAllNotes().size() > 0) {
            this.noteDetailList = this.dbVaccination.getAllNotes();
            this.noteDetailAdapter = new NoteDetailAdapter(getActivity(), this.noteDetailList);
            this.notesLV.setAdapter(this.noteDetailAdapter);
        } else {
            //Toast.makeText(getActivity(), getString(R.string.no_data), Toast.LENGTH_SHORT).show();
        }
    }
}

