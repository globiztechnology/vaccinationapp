package com.globiz.vaccinationapp.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import com.globiz.vaccinationapp.R;
import com.globiz.vaccinationapp.controller.AppController;
import com.globiz.vaccinationapp.customViews.CaseSensitiveLetters;
import com.globiz.vaccinationapp.util.Constant;
import com.globiz.vaccinationapp.util.Functions;
import com.globiz.vaccinationapp.util.Preferences;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import java.io.PrintStream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ContactUsFragment extends Fragment implements OnClickListener {
    String comment;
    EditText commentET;
    String email;
    EditText emailET;
    String name;
    EditText nameET;
    private JSONObject object;
    String phone;
    EditText phoneET;
    String subject;
    EditText subjectET;
    private Button submitBT;
    public ProgressDialog loading;
    CaseSensitiveLetters convert;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact_us, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        this.nameET = (EditText) view.findViewById(R.id.edtfullname);
        this.emailET = (EditText) view.findViewById(R.id.edtMail);
        this.phoneET = (EditText) view.findViewById(R.id.edtMobile);
        this.subjectET = (EditText) view.findViewById(R.id.edtSubject);
        this.commentET = (EditText) view.findViewById(R.id.edtComment);
        StringBuilder sb = new StringBuilder();
        sb.append(Preferences.getUserFName(getActivity()));
        sb.append(" ");
        sb.append(Preferences.getUserLName(getActivity()));
        this.nameET.setText(sb.toString());
        this.emailET.setText(Preferences.getUserEmail(getActivity()));
        this.phoneET.setText(Preferences.getUserPhone(getActivity()));
        this.submitBT = (Button) view.findViewById(R.id.btnSubmitContactus);
        this.submitBT.setOnClickListener(this);
    }

    private boolean isValidInput() {
        this.nameET.setError(null);
        this.emailET.setError(null);
        this.phoneET.setError(null);
        this.subjectET.setError(null);
        this.commentET.setError(null);
        this.name = this.nameET.getText().toString().trim();
        this.email = this.emailET.getText().toString().trim();
        this.phone = this.phoneET.getText().toString().trim();
        this.subject = this.subjectET.getText().toString().trim();
        this.comment = this.commentET.getText().toString().trim();
        if (TextUtils.isEmpty(this.name)) {
            Functions.setErrorEditText(this.nameET, "Full Name can't be Empty");
            return false;
        } else if (TextUtils.isEmpty(this.email)) {
            Functions.setErrorEditText(this.emailET, "Email can't be Empty");
            return false;
        } else if (!Functions.isValidEmail(this.email)) {
            Functions.setErrorEditText(this.emailET, "Invalid Email");
            return false;
        } else if (TextUtils.isEmpty(this.phone)) {
            Functions.setErrorEditText(this.phoneET, "Mobile can't be Empty");
            return false;
        } else if (!Functions.isValidMobile(this.phone)) {
            Functions.setErrorEditText(this.phoneET, "Invalid Phone : 10 digit");
            return false;
        } else if (TextUtils.isEmpty(this.subject)) {
            Functions.setErrorEditText(this.subjectET, "Subject can't be Empty");
            return false;
        } else if (!TextUtils.isEmpty(this.comment)) {
            return true;
        } else {
            Functions.setErrorEditText(this.commentET, "Comment can't be Empty");
            return true;
        }
    }

    public void sendComment() {
        if (isValidInput()) {
            this.object = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("name", this.name);
                jsonObject.put("email", this.email);
                jsonObject.put("phone", this.phone);
                jsonObject.put("subject", this.subject);
                jsonObject.put(Param.CONTENT, this.comment);
                jsonArray.put(jsonObject);
                this.object.put("user", jsonArray);
                Log.e("response", this.object.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            loading =new ProgressDialog(getActivity(),R.style.DialogTheme);
            loading.setMessage("Please wait...");
            loading.show();
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.CONTACT_URL, this.object, new Listener<JSONObject>() {
                public void onResponse(JSONObject response) {
                    PrintStream printStream = System.out;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Aman : ");
                    sb.append(response);
                    printStream.println(sb.toString());
                    try {
                        boolean status = response.getBoolean("status");
                        String msg = response.getString(NotificationCompat.CATEGORY_MESSAGE);
                        if (status) {
                            loading.dismiss();
                            ContactUsFragment.this.nameET.getText().clear();
                            ContactUsFragment.this.nameET.setHint("Enter Full Name");
                            ContactUsFragment.this.emailET.getText().clear();
                            ContactUsFragment.this.emailET.setHint("Enter Email");
                            ContactUsFragment.this.phoneET.getText().clear();
                            ContactUsFragment.this.phoneET.setHint(" Enter Phone No.");
                            ContactUsFragment.this.subjectET.getText().clear();
                            ContactUsFragment.this.subjectET.setHint("Subject");
                            ContactUsFragment.this.commentET.getText().clear();
                            ContactUsFragment.this.commentET.setHint("Put your Comment Here ");
                            Toast.makeText(ContactUsFragment.this.getActivity(), msg, Toast.LENGTH_LONG).show();
                            return;
                        }
                        loading.dismiss();
                        Toast.makeText(ContactUsFragment.this.getActivity(), msg, Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        loading.dismiss();
                        e.printStackTrace();
                    }
                }
            }, new ErrorListener() {
                public void onErrorResponse(VolleyError error) {
                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjectRequest);
        }
    }

    public void onClick(View v) {
        if (v.getId() ==R.id.btnSubmitContactus) {
            if (Functions.isNetworkConnected(getActivity())) {
                sendComment();
            } else {
                Toast.makeText(getActivity(), R.string.check_your_internet_connectivity, Toast.LENGTH_SHORT).show();
            }
        }
    }
}

