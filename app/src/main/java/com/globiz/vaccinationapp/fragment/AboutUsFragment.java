package com.globiz.vaccinationapp.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import com.globiz.vaccinationapp.R;
import com.globiz.vaccinationapp.controller.AppController;
import com.globiz.vaccinationapp.util.Constant;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import org.json.JSONException;
import org.json.JSONObject;

public class AboutUsFragment extends Fragment {
    /* access modifiers changed from: private */
    public TextView aboutusTV;
    private ProgressDialog mProgressDialog;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about_us, container, false);
        this.aboutusTV = (TextView) view.findViewById(R.id.about_usTV);
        getAboutUsData();
        return view;
    }

    public void getAboutUsData() {
        showProgressDialog();
        AppController.getInstance().addToRequestQueue(new StringRequest(1, Constant.ABOUTUS, new Listener<String>() {
            public void onResponse(String response) {
                try {
                    AboutUsFragment.this.hideProgressDialog();
                    AboutUsFragment.this.aboutusTV.setText(Html.fromHtml(new JSONObject(response).getJSONObject("About").getString(Param.CONTENT)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new ErrorListener() {
            public void onErrorResponse(VolleyError error) {
            }
        }));
    }
    private void showProgressDialog() {
        if (this.mProgressDialog == null) {
            this.mProgressDialog = new ProgressDialog(getActivity(),R.style.DialogTheme);
            this.mProgressDialog.setMessage(getString(R.string.loading));
            this.mProgressDialog.setIndeterminate(true);
        }
        this.mProgressDialog.show();
    }

    /* access modifiers changed from: private */
    public void hideProgressDialog() {
        if (this.mProgressDialog != null && this.mProgressDialog.isShowing()) {
            this.mProgressDialog.hide();
        }
    }
}

