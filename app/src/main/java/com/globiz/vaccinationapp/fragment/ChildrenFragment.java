package com.globiz.vaccinationapp.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import com.facebook.share.internal.ShareConstants;
import com.globiz.vaccinationapp.R;
import com.globiz.vaccinationapp.activity.AddChildActivity;
import com.globiz.vaccinationapp.activity.ChildDetailActivity;
import com.globiz.vaccinationapp.adapter.ChildDetailAdapter;
import com.globiz.vaccinationapp.controller.AppController;
import com.globiz.vaccinationapp.customViews.CaseSensitiveLetters;
import com.globiz.vaccinationapp.database.DBVaccination;
import com.globiz.vaccinationapp.model.ChildDetailModel;
import com.globiz.vaccinationapp.util.Constant;
import com.globiz.vaccinationapp.util.Functions;
import com.globiz.vaccinationapp.util.Preferences;
import com.google.gson.Gson;
import java.io.PrintStream;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ChildrenFragment extends Fragment {
    ChildDetailAdapter childDetailAdapter;
    public ArrayList<ChildDetailModel> childDetailList;
    private String childId;
    /* access modifiers changed from: private */
    public ListView childList;
    Context context;
    DBVaccination dbVaccination;
    /* access modifiers changed from: private */
    public ProgressDialog loading;
    /* access modifiers changed from: private */
    public JSONObject object;
    CaseSensitiveLetters convert;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_children, container, false);
        this.dbVaccination = new DBVaccination(getActivity());

        initViews(view);
        return view;
    }

    private void initViews(View view) {
        this.childList = (ListView) view.findViewById(R.id.listView);
        if (Functions.isNetworkConnected(getActivity())) {
            getChildDetail();
        } else if (this.dbVaccination.getAllNotes().size() > 0) {
            this.childDetailList = this.dbVaccination.getAllChilds();
            this.childDetailAdapter = new ChildDetailAdapter(getActivity(), this.childDetailList);
            this.childList.setAdapter(this.childDetailAdapter);
        } else {
            Toast.makeText(getActivity(), getString(R.string.no_data), Toast.LENGTH_SHORT).show();
        }
        this.childList.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                String child_id = ((ChildDetailModel) ChildrenFragment.this.childDetailList.get(position)).getId();
                String child_image = "";
                if (!TextUtils.isEmpty(((ChildDetailModel) ChildrenFragment.this.childDetailList.get(position)).getImage())) {
                    child_image = ((ChildDetailModel) ChildrenFragment.this.childDetailList.get(position)).getImage();
                }

                Intent intent = new Intent(ChildrenFragment.this.getActivity(), ChildDetailActivity.class);
                intent.putExtra("child_id", child_id);
                intent.putExtra("image", child_image);
                StringBuilder sb = new StringBuilder();
                sb.append(((ChildDetailModel) ChildrenFragment.this.childDetailList.get(position)).getFirst_name());
                sb.append(" ");
                sb.append(((ChildDetailModel) ChildrenFragment.this.childDetailList.get(position)).getLast_name());
                intent.putExtra("child_name", sb.toString());
                intent.putExtra("mother_name", ((ChildDetailModel) ChildrenFragment.this.childDetailList.get(position)).getMother_name());
                intent.putExtra("child_dob", ((ChildDetailModel) ChildrenFragment.this.childDetailList.get(position)).getDob());
                intent.putExtra("place_birth", ((ChildDetailModel) ChildrenFragment.this.childDetailList.get(position)).getPlace_birth());
                intent.putExtra("child_aadhar", ((ChildDetailModel) ChildrenFragment.this.childDetailList.get(position)).getAadhar());
                intent.putExtra("gender", ((ChildDetailModel) ChildrenFragment.this.childDetailList.get(position)).getGender());
                Log.e("cccc",( ChildrenFragment.this.childDetailList.get(position)).getBlood_group());
                intent.putExtra("blood_group", ((ChildDetailModel) ChildrenFragment.this.childDetailList.get(position)).getBlood_group());
                intent.putExtra("relation", ((ChildDetailModel) ChildrenFragment.this.childDetailList.get(position)).getRelation());
                ChildrenFragment.this.startActivity(intent);
            }
        });
    }

    private void getChildDetail() {
        new AsyncTask<String, String, String>() {
            /* access modifiers changed from: protected */
            public String doInBackground(String... params) {
                ChildrenFragment.this.object = new JSONObject();
                JSONArray jsonArray = new JSONArray();
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("parent_id", Preferences.getUserId(ChildrenFragment.this.getActivity()));
                    jsonArray.put(jsonObject);
                    ChildrenFragment.this.object.put("childs", jsonArray);
                    Log.e("response : ", ChildrenFragment.this.object.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            /* access modifiers changed from: protected */
            public void onPreExecute() {
                super.onPreExecute();
                ChildrenFragment.this.loading =new ProgressDialog(getActivity(),R.style.DialogTheme);
                ChildrenFragment.this.loading.setMessage("Loading..");
                ChildrenFragment.this.loading.show();
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(String s) {
                super.onPostExecute(s);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.CHILD_DETAIL_URL, ChildrenFragment.this.object, new Listener<JSONObject>() {
                    public void onResponse(JSONObject response) {
                        JSONObject jSONObject = response;
                        try {
                            boolean status = jSONObject.getBoolean("status");
                            String string = jSONObject.getString(NotificationCompat.CATEGORY_MESSAGE);
                            if (status) {
                                ChildrenFragment.this.loading.dismiss();
                                PrintStream printStream = System.out;
                                StringBuilder sb = new StringBuilder();
                                sb.append("Response :");
                                sb.append(jSONObject);
                                printStream.println(sb.toString());
                                Log.e("response : ", sb.toString());
                                try {
                                    JSONArray jsonArray = jSONObject.getJSONArray("Childs");
                                    ArrayList<String> chilDetail = new ArrayList<>();
                                    ChildrenFragment.this.childDetailList = new ArrayList<>();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject object1 = jsonArray.getJSONObject(i);
                                        if (!ChildrenFragment.this.dbVaccination.checkIsChildAlreadyInDBorNot(object1.getString(ShareConstants.WEB_DIALOG_PARAM_ID))) {
                                            ChildDetailModel childDetailModel = new ChildDetailModel(object1.getString(ShareConstants.WEB_DIALOG_PARAM_ID), object1.getString("first_name"), object1.getString("last_name"), object1.getString("image"), object1.getString("dob"), object1.getString("mother_name"), object1.getString("place_birth"), object1.getString("gender"), object1.getString("aadhar"),object1.getString("relation"),object1.getString("blood_group"));
                                            ChildrenFragment.this.dbVaccination.addAllChilds(childDetailModel);
                                        }
                                        ChildrenFragment.this.childDetailList.add(new Gson().fromJson(object1.toString(), ChildDetailModel.class));
                                        chilDetail.add(((ChildDetailModel) ChildrenFragment.this.childDetailList.get(i)).getFirst_name());
                                        chilDetail.add(((ChildDetailModel) ChildrenFragment.this.childDetailList.get(i)).getLast_name());
                                        chilDetail.add(((ChildDetailModel) ChildrenFragment.this.childDetailList.get(i)).getDob());
                                        chilDetail.add(((ChildDetailModel) ChildrenFragment.this.childDetailList.get(i)).getRelation());
                                        chilDetail.add(((ChildDetailModel) ChildrenFragment.this.childDetailList.get(i)).getBlood_group());

                                    }
                                    ChildrenFragment.this.childDetailAdapter = new ChildDetailAdapter(ChildrenFragment.this.getActivity(), ChildrenFragment.this.childDetailList);
                                    ChildrenFragment.this.childList.setAdapter(ChildrenFragment.this.childDetailAdapter);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                return;
                            }
                            ChildrenFragment.this.loading.dismiss();
                            ChildrenFragment.this.childList.setAdapter(null);
                            final Dialog dialogReminder = new Dialog(ChildrenFragment.this.getActivity());
                            dialogReminder.setContentView(R.layout.child_notification_dialog);
                            dialogReminder.getWindow().setLayout(-1, -2);
                            TextView textView = (TextView) dialogReminder.findViewById(R.id.child_reminderTV);
                            ((Button) dialogReminder.findViewById(R.id.btn_OK)).setOnClickListener(new OnClickListener() {
                                public void onClick(View v) {
                                    dialogReminder.dismiss();
                                }
                            });
                            dialogReminder.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                            dialogReminder.show();
                        } catch (JSONException e2) {
                            e2.printStackTrace();
                        }
                    }
                }, new ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        ChildrenFragment.this.loading.dismiss();
                       // Toast.makeText(ChildrenFragment.this.getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
                        error.printStackTrace();
                    }
                });
                AppController.getInstance().addToRequestQueue(jsonObjectRequest);
            }
        }.execute(new String[0]);
    }

    private void getChildDetailWithoutProgress() {
        new AsyncTask<String, String, String>() {
            /* access modifiers changed from: protected */
            public String doInBackground(String... params) {
                ChildrenFragment.this.object = new JSONObject();
                JSONArray jsonArray = new JSONArray();
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("parent_id", Preferences.getUserId(ChildrenFragment.this.getActivity()));
                    jsonArray.put(jsonObject);
                    ChildrenFragment.this.object.put("childs", jsonArray);
                    Log.e("response : ", ChildrenFragment.this.object.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            /* access modifiers changed from: protected */
            public void onPreExecute() {
                super.onPreExecute();
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(String s) {
                super.onPostExecute(s);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.CHILD_DETAIL_URL, ChildrenFragment.this.object, new Listener<JSONObject>() {
                    public void onResponse(JSONObject response) {
                        JSONObject jSONObject = response;
                        try {
                            boolean status = jSONObject.getBoolean("status");
                            String string = jSONObject.getString(NotificationCompat.CATEGORY_MESSAGE);
                            if (status) {
                                PrintStream printStream = System.out;
                                StringBuilder sb = new StringBuilder();
                                sb.append("Response :");
                                sb.append(jSONObject);
                                printStream.println(sb.toString());
                                try {
                                    JSONArray jsonArray = jSONObject.getJSONArray("Childs");
                                    ArrayList<String> chilDetail = new ArrayList<>();
                                    ChildrenFragment.this.childDetailList = new ArrayList<>();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject object1 = jsonArray.getJSONObject(i);
                                        if (!ChildrenFragment.this.dbVaccination.checkIsChildAlreadyInDBorNot(object1.getString(ShareConstants.WEB_DIALOG_PARAM_ID))) {
                                            ChildDetailModel childDetailModel = new ChildDetailModel(object1.getString(ShareConstants.WEB_DIALOG_PARAM_ID), object1.getString("first_name"), object1.getString("last_name"), object1.getString("image"), object1.getString("dob"), object1.getString("mother_name"), object1.getString("place_birth"), object1.getString("gender"), object1.getString("aadhar"),object1.getString("relation"),object1.getString("blood_group"));
                                            ChildrenFragment.this.dbVaccination.addAllChilds(childDetailModel);
                                        }
                                        ChildrenFragment.this.childDetailList.add(new Gson().fromJson(object1.toString(), ChildDetailModel.class));
                                        chilDetail.add(((ChildDetailModel) ChildrenFragment.this.childDetailList.get(i)).getFirst_name());
                                        chilDetail.add(((ChildDetailModel) ChildrenFragment.this.childDetailList.get(i)).getLast_name());
                                        chilDetail.add(((ChildDetailModel) ChildrenFragment.this.childDetailList.get(i)).getDob());
                                        chilDetail.add(((ChildDetailModel) ChildrenFragment.this.childDetailList.get(i)).getRelation());
                                        chilDetail.add(((ChildDetailModel) ChildrenFragment.this.childDetailList.get(i)).getBlood_group());
                                    }
                                    ChildrenFragment.this.childDetailAdapter = new ChildDetailAdapter(ChildrenFragment.this.getActivity(), ChildrenFragment.this.childDetailList);
                                    ChildrenFragment.this.childList.setAdapter(ChildrenFragment.this.childDetailAdapter);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                return;
                            }
                            ChildrenFragment.this.childList.setAdapter(null);
                            final Dialog dialogReminder = new Dialog(ChildrenFragment.this.getActivity());
                            dialogReminder.setContentView(R.layout.child_notification_dialog);
                            dialogReminder.getWindow().setLayout(-1, -2);
                            TextView textView = (TextView) dialogReminder.findViewById(R.id.child_reminderTV);
                            ((Button) dialogReminder.findViewById(R.id.btn_OK)).setOnClickListener(new OnClickListener() {
                                public void onClick(View v) {
                                    dialogReminder.dismiss();
                                }
                            });
                            dialogReminder.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                            dialogReminder.show();
                        } catch (JSONException e2) {
                            e2.printStackTrace();
                        }
                    }
                }, new ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ChildrenFragment.this.getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
                        error.printStackTrace();
                    }
                });
                AppController.getInstance().addToRequestQueue(jsonObjectRequest);
            }
        }.execute(new String[0]);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.add_child_menu, menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != R.id.addChild_filter) {
            return super.onOptionsItemSelected(item);
        }
        if (Functions.isNetworkConnected(getActivity())) {
            startActivity(new Intent(getActivity(), AddChildActivity.class));
        } else {
            Toast.makeText(getActivity(), "Please check Internet Connectivity", Toast.LENGTH_LONG).show();
        }
        return true;
    }

    public void onResume() {
        super.onResume();
        if (Functions.isNetworkConnected(getActivity())) {
            getChildDetailWithoutProgress();
        } else if (this.dbVaccination.getAllChilds().size() > 0) {
            this.childDetailList = this.dbVaccination.getAllChilds();
            this.childDetailAdapter = new ChildDetailAdapter(getActivity(), this.childDetailList);
            this.childList.setAdapter(this.childDetailAdapter);
        } else {
            Toast.makeText(getActivity(), getString(R.string.no_data), Toast.LENGTH_SHORT).show();
        }
    }
}
