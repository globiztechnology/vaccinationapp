package com.globiz.vaccinationapp.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore.Images.Media;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.ItemTouchHelper;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.FacebookSdk;
import com.facebook.internal.ServerProtocol;
import com.facebook.share.internal.ShareConstants;
import com.globiz.vaccinationapp.R;
import com.globiz.vaccinationapp.activity.AddChildActivity;
import com.globiz.vaccinationapp.activity.EditProfile;
import com.globiz.vaccinationapp.controller.AppController;
import com.globiz.vaccinationapp.customViews.CaseSensitiveLetters;
import com.globiz.vaccinationapp.customViews.CircularImageView;
import com.globiz.vaccinationapp.util.Constant;
import com.globiz.vaccinationapp.util.Functions;
import com.globiz.vaccinationapp.util.Preferences;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ProfileFragment extends Fragment implements OnClickListener {
    private int PICK_IMAGE_REQUEST = 1;
    /* access modifiers changed from: private */
    public EditText aadharNoET;
    private Button addChildBt;
    /* access modifiers changed from: private */
    public EditText addressET;
    private Bitmap bitmap;
    private TextView changePassTV;
    /* access modifiers changed from: private */
    public EditText childCountET;
    private EditText childET;
    /* access modifiers changed from: private */
    public EditText emailET;
    /* access modifiers changed from: private */
    public String finalImageUrl;
    private GoogleSignInOptions googleSigninOption;
    private ProgressDialog loading;
    private GoogleApiClient mGoogleApiClient;
    private ProgressDialog mProgressDialog;
    private JSONObject object;
    /* access modifiers changed from: private */
    public EditText phoneNoET,relationET;
    /* access modifiers changed from: private */
    public TextView placeHolder;
    /* access modifiers changed from: private */
    public TextView profileCompletenessET;
    private CircularImageView uploadImage;
    private String userEmail;
    /* access modifiers changed from: private */
    public CircularImageView userImage;
    private String userName;
    /* access modifiers changed from: private */
    public TextView userNameET;
    public String gender;
    CaseSensitiveLetters convert;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getActivity());
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        convert=new CaseSensitiveLetters();
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        this.userNameET = (TextView) view.findViewById(R.id.nameET);
        this.profileCompletenessET = (TextView) view.findViewById(R.id.profileCompletenessET);
        PrintStream printStream = System.out;
        StringBuilder sb = new StringBuilder();
        sb.append("userNameET");
        sb.append(Preferences.getUserName(getActivity()));
        printStream.println(sb.toString());
        this.emailET = (EditText) view.findViewById(R.id.emailET);
        this.phoneNoET = (EditText) view.findViewById(R.id.mobileNoET);
        this.addressET = (EditText) view.findViewById(R.id.paentAddET);
        this.relationET = (EditText) view.findViewById(R.id.paentRel);
        this.aadharNoET = (EditText) view.findViewById(R.id.aadharNoET);
        this.childCountET = (EditText) view.findViewById(R.id.count_child);
        this.addChildBt = (Button) view.findViewById(R.id.addchildBT);
        this.userImage = (CircularImageView) view.findViewById(R.id.userProfilePic);
      //  this.uploadImage = (CircularImageView) view.findViewById(R.id.changeDisplayPic);
        this.placeHolder = (TextView) view.findViewById(R.id.placeholder);
        this.bitmap = ((BitmapDrawable) this.userImage.getDrawable()).getBitmap();
      //  this.uploadImage.setOnClickListener(this);
        this.addChildBt.setOnClickListener(this);
        if (Functions.isNetworkConnected(getActivity())) {
            setAllDetail();
        } else {
            setLocalData();
        }
    }

    private void setLocalData() {
        StringBuilder sb = new StringBuilder();
        sb.append(convert.caseSensitive(Preferences.getUserFName(getActivity())));
        sb.append(" ");
        sb.append(convert.caseSensitive(Preferences.getUserLName(getActivity())));
        this.userNameET.setText(convert.caseSensitive(sb.toString()));
        this.emailET.setText(Preferences.getUserEmail(getActivity()));
        this.phoneNoET.setText(Preferences.getUserPhone(getActivity()));
        this.addressET.setText(capFirstLetter(Preferences.getUserAddress(getActivity())));
        this.relationET.setText(capFirstLetter(Preferences.getUserRelation(getActivity())));
        this.childCountET.setText(Preferences.getUserChild(getActivity()));
        if (!Preferences.getUserAadhar(getActivity()).equalsIgnoreCase("null")) {
            this.aadharNoET.setText(Preferences.getUserAadhar(getActivity()));
        } else {
            this.aadharNoET.setText("");
        }
        ProfileFragment.this.gender=Preferences.getUserGender(getActivity());
        Log.e("response : ", ProfileFragment.this.gender.toString());
    }

    public void setAllDetail() {

        this.object = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(ShareConstants.WEB_DIALOG_PARAM_ID, Preferences.getUserId(getActivity()));
            jsonArray.put(jsonObject);
            this.object.put("user", jsonArray);
            Log.e("response : ", this.object.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //showProgressDialog();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.PROFILE_DETAIL_URL, this.object, new Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                try {
                    boolean status = response.getBoolean("status");
                    String string = response.getString(NotificationCompat.CATEGORY_MESSAGE);
                    if (status) {
                      //  ProfileFragment.this.hideProgressDialog();
                        PrintStream printStream = System.out;
                        StringBuilder sb = new StringBuilder();
                        sb.append("user Response :");
                        sb.append(response);
                        printStream.println(sb.toString());
                        Log.e("response : ", sb.toString());
                        JSONObject jsonObject = response.getJSONObject("User");
                        Preferences.setUserId(ProfileFragment.this.getActivity(), jsonObject.getString(ShareConstants.WEB_DIALOG_PARAM_ID));
                        Preferences.setUserEmail(ProfileFragment.this.getActivity(), jsonObject.getString("email"));
                        Preferences.setUserPassword(ProfileFragment.this.getActivity(), jsonObject.getString("password"));
                        FragmentActivity activity = ProfileFragment.this.getActivity();
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(jsonObject.getString("firstname"));
                        sb2.append(" ");
                        sb2.append(jsonObject.getString("lastname"));
                        Preferences.setUserName(activity, sb2.toString());
                        Preferences.setUserFName(ProfileFragment.this.getActivity(), jsonObject.getString("firstname"));
                        Preferences.setUserLName(ProfileFragment.this.getActivity(), jsonObject.getString("lastname"));
                        Preferences.setUserPhone(ProfileFragment.this.getActivity(), jsonObject.getString("phone"));
                        Preferences.setUserAddress(ProfileFragment.this.getActivity(), jsonObject.getString("address"));
                        Preferences.setUserGender(ProfileFragment.this.getActivity(), jsonObject.getString("gender"));
                       // Preferences.setUserRelation(ProfileFragment.this.getActivity(), jsonObject.getString("relation"));
                        Log.e("REA",jsonObject.getString("relation"));
                        if (!jsonObject.getString("relation").equalsIgnoreCase("null")) {
                            ProfileFragment.this.relationET.setText(capFirstLetter(jsonObject.getString("relation")));
                            Preferences.setUserRelation(ProfileFragment.this.getActivity(), jsonObject.getString("relation"));
                        } else {
                            ProfileFragment.this.relationET.setText("");
                            Preferences.setUserRelation(ProfileFragment.this.getActivity(), "");
                        }
                        if (!jsonObject.getString("aadhar").equalsIgnoreCase("null")) {
                            ProfileFragment.this.aadharNoET.setText(jsonObject.getString("aadhar"));
                            Preferences.setUserAadhar(ProfileFragment.this.getActivity(), jsonObject.getString("aadhar"));
                        } else {
                            ProfileFragment.this.aadharNoET.setText("");
                            Preferences.setUserAadhar(ProfileFragment.this.getActivity(), "");
                        }
                        Preferences.setUserChild(ProfileFragment.this.getActivity(), jsonObject.getString("no_of_child"));
                        TextView access$100 = ProfileFragment.this.userNameET;
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append(jsonObject.getString("firstname"));
                        sb3.append(" ");
                        sb3.append(jsonObject.getString("lastname"));
                        access$100.setText(convert.caseSensitive(sb3.toString()));
                        ProfileFragment.this.emailET.setText(jsonObject.getString("email"));
                        ProfileFragment.this.phoneNoET.setText(jsonObject.getString("phone"));
                        if(jsonObject.getString("address").isEmpty()) {
                            ProfileFragment.this.addressET.setText(jsonObject.getString("address"));
                        }else{
                            ProfileFragment.this.addressET.setText(capFirstLetter(jsonObject.getString("address")));
                        }
                        ProfileFragment.this.addressET.setText(jsonObject.getString("address"));
                      //  ProfileFragment.this.relationET.setText(capFirstLetter(jsonObject.getString("relation")));
                        ProfileFragment.this.childCountET.setText(jsonObject.getString("no_of_child"));
                        ProfileFragment.this.gender=jsonObject.getString("gender");
                        ProfileFragment.this.finalImageUrl = Preferences.getUserImageUrl(ProfileFragment.this.getActivity());
                        Log.e("Image Name final",ProfileFragment.this.finalImageUrl);
                        String isLoggedInBy = Preferences.getIsLoggedInBy(ProfileFragment.this.getActivity());
                        String imageName = Preferences.getUserImageUrl(ProfileFragment.this.getActivity());
                        Log.e("Image Name",imageName);
                        if (!TextUtils.isEmpty(ProfileFragment.this.finalImageUrl)) {
                            if (!TextUtils.isEmpty(Preferences.getUserImageUrl(ProfileFragment.this.getActivity()))) {
                                if (ProfileFragment.this.finalImageUrl.contains(Constant.BASE_IMAGE_URL)) {
                                    if(ProfileFragment.this.gender.matches("Female")){
                                        Picasso.with(ProfileFragment.this.getActivity()).load(ProfileFragment.this.finalImageUrl).placeholder((int) R.drawable.profile_logo1).into((ImageView) ProfileFragment.this.userImage);
                                    }
                                    else {
                                        Picasso.with(ProfileFragment.this.getActivity()).load(ProfileFragment.this.finalImageUrl).placeholder((int) R.drawable.profile_logo).into((ImageView) ProfileFragment.this.userImage);
                                    }
                                } else if (ProfileFragment.this.finalImageUrl.contains("google")) {
                                    if(ProfileFragment.this.gender.matches("Female")){
                                        Picasso.with(ProfileFragment.this.getActivity()).load(ProfileFragment.this.finalImageUrl).placeholder((int) R.drawable.profile_logo1).into((ImageView) ProfileFragment.this.userImage);
                                    }else {
                                        Picasso.with(ProfileFragment.this.getActivity()).load(ProfileFragment.this.finalImageUrl).placeholder((int) R.drawable.profile_logo).into((ImageView) ProfileFragment.this.userImage);
                                    }
                                    }
                                    else {
                                    if (!ProfileFragment.this.finalImageUrl.contains("fb")) {
                                        if (!ProfileFragment.this.finalImageUrl.contains("facebook")) {
                                            ProfileFragment profileFragment = ProfileFragment.this;
                                            StringBuilder sb4 = new StringBuilder();
                                            sb4.append(Constant.BASE_IMAGE_URL);
                                            sb4.append("profileimg/");
                                            sb4.append(ProfileFragment.this.finalImageUrl);
                                            profileFragment.finalImageUrl = sb4.toString();
                                            if(ProfileFragment.this.gender.matches("Female")) {
                                                Picasso.with(ProfileFragment.this.getActivity()).load(ProfileFragment.this.finalImageUrl).placeholder((int) R.drawable.profile_logo1).into((ImageView) ProfileFragment.this.userImage);
                                            }else{
                                                Picasso.with(ProfileFragment.this.getActivity()).load(ProfileFragment.this.finalImageUrl).placeholder((int) R.drawable.profile_logo).into((ImageView) ProfileFragment.this.userImage);
                                            }
                                            }
                                    }
                                    if(ProfileFragment.this.gender.matches("Female")) {
                                        Picasso.with(ProfileFragment.this.getActivity()).load(ProfileFragment.this.finalImageUrl).placeholder((int) R.drawable.profile_logo1).into((ImageView) ProfileFragment.this.userImage);
                                    }else{
                                        Picasso.with(ProfileFragment.this.getActivity()).load(ProfileFragment.this.finalImageUrl).placeholder((int) R.drawable.profile_logo).into((ImageView) ProfileFragment.this.userImage);
                                    }
                                    }
                                PrintStream printStream2 = System.out;
                                StringBuilder sb5 = new StringBuilder();
                                sb5.append("final : ");
                                sb5.append(ProfileFragment.this.finalImageUrl);
                                printStream2.println(sb5.toString());
                                ProfileFragment.this.placeHolder.setVisibility(View.GONE);
                            } else if (imageName.equals("null")) {
                                String fName = String.valueOf(jsonObject.getString("firstname").charAt(0));
                                String lName = String.valueOf(jsonObject.getString("lastname").charAt(0));
                                TextView access$800 = ProfileFragment.this.placeHolder;
                                StringBuilder sb6 = new StringBuilder();
                                sb6.append(fName.toUpperCase());
                                sb6.append(lName.toUpperCase());
                                access$800.setText(convert.caseSensitive(sb6.toString()));
                                ProfileFragment.this.userImage.setVisibility(View.GONE);
                            }
                        }
                        ProfileFragment.this.profileCompleteness();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                String errortype = null;
                if ((error instanceof TimeoutError) || (error instanceof NoConnectionError)) {
                    errortype = "No Internet Access, check your internet connection.";
                } else if (error instanceof AuthFailureError) {
                    errortype = "Authentication error, please try again.";
                } else if (error instanceof ServerError) {
                    errortype = "Server error, please try again.";
                } else if (error instanceof NetworkError) {
                    errortype = "Network error, please try again.";
                } else if (error instanceof ParseError) {
                    errortype = "Internal error, please try again.";
                }
                Toast.makeText(ProfileFragment.this.getActivity(), errortype, Toast.LENGTH_SHORT).show();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }
    public  String capFirstLetter(String input) {

            return input.substring(0,1).toUpperCase() + input.substring(1,input.length());

    }
    public void onClick(View v) {
        int id = v.getId();
        if (id != R.id.addchildBT) {
            if (id == R.id.changeDisplayPic) {
                dialogDeleteUpload();
            }
        } else if (Functions.isNetworkConnected(getActivity())) {
            startActivity(new Intent(getActivity(), AddChildActivity.class));
        } else {
            Toast.makeText(getActivity(), R.string.check_your_internet_connectivity, Toast.LENGTH_SHORT).show();
        }
    }

    public void profileCompleteness() {
        this.object = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(ShareConstants.WEB_DIALOG_PARAM_ID, Preferences.getUserId(getActivity()));
            jsonArray.put(jsonObject);
            this.object.put("user", jsonArray);
            Log.e("response : ", this.object.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.PROFILE_COMPLETE_URL, this.object, new Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                try {
                    if (response.has("email")) {
                        PrintStream printStream = System.out;
                        StringBuilder sb = new StringBuilder();
                        sb.append("user Response :");
                        sb.append(response);
                        printStream.println(sb.toString());
                        if (response.getString("ProfileComplete").equalsIgnoreCase(String.valueOf(100))) {
                            ProfileFragment.this.profileCompletenessET.setText("Profile is 100% completed");
                            return;
                        }
                        double percentage = Double.valueOf(response.getString("ProfileComplete")).doubleValue();
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("Profile completeness ");
                        sb2.append(Math.floor(percentage * 100.0d) / 100.0d);
                        sb2.append("%");
                        ProfileFragment.this.profileCompletenessET.setText(sb2.toString());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                String errortype = null;
                if ((error instanceof TimeoutError) || (error instanceof NoConnectionError)) {
                    errortype = "No Internet Access, check your internet connection.";
                } else if (error instanceof AuthFailureError) {
                    errortype = "Authentication error, please try again.";
                } else if (error instanceof ServerError) {
                    errortype = "Server error, please try again.";
                } else if (error instanceof NetworkError) {
                    errortype = "Network error, please try again.";
                } else if (error instanceof ParseError) {
                    errortype = "Internal error, please try again.";
                }
                Toast.makeText(ProfileFragment.this.getActivity(), errortype, Toast.LENGTH_SHORT).show();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.profile_menu, menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != R.id.edit_profile_filter) {
            return super.onOptionsItemSelected(item);
        }
        startActivity(new Intent(getActivity(), EditProfile.class));
        return true;
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
//        if(imageBytes.length>250000){
//            return String.valueOf(false);
//        }
//        else{
            String encodedImage = Base64.encodeToString(imageBytes, 0);
            Bitmap bmp2 = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
            if (bmp2.getHeight() <= 400 && bmp2.getWidth() <= 400) {
                return encodedImage;
            }
            Bitmap bmp3 = Bitmap.createScaledBitmap(bmp2, 500, 500, false);
            byte[] b = baos.toByteArray();
            System.gc();
            return Base64.encodeToString(b, 2);
       // }
    }

    private void showProgressDialog() {
        if (this.mProgressDialog == null) {
            this.mProgressDialog =  new ProgressDialog(getActivity(),R.style.DialogTheme);
            this.mProgressDialog.setMessage(getString(R.string.loading));
            this.mProgressDialog.setIndeterminate(true);
        }
        this.mProgressDialog.show();
    }

    /* access modifiers changed from: private */
    public void hideProgressDialog() {
        if (this.mProgressDialog != null && this.mProgressDialog.isShowing()) {
            this.mProgressDialog.hide();
        }
    }

    private void uploadImage() {
        showProgressDialog();
        this.object = new JSONObject();
        String image = getStringImage(this.bitmap);
//        if(image=="false"){
//            ProfileFragment.this.hideProgressDialog();
//            Toast.makeText(ProfileFragment.this.getActivity(), "Upload the image less than 2MB", Toast.LENGTH_LONG).show();
//        }
//        else {
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("image", image);
                jsonObject.put(ShareConstants.WEB_DIALOG_PARAM_ID, Preferences.getUserId(getActivity()));
                jsonArray.put(jsonObject);
                this.object.put("user", jsonArray);
                Log.e("response : ", this.object.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.UPLOADIMAGE_URL, this.object, new Listener<JSONObject>() {
                public void onResponse(JSONObject response) {
                    try {
                        boolean status = response.getBoolean("status");
                        String msg = response.getString(NotificationCompat.CATEGORY_MESSAGE);
                        if (status) {
                            ProfileFragment.this.hideProgressDialog();
                            PrintStream printStream = System.out;
                            StringBuilder sb = new StringBuilder();
                            sb.append("Response s :");
                            sb.append(response);
                            printStream.println(sb.toString());
                            Preferences.setUserImageUrl(ProfileFragment.this.getActivity(), response.getString("image_name"));
                            Toast.makeText(ProfileFragment.this.getActivity(), "Photo Added Successfully", Toast.LENGTH_LONG).show();
                            Picasso with = Picasso.with(ProfileFragment.this.getActivity());
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append(Constant.BASE_IMAGE_URL);
                            sb2.append("profileimg/");
                            sb2.append(Preferences.getUserImageUrl(ProfileFragment.this.getContext()));
                            with.load(sb2.toString()).into((ImageView) ProfileFragment.this.userImage);
                            return;
                        }
                        Toast.makeText(ProfileFragment.this.getActivity(), msg, Toast.LENGTH_LONG).show();
                        ProfileFragment.this.hideProgressDialog();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new ErrorListener() {
                public void onErrorResponse(VolleyError error) {
                    ProfileFragment.this.hideProgressDialog();
                    Toast.makeText(ProfileFragment.this.getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
                    error.printStackTrace();
                }
            });
            Volley.newRequestQueue(getActivity()).add(jsonObjectRequest);
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(20000, 0, 1.0f));
      //  }
    }

    /* access modifiers changed from: private */
    public void showFileChooser() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{"android.permission.WRITE_EXTERNAL_STORAGE"}, 1);
                requestPermissions(new String[]{"android.permission.READ_EXTERNAL_STORAGE"}, 1);
            } else if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_DENIED) {

                Intent intent = new Intent("android.intent.action.PICK", Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                intent.putExtra("crop", ServerProtocol.DIALOG_RETURN_SCOPES_TRUE);
                intent.putExtra("outputX", ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION);
                intent.putExtra("outputY", ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION);
                intent.putExtra("aspectX", 1);
                intent.putExtra("aspectY", 1);
                intent.putExtra("scale", true);
                intent.putExtra("output", true);
                intent.putExtra("outputFormat", CompressFormat.JPEG.toString());
                intent.setAction("android.intent.action.GET_CONTENT");
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), this.PICK_IMAGE_REQUEST);
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == this.PICK_IMAGE_REQUEST && resultCode == -1 && data != null && data.getData() != null) {
            try {
                this.bitmap = Media.getBitmap(getActivity().getContentResolver(), data.getData());
                this.userImage.setImageBitmap(this.bitmap);

                if (Functions.isNetworkConnected(getActivity())) {
                    uploadImage();
                } else {
                    Toast.makeText(getActivity(), getString(R.string.check_your_internet_connectivity), Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    public void deleteImage() {
        showProgressDialog();
        this.object = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(ShareConstants.WEB_DIALOG_PARAM_ID, Preferences.getUserId(getActivity()));
            jsonArray.put(jsonObject);
            this.object.put("user", jsonArray);
            Log.e("response : ", this.object.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.DELETE_PROFILE_PICTURE, this.object, new Listener<JSONObject>() {
            public void onResponse(JSONObject response) {
                try {
                    boolean status = response.getBoolean("status");
                    String msg = response.getString(NotificationCompat.CATEGORY_MESSAGE);
                    if (status) {
                        ProfileFragment.this.hideProgressDialog();
                        PrintStream printStream = System.out;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Response s :");
                        sb.append(response);
                        printStream.println(sb.toString());
                        Preferences.setUserImageUrl(ProfileFragment.this.getActivity(), ProfileFragment.this.getURLForResource(R.drawable.profile_logo));
                        ProfileFragment.this.userImage.setImageDrawable(ContextCompat.getDrawable(ProfileFragment.this.getActivity(), R.drawable.profile_logo));
                        if(ProfileFragment.this.gender.matches("Female")) {
                            Picasso.with(ProfileFragment.this.getActivity()).load((int) R.drawable.profile_logo1).into((ImageView) ProfileFragment.this.userImage);
                        }else{
                            Picasso.with(ProfileFragment.this.getActivity()).load((int) R.drawable.profile_logo).into((ImageView) ProfileFragment.this.userImage);
                        }
                        Toast.makeText(ProfileFragment.this.getActivity(), "Photo deleted successfully", Toast.LENGTH_LONG).show();
                        return;
                    }
                    Toast.makeText(ProfileFragment.this.getActivity(), msg, Toast.LENGTH_LONG).show();
                    ProfileFragment.this.hideProgressDialog();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                ProfileFragment.this.hideProgressDialog();
                Toast.makeText(ProfileFragment.this.getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
                error.printStackTrace();
            }
        });
        Volley.newRequestQueue(getActivity()).add(jsonObjectRequest);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(20000, 0, 1.0f));
    }

    private void dialogDeleteUpload() {
        String[] options = {"Upload Image", "Delete Image"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle((CharSequence) "Choose an Option");
        builder.setCancelable(true);
        builder.setItems((CharSequence[]) options, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    ProfileFragment.this.showFileChooser();
                   // Toast.makeText(ProfileFragment.this.getActivity(), "Please upload image less than 2 mb", Toast.LENGTH_SHORT).show();
                } else if (which == 1) {
                    ProfileFragment.this.deleteImage();
                }
            }
        });
        builder.show();
    }

    public void onResume() {
        super.onResume();
        if (Functions.isNetworkConnected(getActivity())) {
            setAllDetail();
            return;
        }
        this.profileCompletenessET.setVisibility(View.GONE);
        setLocalData();
    }

    public String getURLForResource(int resourceId) {
        StringBuilder sb = new StringBuilder();
        sb.append("android.resource://");
        sb.append(R.class.getPackage().getName());
        sb.append("/");
        sb.append(resourceId);
        return Uri.parse(sb.toString()).toString();
    }
}

