package com.globiz.vaccinationapp.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.globiz.vaccinationapp.R;
import com.globiz.vaccinationapp.controller.AppController;
import com.globiz.vaccinationapp.util.Constant;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;


public class HelpUsFragment extends Fragment {

    public TextView aboutusTV;
    private ProgressDialog   mProgressDialog;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_help_us, container, false);
        this.aboutusTV = (TextView) view.findViewById(R.id.help_usTV);
        getAboutUsData();
        return view;
    }

    public void getAboutUsData() {
        showProgressDialog();
        AppController.getInstance().addToRequestQueue(new StringRequest(1, Constant.HELPUS, new Response.Listener<String>() {
            public void onResponse(String response) {
                try {
                    HelpUsFragment.this.hideProgressDialog();
                    HelpUsFragment.this.aboutusTV.setText(Html.fromHtml(new JSONObject(response).getJSONObject("HelpU").getString(FirebaseAnalytics.Param.CONTENT)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
            }
        }));
    }
    private void showProgressDialog() {
        if (this.mProgressDialog == null) {
            this.mProgressDialog = new ProgressDialog(getActivity(),R.style.DialogTheme);
            this.mProgressDialog.setMessage(getString(R.string.loading));
            this.mProgressDialog.setIndeterminate(true);
        }
        this.mProgressDialog.show();
    }

    /* access modifiers changed from: private */
    public void hideProgressDialog() {
        if (this.mProgressDialog != null && this.mProgressDialog.isShowing()) {
            this.mProgressDialog.hide();
        }
    }

}