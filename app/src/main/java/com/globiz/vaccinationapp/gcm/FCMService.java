package com.globiz.vaccinationapp.gcm;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;

import com.facebook.share.internal.ShareConstants;
import com.globiz.vaccinationapp.R;
import com.globiz.vaccinationapp.activity.MainActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class FCMService extends FirebaseMessagingService {
    private static final String TAG = "FCMService";
    Bitmap image;
    Notification.Builder notificationBuilder;

    public void onMessageReceived(RemoteMessage remoteMessage) {
        String tag = (String) remoteMessage.getData().get("tag");
        String msg = (String) remoteMessage.getData().get(ShareConstants.WEB_DIALOG_PARAM_MESSAGE);
        this.image = getBitmapFromURL((String) remoteMessage.getData().get("image"));
        sendNotification(tag, msg, this.image);
    }

    private void sendNotification(String tag, String messageBody, Bitmap img) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        this.notificationBuilder = new Notification.Builder(this).setSmallIcon(R.mipmap.ic_launcher).setContentTitle("Vaccine Date").setContentText(messageBody).setAutoCancel(true).setSound(RingtoneManager.getDefaultUri(2)).setContentIntent(PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT));
        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).notify(0, this.notificationBuilder.build());
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(src).openConnection();
            connection.setDoInput(true);
            connection.connect();
            return BitmapFactory.decodeStream(connection.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}

