package com.globiz.vaccinationapp.gcm;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;


public class FCMInstantIDService extends FirebaseMessagingService {
    private static final String TAG = "FCMInstantIDService";


    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.e("TAG",s);
        sendRegistrationToServer(s);
    }
//    public void onTokenRefresh() {
//        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
//        String str = TAG;
//        StringBuilder sb = new StringBuilder();
//        sb.append("Refreshed token: ");
//        sb.append(refreshedToken);
//        Log.d(str, sb.toString());
//        sendRegistrationToServer(refreshedToken);
    // }

    private void sendRegistrationToServer(String token) {
    }
}