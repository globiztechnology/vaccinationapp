package com.globiz.vaccinationapp.gcm;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.facebook.share.internal.ShareConstants;

public class SmsReceiver extends BroadcastReceiver {
    private static final String TAG = SmsReceiver.class.getSimpleName();

    public void onReceive(Context context, Intent intent) {
        Exception e;
        Bundle bundle;
        Bundle bundle2 = intent.getExtras();
        if (bundle2 != null) {
            try {
                Object[] pdusObj = (Object[]) bundle2.get("pdus");
                int length = pdusObj.length;
                char c = 0;
                int i = 0;
                while (i < length) {
                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String[] senderMessage = currentMessage.getDisplayOriginatingAddress().split("-", 2);
                    String str = senderMessage[c];
                    String second = senderMessage[1];
                    Log.i("second :", second);
                    if (second.equalsIgnoreCase("ABWTOP")) {
                        String[] result = currentMessage.getMessageBody().split(" ", 2);
                        String first = result[c];
                        String str2 = result[1];
                        bundle = bundle2;
                        try {
                            Intent myIntent = new Intent("otp");
                            myIntent.putExtra(ShareConstants.WEB_DIALOG_PARAM_MESSAGE, first);
                            LocalBroadcastManager.getInstance(context).sendBroadcast(myIntent);
                        } catch (Exception e2) {
                            e = e2;
                        }
                    } else {
                        bundle = bundle2;
                    }
                    i++;
                    bundle2 = bundle;
                    c = 0;
                }
            } catch (Exception e3) {
                Bundle bundle3 = bundle2;
                e = e3;
                String str3 = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("Exception: ");
                sb.append(e.getMessage());
                Log.e(str3, sb.toString());
                return;
            }
        }
    }
}