package com.globiz.vaccinationapp.gcm;

import android.util.Log;

import androidx.annotation.RequiresApi;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;

@RequiresApi(api = 21)
public class FirebaseJobService extends JobService {
    private static final String TAG = "FCMJobService";

    public boolean onStartJob(JobParameters jobParameters) {
        Log.d(TAG, "Performing long running task in scheduled job");
        return false;
    }

    public boolean onStopJob(JobParameters jobParameters) {
        return false;
    }
}