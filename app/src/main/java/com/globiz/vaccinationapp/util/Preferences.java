package com.globiz.vaccinationapp.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.globiz.vaccinationapp.customViews.CaseSensitiveLetters;

public class Preferences {
    private static String CHILDDOB = null;
    private static String CHILDFNAME = null;
    private static String CHILDGENDER = null;
    private static String CHILDID = null;
    private static String CHILDIMAGEURL = null;
    private static String CHILDLNAME = null;
    private static String DECODEURL = null;
    private static String IS_LOGGED_IN_BY = null;
    private static final String LOGGED_IN = "LOGGED_STATE";
    private static String PROFILEIMAGELOCALURI;
    private static String TAG = "VACCINATION::KEYS";
    private static String USERAADHAR;
    private static String USERADDRESS;
    private static String USERCHILD;
    private static String USERDOB;
    private static String USEREMAIL;
    private static String USERFNAME;
    private static String USERID;
    private static String USERIMAGEURL;
    private static String USERLNAME;
    private static String USERNAME;
    private static String USERPASSWORD;
    private static String USERPHONE;
    private static  String USERGENDER;
    private static  String USERRELATION;
    private final Context context;
    CaseSensitiveLetters convert;
    static {
        StringBuilder sb = new StringBuilder();
        sb.append(TAG);
        sb.append("USERID");
        USERID = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append(TAG);
        sb2.append("USERNAME");
        USERNAME = sb2.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append(TAG);
        sb3.append("USERFNAME");
        USERFNAME = sb3.toString();
        StringBuilder sb4 = new StringBuilder();
        sb4.append(TAG);
        sb4.append("USEREMAIL");
        USEREMAIL = sb4.toString();
        StringBuilder sb5 = new StringBuilder();
        sb5.append(TAG);
        sb5.append("USERPASSWORD");
        USERPASSWORD = sb5.toString();
        StringBuilder sb6 = new StringBuilder();
        sb6.append(TAG);
        sb6.append("USERPHONE");
        USERPHONE = sb6.toString();
        StringBuilder sb7 = new StringBuilder();
        sb7.append(TAG);
        sb7.append("USERAADHAR");
        USERAADHAR = sb7.toString();
        StringBuilder sb8 = new StringBuilder();
        sb8.append(TAG);
        sb8.append("USERIMAGEURL");
        USERIMAGEURL = sb8.toString();
        StringBuilder sb9 = new StringBuilder();
        sb9.append(TAG);
        sb9.append("CHILDIMAGEURL");
        CHILDIMAGEURL = sb9.toString();
        StringBuilder sb10 = new StringBuilder();
        sb10.append(TAG);
        sb10.append("USERDOB");
        USERDOB = sb10.toString();
        StringBuilder sb11 = new StringBuilder();
        sb11.append(TAG);
        sb11.append("PROFILEIMAGELOCALURI");
        PROFILEIMAGELOCALURI = sb11.toString();
        StringBuilder sb12 = new StringBuilder();
        sb12.append(TAG);
        sb12.append("DECODEURL");
        DECODEURL = sb12.toString();
        StringBuilder sb13 = new StringBuilder();
        sb13.append(TAG);
        sb13.append("isLoggedInBy");
        IS_LOGGED_IN_BY = sb13.toString();
        StringBuilder sb14 = new StringBuilder();
        sb14.append(TAG);
        sb14.append("USERADDRESS");
        USERADDRESS = sb14.toString();
        StringBuilder sb15 = new StringBuilder();
        sb15.append(TAG);
        sb15.append("USERLNAME");
        USERLNAME = sb15.toString();
        StringBuilder sb16 = new StringBuilder();
        sb16.append(TAG);
        sb16.append("USERCHILD");
        USERCHILD = sb16.toString();
        StringBuilder sb17 = new StringBuilder();
        sb17.append(TAG);
        sb17.append("CHILDID");
        CHILDID = sb17.toString();
        StringBuilder sb18 = new StringBuilder();
        sb18.append(TAG);
        sb18.append("CHILDFNAME");
        CHILDFNAME = sb18.toString();
        StringBuilder sb19 = new StringBuilder();
        sb19.append(TAG);
        sb19.append("CHILDDOB");
        CHILDDOB = sb19.toString();
        StringBuilder sb20 = new StringBuilder();
        sb20.append(TAG);
        sb20.append("GENDER");
        CHILDGENDER = sb20.toString();
        StringBuilder sb21 = new StringBuilder();
        sb21.append(TAG);
        sb21.append("CHILDLNAME");
        CHILDLNAME = sb21.toString();
        StringBuilder sb22 = new StringBuilder();
        sb22.append(TAG);
        sb22.append("USERGENDER");
        USERGENDER = sb22.toString();
        StringBuilder sb23 = new StringBuilder();
        sb23.append(TAG);
        sb23.append("USERRELATION");
        USERRELATION = sb23.toString();
    }

    public Preferences(Context context2) {
        this.context = context2;
        convert= new CaseSensitiveLetters();
    }

    private static SharedPreferences getPreferences(Context ctx) {
        return ctx.getSharedPreferences("", 0);
    }

    private static String getStringValue(Context ctx, String key, String value) {
        return getPreferences(ctx).getString(key, value);
    }

    private static void setValue(Context ctx, String key, String value) {
        getPreferences(ctx).edit().putString(key, value).apply();
    }

    private static int getIntValue(Context ctx, String key, int value) {
        return getPreferences(ctx).getInt(key, value);
    }

    private static void setValue(Context ctx, String key, int value) {
        getPreferences(ctx).edit().putInt(key, value).apply();
    }

    private static boolean getBooleanValue(Context ctx, String key, boolean value) {
        return getPreferences(ctx).getBoolean(key, value);
    }

    private static void setValue(Context ctx, String key, boolean value) {
        getPreferences(ctx).edit().putBoolean(key, value).apply();
    }

    public static String getUserId(Context ctx) {
        return getStringValue(ctx, USERID, "");
    }

    public static void setUserId(Context ctx, String value) {
        setValue(ctx, USERID, value);
    }

    public static String getUserFName(Context ctx) {
        return getStringValue(ctx, USERFNAME, "");
    }

    public static void setUserFName(Context ctx, String value) {
        setValue(ctx, USERFNAME, value);
    }

    public static String getUserLName(Context ctx) {
        return getStringValue(ctx, USERLNAME, "");
    }

    public static void setUserLName(Context ctx, String value) {
        setValue(ctx, USERLNAME, value);
    }

    public static String getUserEmail(Context ctx) {
        return getStringValue(ctx, USEREMAIL, "");
    }

    public static void setUserEmail(Context ctx, String value) {
        setValue(ctx, USEREMAIL, value);
    }
    public static void setUserGender(Context ctx, String value) {
        setValue(ctx, USERGENDER, value);
    }
    public static void setUserRelation(Context ctx, String value) {
        setValue(ctx, USERRELATION, value);
    }

    public static String getUserPassword(Context ctx) {
        return getStringValue(ctx, USERPASSWORD, "");
    }

    public static void setUserPassword(Context ctx, String value) {
        setValue(ctx, USERPASSWORD, value);
    }

    public static String getUserAadhar(Context ctx) {
        return getStringValue(ctx, USERAADHAR, "");
    }

    public static void setUserAadhar(Context ctx, String value) {
        setValue(ctx, USERAADHAR, value);
    }

    public static String getUserPhone(Context ctx) {
        return getStringValue(ctx, USERPHONE, "");
    }

    public static void setUserPhone(Context ctx, String value) {
        setValue(ctx, USERPHONE, value);
    }

    public static String getUserAddress(Context ctx) {
        return getStringValue(ctx, USERADDRESS, "");
    }

    public static void setUserAddress(Context ctx, String value) {
        setValue(ctx, USERADDRESS, value);
    }

    public static String getUserName(Context ctx) {
        return getStringValue(ctx, USERNAME, "");
    }
    public static String getUserGender(Context ctx) {
        return getStringValue(ctx, USERGENDER, "");
    }
    public static String getUserRelation(Context ctx) {
        return getStringValue(ctx, USERRELATION, "");
    }
    public static void setUserName(Context ctx, String value) {
        setValue(ctx, USERNAME, value);
    }

    public static String getIsLoggedInBy(Context ctx) {
        return getStringValue(ctx, IS_LOGGED_IN_BY, "");
    }

    public static void setIsLoggedInBy(Context ctx, String value) {
        setValue(ctx, IS_LOGGED_IN_BY, value);
    }

    public static void resetUserDetails(Activity activity) {
        setUserEmail(activity, "");
        setUserId(activity, "");
        setUserName(activity, "");
        setUserFName(activity, "");
        setUserPhone(activity, "");
        setUserAadhar(activity, "");
        setUserLName(activity, "");
        setUserChild(activity, "");
        setUserAddress(activity, "");
        setUserGender(activity, "");
        setUserRelation(activity, "");
    }

    public static String getUserImageUrl(Context ctx) {
        return getStringValue(ctx, USERIMAGEURL, "");
    }

    public static void setUserImageUrl(Context ctx, String value) {
        setValue(ctx, USERIMAGEURL, value);
    }

    public static String getChildImageUrl(Context ctx) {
        return getStringValue(ctx, CHILDIMAGEURL, "");
    }

    public static void setChildImageUrl(Context ctx, String value) {
        setValue(ctx, CHILDIMAGEURL, value);
    }

    public static String getUserChild(Context ctx) {
        return getStringValue(ctx, USERCHILD, "");
    }

    public static void setUserChild(Context ctx, String value) {
        setValue(ctx, USERCHILD, value);
    }

    public static String getChildId(Context ctx) {
        return getStringValue(ctx, CHILDID, "");
    }

    public static void setChildId(Context ctx, String value) {
        setValue(ctx, CHILDID, value);
    }

    public static String getChildFName(Context ctx) {
        return getStringValue(ctx, CHILDFNAME, "");
    }

    public static void setChildFName(Context ctx, String value) {
        setValue(ctx, CHILDFNAME, value);
    }

    public static String getChildLName(Context ctx) {
        return getStringValue(ctx, CHILDLNAME, "");
    }

    public static void setChildLName(Context ctx, String value) {
        setValue(ctx, CHILDLNAME, value);
    }

    public static String getChildDob(Context ctx) {
        return getStringValue(ctx, CHILDDOB, "");
    }

    public static void setChildDob(Context ctx, String value) {
        setValue(ctx, CHILDDOB, value);
    }

    public static String getChildGender(Context ctx) {
        return getStringValue(ctx, CHILDGENDER, "");
    }

    public static void setChildGender(Context ctx, String value) {
        setValue(ctx, CHILDGENDER, value);
    }
}
