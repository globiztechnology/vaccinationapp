package com.globiz.vaccinationapp.util;

import android.content.Context;
import com.google.firebase.analytics.FirebaseAnalytics.Event;

public class Constant {
    public static final String ABOUTUS;
    public static final String HELPUS;
    public static String USER_EXIST = null;
    public static String ADD_CHILD_URL = null;
    public static String BASE_IMAGE_URL = "https://heedforyou.com/vaccination/img/";
   // public static String BASE_IMAGE_URL = "https://globizdevserver.com/Vaccination/img/";
  //  public static String BASE_URL = "https://globizdevserver.com/Vaccination/mobile/";
    public static String BASE_URL = "https://heedforyou.com/vaccination/mobile/";

    public static final String CHANGE_PASSWORD;
    public static String CHILD_DETAIL_URL = null;
    public static final String COMPLETE_VACCINE_DETAIL_URL;
    public static final String COMPLETE_VACCINE_URL;
    public static final String CONTACT_URL;
    public static final String CUSTOM = "custom";
    public static String ChildId = "";
    public static final String DELETE_CHILD;
    public static final String DELETE_CHILD_PROFILE_PICTURE;
    public static final String DELETE_PROFILE_PICTURE;
    public static final String EDITNOTES;
    public static final String DELETENOTES;
    public static final String EDITPROFILE;
    public static final String EDIT_CHILD;
    public static final String FAQ;
    public static final String FBLOGIN = "fbLogin";
    public static final String FORGETPASSWORD;
    public static final String GPLUS = "gPlus";
    public static final String HOWITSWORK;
    public static final String NOTE_DETAIL_URL;
    public static final String NOTE_URL;
    public static final String PROFILE_COMPLETE_URL;
    public static final String PROFILE_DETAIL_URL;
    public static String SENDOTP_URL;
    public static String SOCIAL_LOGIN_URL;
    public static String UPLOADIMAGE_URL;
    public static String USER_LOGIN;
    public static String USER_REGISTRATION;
    public static String VACCINE_DETAIL_URL;
    public static  String CHILD_BLOOD_GROUPS;
    private static Context context;

    static {
        StringBuilder sb = new StringBuilder();
        sb.append(BASE_URL);
        sb.append("uploadImage");
        UPLOADIMAGE_URL = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append(BASE_URL);
        sb2.append(Event.LOGIN);
        USER_LOGIN = sb2.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append(BASE_URL);
        sb3.append("signup");
        USER_REGISTRATION = sb3.toString();
        StringBuilder sb4 = new StringBuilder();
        sb4.append(BASE_URL);
        sb4.append("addchild");
        ADD_CHILD_URL = sb4.toString();
        StringBuilder sb5 = new StringBuilder();
        sb5.append(BASE_URL);
        sb5.append("ChildDetails");
        CHILD_DETAIL_URL = sb5.toString();
        StringBuilder sb6 = new StringBuilder();
        sb6.append(BASE_URL);
        sb6.append("complete_vaccine");
        VACCINE_DETAIL_URL = sb6.toString();
        StringBuilder sb7 = new StringBuilder();
        sb7.append(BASE_URL);
        sb7.append("faq");
        FAQ = sb7.toString();
        StringBuilder sb8 = new StringBuilder();
        sb8.append(BASE_URL);
        sb8.append("aboutus");
        ABOUTUS = sb8.toString();
        StringBuilder sb9 = new StringBuilder();
        sb9.append(BASE_URL);
        sb9.append("how_it_works");
        HOWITSWORK = sb9.toString();
        StringBuilder sb10 = new StringBuilder();
        sb10.append(BASE_URL);
        sb10.append("fblogin");
        SOCIAL_LOGIN_URL = sb10.toString();
        StringBuilder sb11 = new StringBuilder();
        sb11.append(BASE_URL);
        sb11.append("edituser");
        EDITPROFILE = sb11.toString();
        StringBuilder sb12 = new StringBuilder();
        sb12.append(BASE_URL);
        sb12.append("forgetpassord");
        FORGETPASSWORD = sb12.toString();
        StringBuilder sb13 = new StringBuilder();
        sb13.append(BASE_URL);
        sb13.append("deletechild");
        DELETE_CHILD = sb13.toString();
        StringBuilder sb14 = new StringBuilder();
        sb14.append(BASE_URL);
        sb14.append("editchild");
        EDIT_CHILD = sb14.toString();
        StringBuilder sb15 = new StringBuilder();
        sb15.append(BASE_URL);
        sb15.append("parent_note");
        NOTE_URL = sb15.toString();
        StringBuilder sb16 = new StringBuilder();
        sb16.append(BASE_URL);
        sb16.append("view_parent_note");
        NOTE_DETAIL_URL = sb16.toString();
        StringBuilder sb17 = new StringBuilder();
        sb17.append(BASE_URL);
        sb17.append("userProfile");
        PROFILE_DETAIL_URL = sb17.toString();
        StringBuilder sb18 = new StringBuilder();
        sb18.append(BASE_URL);
        sb18.append("profileComplete");
        PROFILE_COMPLETE_URL = sb18.toString();
        StringBuilder sb19 = new StringBuilder();
        sb19.append(BASE_URL);
        sb19.append("contact");
        CONTACT_URL = sb19.toString();
        StringBuilder sb20 = new StringBuilder();
        sb20.append(BASE_URL);
        sb20.append("update_complete_vaccine");
        COMPLETE_VACCINE_URL = sb20.toString();
        StringBuilder sb21 = new StringBuilder();
        sb21.append(BASE_URL);
        sb21.append("view_complete_vaccine");
        COMPLETE_VACCINE_DETAIL_URL = sb21.toString();
        StringBuilder sb22 = new StringBuilder();
        sb22.append(BASE_URL);
        sb22.append("edit_parent_note");
        EDITNOTES = sb22.toString();
        StringBuilder sb23 = new StringBuilder();
        sb23.append(BASE_URL);
        sb23.append("sendOtp");
        SENDOTP_URL = sb23.toString();
        StringBuilder sb24 = new StringBuilder();
        sb24.append(BASE_URL);
        sb24.append("changepassword");
        CHANGE_PASSWORD = sb24.toString();
        StringBuilder sb25 = new StringBuilder();
        sb25.append(BASE_URL);
        sb25.append("deleteImage");
        DELETE_PROFILE_PICTURE = sb25.toString();
        StringBuilder sb26 = new StringBuilder();
        sb26.append(BASE_URL);
        sb26.append("deleteChildImage");
        DELETE_CHILD_PROFILE_PICTURE = sb26.toString();

        StringBuilder sb27 = new StringBuilder();
        sb27.append(BASE_URL);
        sb27.append("delete_parent_note");
       DELETENOTES = sb27.toString();

        StringBuilder sb29 = new StringBuilder();
        sb29.append(BASE_URL);
        sb29.append("helpus");
        HELPUS = sb29.toString();

        StringBuilder sb30 = new StringBuilder();
        sb30.append(BASE_URL);
        sb30.append("blood_groups");
        CHILD_BLOOD_GROUPS = sb30.toString();

        StringBuilder sb31 = new StringBuilder();
        sb31.append(BASE_URL);
        sb31.append("is_user_exsits");
        USER_EXIST = sb31.toString();
//        StringBuilder sb29 = new StringBuilder();
//        sb29.append(BASE_URL);
//        sb29.append("editchild");
//        EDIT_CHILD = sb14.toString();
    }
}

