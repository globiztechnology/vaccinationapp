package com.globiz.vaccinationapp.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.EditText;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Functions {
    public static void setErrorEditText(EditText edit, String message) {
        edit.setError(message);
        edit.requestFocus();
    }

    public static boolean isValidEmail(String email) {
        Matcher matcher = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$").matcher(email);
        StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(matcher.matches());
        Log.d("Fun", sb.toString());
        return matcher.matches();
    }

    public static boolean isValidMobile(String mobileNo) {
        if (mobileNo == null || mobileNo.length() != 10) {
            return false;
        }
        return true;
    }

    public static boolean isValidPassword(String password) {
        if (password == null || password.length() < 6) {
            return false;
        }
        return true;
    }

    public static boolean isNetworkConnected(Context context) {
        NetworkInfo netInfo = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
