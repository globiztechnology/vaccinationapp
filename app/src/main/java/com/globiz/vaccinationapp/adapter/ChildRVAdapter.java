package com.globiz.vaccinationapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;


import com.globiz.vaccinationapp.R;
import com.globiz.vaccinationapp.customViews.CaseSensitiveLetters;
import com.globiz.vaccinationapp.model.ChildDetailModel;

import java.util.ArrayList;

public class ChildRVAdapter extends RecyclerView.Adapter {
    private ArrayList<ChildDetailModel> _ChildDetailModelArrayList;
    private Context _context;
    /* access modifiers changed from: private */
    public OnItemClickListener _onItemClickListener;
    /* access modifiers changed from: private */
    public int row_index = 0;
    CaseSensitiveLetters convert;

    private class ItemListHolder extends RecyclerView.ViewHolder {
        TextView childName;

        ItemListHolder(View itemView) {
            super(itemView);
            convert=new CaseSensitiveLetters();
            this.childName = (TextView) itemView.findViewById(R.id.childName);
        }
    }

    public interface OnItemClickListener {
        void inItemClick(int i);
    }

    public ChildRVAdapter(Context context, ArrayList<ChildDetailModel> _ChildDetailModelArrayList2, OnItemClickListener onItemClickListener) {
        this._context = context;
        this._onItemClickListener = onItemClickListener;
        this._ChildDetailModelArrayList = _ChildDetailModelArrayList2;
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemListHolder(LayoutInflater.from(this._context).inflate(R.layout.child_view, parent, false));
    }

    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ChildDetailModel detailModel = (ChildDetailModel) this._ChildDetailModelArrayList.get(position);
        ItemListHolder itemListHolder = (ItemListHolder) holder;
        TextView textView = itemListHolder.childName;
        StringBuilder sb = new StringBuilder();
        sb.append(convert.caseSensitive(detailModel.getFirst_name()));
        sb.append(" ");
        sb.append(convert.caseSensitive(detailModel.getLast_name()));
        textView.setText(sb.toString());
        itemListHolder.childName.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                ChildRVAdapter.this.row_index = position;
                ChildRVAdapter.this._onItemClickListener.inItemClick(position);
                ChildRVAdapter.this.notifyDataSetChanged();
            }
        });
        if (this.row_index == position) {
            itemListHolder.childName.setBackgroundColor(ContextCompat.getColor(this._context, R.color.colorPrimary));

        } else {
            itemListHolder.childName.setBackgroundColor(ContextCompat.getColor(this._context, R.color.grey));
        }
    }

    public int getItemCount() {
        return this._ChildDetailModelArrayList.size();
    }
}
