package com.globiz.vaccinationapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.globiz.vaccinationapp.R;
import com.globiz.vaccinationapp.model.NoteDetailModel;

import java.util.ArrayList;

public class NoteDetailAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<NoteDetailModel> noteDetailList = new ArrayList<>();

    private class NoteHolder {
        /* access modifiers changed from: private */
        public TextView noteChildName;
        /* access modifiers changed from: private */
        public TextView noteDetail;
        /* access modifiers changed from: private */
        public TextView noteVaccineName;

        private NoteHolder() {
        }
    }

    public NoteDetailAdapter(Context context2, ArrayList<NoteDetailModel> noteDetailList2) {
        this.context = context2;
        this.noteDetailList = noteDetailList2;
    }

    public int getCount() {
        return this.noteDetailList.size();
    }

    public Object getItem(int position) {
        return this.noteDetailList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        NoteHolder noteHolder = new NoteHolder();
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.note_single_row, parent, false);
            noteHolder.noteVaccineName = (TextView) convertView.findViewById(R.id.noteVaccineName);
            noteHolder.noteDetail = (TextView) convertView.findViewById(R.id.noteDetail);
            noteHolder.noteChildName = (TextView) convertView.findViewById(R.id.noteChildName);
            convertView.setTag(noteHolder);
        } else {
            noteHolder = (NoteHolder) convertView.getTag();
        }
        NoteDetailModel detailModel = (NoteDetailModel) this.noteDetailList.get(position);
        TextView access$100 = noteHolder.noteVaccineName;
        StringBuilder sb = new StringBuilder();
        sb.append("Vaccine Name: ");
        sb.append(detailModel.getVaccine_name());
        access$100.setText(sb.toString());
        TextView access$200 = noteHolder.noteDetail;
        StringBuilder sb2 = new StringBuilder();
        sb2.append("Note: ");
        sb2.append(detailModel.getNote());
        access$200.setText(sb2.toString());
        TextView access$300 = noteHolder.noteChildName;
        StringBuilder sb3 = new StringBuilder();
        sb3.append("Child Name: ");
        sb3.append(detailModel.getChild_first_name());
        sb3.append(" ");
        sb3.append(detailModel.getChild_last_name());
        access$300.setText(sb3.toString());
        return convertView;
    }
}
