package com.globiz.vaccinationapp.adapter;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import com.facebook.appevents.AppEventsConstants;
import com.facebook.share.internal.ShareConstants;
import com.globiz.vaccinationapp.R;
import com.globiz.vaccinationapp.controller.AppController;
import com.globiz.vaccinationapp.database.DBVaccination;
import com.globiz.vaccinationapp.fragment.VaccineDetailFragment;
import com.globiz.vaccinationapp.model.ScheduleModel;
import com.globiz.vaccinationapp.util.Constant;
import com.globiz.vaccinationapp.util.Functions;
import com.globiz.vaccinationapp.util.Preferences;
import com.squareup.picasso.Picasso;

import java.io.PrintStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ScheduleAdapter extends BaseAdapter {
    /* access modifiers changed from: private */
    public int bgcolor = 0;
    public int iconColor=0;
    Context context;
    /* access modifiers changed from: private */
    public Dialog dialog;
    public Dialog dialog1;
    /* access modifiers changed from: private */
    public ProgressDialog loading;
    /* access modifiers changed from: private */
    public EditText noteET;
    public EditText noteVaccDate;
    /* access modifiers changed from: private */
    public JSONObject object;
    public JSONObject updateVaccine;
    /* access modifiers changed from: private */
    public Button saveNoteBT;
    ArrayList<ScheduleModel> scheduleModelArrayList = new ArrayList<>();
    public ArrayList<ScheduleModel> scheduleArrayList = new ArrayList<>();
    DBVaccination dbVaccination;
    /* access modifiers changed from: private */
    public TextView vaccineDateET;
    /* access modifiers changed from: private */
    public TextView vaccineDescET;
    /* access modifiers changed from: private */
    public TextView vaccineNameET;
    public ImageView vaccineFullImage;
    public Button cancelVaccineImageView;
    private DatePickerDialog datePickerDialog;
    public SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy");
    VaccineDetailFragment vaccineDetailFragment;
    public ScheduleAdapter scheduleAdapter;
    Calendar newCalendar;
    public ListView scheduleLV;
//    public Date convertedDate=null;

    public class ScheduleHolder {
        public RelativeLayout comple;
        public ImageView complete_vaccine;
        public ImageView imageEdit;
        public ImageView imageReminder;
        public TextView vaccineDate;
        public ImageView vaccineImageView;
        public TextView vaccineName;

        public ScheduleHolder() {
        }
    }

    public ScheduleAdapter(Context context2, ArrayList<ScheduleModel> scheduleArrayList,VaccineDetailFragment vaccineDetailFragment) {
        this.scheduleModelArrayList = scheduleArrayList;
        this.context = context2;
        newCalendar = Calendar.getInstance();
        this.vaccineDetailFragment = vaccineDetailFragment;
        this.dbVaccination = new DBVaccination(this.context);
    }

    public int getCount() {
        return this.scheduleModelArrayList.size();
    }

    public Object getItem(int position) {
        return this.scheduleModelArrayList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ScheduleHolder scheduleHolder;
        if (convertView == null) {
            scheduleHolder = new ScheduleHolder();
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.vaccine_single_row, parent, false);
            scheduleHolder.vaccineName = (TextView) convertView.findViewById(R.id.vaccineName);
            scheduleHolder.vaccineDate = (TextView) convertView.findViewById(R.id.vaccineDate);
            scheduleHolder.vaccineImageView = (ImageView) convertView.findViewById(R.id.vaccineImageView);
            scheduleHolder.imageEdit = (ImageView) convertView.findViewById(R.id.imageEdit);
            scheduleHolder.imageReminder = (ImageView) convertView.findViewById(R.id.imageReminder);
            scheduleHolder.complete_vaccine = (ImageView) convertView.findViewById(R.id.complete_vaccine);
            scheduleHolder.comple = (RelativeLayout) convertView.findViewById(R.id.complete);

            convertView.setTag(scheduleHolder);
        } else {
            scheduleHolder = (ScheduleHolder) convertView.getTag();
        }
        final ScheduleModel scheduleModel = (ScheduleModel) this.scheduleModelArrayList.get(position);
        scheduleHolder.vaccineName.setText(scheduleModel.getVaccine_name());
        scheduleHolder.vaccineDate.setText(scheduleModel.getExpire_date());
        if (!TextUtils.isEmpty(scheduleModel.getVaccine_image())) {
            Picasso with = Picasso.with(this.context);
            StringBuilder sb = new StringBuilder();
            sb.append(Constant.BASE_IMAGE_URL);
            sb.append(scheduleModel.getVaccine_image());
            with.load(sb.toString()).into(scheduleHolder.vaccineImageView);
        }
        try {

//            if (System.currentTimeMillis() > new SimpleDateFormat("yyyy-MMM-dd").parse(scheduleModel.getExpire_date()).getTime()) {
//                this.bgcolor = 1;
//                scheduleHolder.comple.setBackgroundColor(this.context.getResources().getColor(R.color.red));
//            }
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String currentDateandTime = sdf.format(new Date());
            String senddate1 = scheduleModel.getExpire_date();
            Date date4=convertStringToDate(senddate1);
            String expDate = sdf.format(date4);
            if (sdf.parse(expDate).before(sdf.parse(currentDateandTime))) {
                this.bgcolor = 1;
                scheduleHolder.comple.setBackgroundColor(this.context.getResources().getColor(R.color.red));
            }else {
                scheduleHolder.comple.setBackgroundColor(this.context.getResources().getColor(R.color.design_fab_shadow_end_color));
                this.bgcolor = 0;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        scheduleHolder.vaccineImageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ScheduleAdapter.this.dialog = new Dialog(ScheduleAdapter.this.context);
                ScheduleAdapter.this.dialog.setContentView(R.layout.vaccine_imageview_dialog);
                ScheduleAdapter.this.dialog.getWindow().setLayout(-1, -2);
                ScheduleAdapter.this.vaccineFullImage=(ImageView) dialog.findViewById(R.id.vaccine_full_image);
                ScheduleAdapter.this.cancelVaccineImageView=(Button) dialog.findViewById(R.id.btn_Cancel);
                Drawable drawable = (scheduleHolder.vaccineImageView).getDrawable();
                if(drawable != null){
                    ScheduleAdapter.this.vaccineFullImage.setImageDrawable(drawable);
                }
                ScheduleAdapter.this.cancelVaccineImageView.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ScheduleAdapter.this.dialog.cancel();
                    }
                });
                ScheduleAdapter.this.dialog.show();
            }

        });
        scheduleHolder.imageEdit.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ScheduleAdapter.this.dialog = new Dialog(ScheduleAdapter.this.context);
                ScheduleAdapter.this.dialog.setContentView(R.layout.edit_vaccine_detail);
                ScheduleAdapter.this.dialog.getWindow().setLayout(-1, -2);
                ScheduleAdapter.this.vaccineNameET = (TextView) ScheduleAdapter.this.dialog.findViewById(R.id.vaccineNameET);
                ScheduleAdapter.this.vaccineDescET = (TextView) ScheduleAdapter.this.dialog.findViewById(R.id.vaccineDescET);
                ScheduleAdapter.this.vaccineDateET = (TextView) ScheduleAdapter.this.dialog.findViewById(R.id.vaccineDateET);
                ScheduleAdapter.this.noteVaccDate=(EditText) ScheduleAdapter.this.dialog.findViewById(R.id.VaccDateET);
                ScheduleAdapter.this.noteET = (EditText) ScheduleAdapter.this.dialog.findViewById(R.id.noteET);
                ScheduleAdapter.this.vaccineNameET.setText(scheduleModel.getVaccine_name());
                ScheduleAdapter.this.vaccineDescET.setText(scheduleModel.getVaccine_description());

                ScheduleAdapter.this.vaccineDateET.setText(scheduleModel.getExpire_date());
                String date=scheduleModel.getVaccine_date();
                if(!date.isEmpty()){
                    Log.e("DATE",scheduleModel.getVaccine_date());
                    ScheduleAdapter.this.noteVaccDate.setText(scheduleModel.getVaccine_date());
//                    ScheduleAdapter.this.noteVaccDate.setFocusable(false);
//                    ScheduleAdapter.this.noteVaccDate.setEnabled(false);
                //    ScheduleAdapter.this.noteVaccDate.setCursorVisible(false);
                 //   ScheduleAdapter.this.noteVaccDate.setKeyListener(null);
                  //  ScheduleAdapter.this.noteVaccDate.setBackgroundColor(Color.TRANSPARENT);
                }
//                if(date.isEmpty()){
                    ScheduleAdapter.this.noteVaccDate.setOnTouchListener(new View.OnTouchListener() {
                        public boolean onTouch(View v, MotionEvent event) {
                            if (event.getAction() != 1) {
                                return false;
                            }else{
                                ScheduleAdapter.this.datePickerDialog(scheduleModel.getExpire_date());
                            }
                            return true;
                        }
                    });
              //  }
                ScheduleAdapter.this.saveNoteBT = (Button) ScheduleAdapter.this.dialog.findViewById(R.id.saveNoteBT);
                ScheduleAdapter.this.saveNoteBT.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        String complete_vacc_id = scheduleModel.getId();
                        String parent_id = Preferences.getUserId(ScheduleAdapter.this.context);
                        String message = ScheduleAdapter.this.noteET.getText().toString().trim();
                        String date= ScheduleAdapter.this.noteVaccDate.getText().toString().trim();
                        if (Functions.isNetworkConnected(ScheduleAdapter.this.context)) {
                            ScheduleAdapter.this.sendNotes(complete_vacc_id, parent_id, message,date);
                            ScheduleAdapter.this.dialog.cancel();
//                            if(date.isEmpty()) {
//                                Functions.setErrorEditText(ScheduleAdapter.this.noteVaccDate, "Date cannot be empty");
//                            }else {
//
//
//
//                            }
//
//                            else if (message.isEmpty()) {
//                                Functions.setErrorEditText(ScheduleAdapter.this.noteET, "Note cannot be empty");
//                            }
                        } else {
                            Toast.makeText(ScheduleAdapter.this.context, R.string.check_your_internet_connectivity, Toast.LENGTH_LONG).show();
                            ScheduleAdapter.this.dialog.cancel();
                        }
                    }
                });
                ScheduleAdapter.this.dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                ScheduleAdapter.this.dialog.show();
            }
        });
        /* access modifiers changed from: private */

        scheduleHolder.imageReminder.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                String senddate1 = String.valueOf(scheduleModel.getExpire_date());

                Date date4=convertStringToDate(senddate1);
                SimpleDateFormat sd=new SimpleDateFormat("dd-MM-yyyy");
                String senddate=sd.format(date4);
                System.currentTimeMillis();
                Calendar cal = Calendar.getInstance();
                String[] date1 = senddate.split("-");
                cal.set(Integer.parseInt(date1[0]), Integer.parseInt(date1[1]) - 1, Integer.parseInt(date1[2]));
                Calendar date = Calendar.getInstance();
                Intent intent = new Intent("android.intent.action.EDIT");
                intent.setType("vnd.android.cursor.item/event");
                intent.putExtra("beginTime", cal.getTimeInMillis());
                intent.putExtra("allDay", false);
                intent.putExtra("rrule", "FREQ=YEARLY");
                intent.putExtra("endTime", date.getTimeInMillis() + 3600000);
                String str = ShareConstants.WEB_DIALOG_PARAM_TITLE;
                StringBuilder sb = new StringBuilder("Reminder for ");
                sb.append(scheduleModel.getChild_name() + "-"+ scheduleModel.getVaccine_name());
                sb.append(" vaccine");
                intent.putExtra(str, sb.toString());
                ScheduleAdapter.this.context.startActivity(intent);

            }

            private Date convertStringToDate(String senddate1) {
                Date date = null;
                Date formatteddate = null;
                DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                try{
                    date = df.parse(senddate1);
                    //formatteddate = df.format(date);
                }
                catch ( Exception ex ){
                    System.out.println(ex);
                }
                return date;
            }
        });
        scheduleHolder.complete_vaccine.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
                String currentDateandTime = sdf.format(new Date());
                String expDate = scheduleModel.getExpire_date();
                try {
                    if (sdf.parse(expDate).before(sdf.parse(currentDateandTime))) {

                        String id = scheduleModel.getId();
                        if (Functions.isNetworkConnected(ScheduleAdapter.this.context)) {

                          ScheduleAdapter.this.bgcolor = 1;
                           ScheduleAdapter.this.completeVaccine(id);
                           scheduleHolder.comple.setBackgroundColor(ScheduleAdapter.this.context.getResources().getColor(R.color.red));

                        } else {
                            Toast.makeText(ScheduleAdapter.this.context, R.string.check_your_internet_connectivity, Toast.LENGTH_LONG).show();
                        }
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(ScheduleAdapter.this.context);
                        builder1.setMessage((CharSequence) "Vaccination date has passed.");
                        builder1.setCancelable(true);
                        builder1.setNegativeButton((CharSequence) "Back", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                        builder1.create().show();
                        ScheduleAdapter.this.bgcolor = 1;
                    } else if (sdf.parse(expDate).equals(sdf.parse(currentDateandTime))) {
                        ScheduleAdapter.this.completeVaccine(scheduleModel.getId());
                        scheduleHolder.comple.setBackgroundColor(ScheduleAdapter.this.context.getResources().getColor(R.color.red));
                        AlertDialog.Builder builder12 = new AlertDialog.Builder(ScheduleAdapter.this.context);
                        builder12.setMessage((CharSequence) "Vaccination date has passed.");
                        builder12.setCancelable(true);
                        builder12.setNegativeButton((CharSequence) "Back", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                        builder12.create().show();
                          ScheduleAdapter.this.bgcolor = 1;
                    } else {
                        AlertDialog.Builder builder13 = new AlertDialog.Builder(ScheduleAdapter.this.context);
                        builder13.setMessage((CharSequence) "Vaccination is still on Schedule.");
                        builder13.setCancelable(true);
                        builder13.setNegativeButton((CharSequence) "Back", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                        builder13.create().show();
                           ScheduleAdapter.this.bgcolor = 0;
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
        if (scheduleModel.getStatus().equals(AppEventsConstants.EVENT_PARAM_VALUE_YES)) {
            scheduleHolder.comple.setBackgroundColor(this.context.getResources().getColor(R.color.red));
            notifyDataSetChanged();
        }
        return convertView;
    }

    private void datePickerDialog(String expire_date) {

        DatePickerDialog datePickerDialog2 = new DatePickerDialog(ScheduleAdapter.this.context,R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
              //  ScheduleAdapter.this.datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                newCalendar.set(year, monthOfYear, dayOfMonth);
                //ScheduleAdapter.this.datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                String selectdate = ScheduleAdapter.this.dateFormatter.format(newCalendar.getTime());
                String currentDateandTime = new SimpleDateFormat("dd-MMM-yyyy").format(new Date());


              //  try {
                    ScheduleAdapter.this.noteVaccDate.setText(ScheduleAdapter.this.dateFormatter.format(newCalendar.getTime()));
                   // ScheduleAdapter.this.datePickerDialog.getDatePicker().setMaxDate(ScheduleAdapter.this.dateFormatter.parse(expire_date)));
//                    if (ScheduleAdapter.this.dateFormatter.parse(selectdate).before(ScheduleAdapter.this.dateFormatter.parse(expire_date))) {
//                        ScheduleAdapter.this.noteVaccDate.setText(ScheduleAdapter.this.dateFormatter.format(newCalendar.getTime()));
//
//                    } else if (ScheduleAdapter.this.dateFormatter.parse(selectdate).equals(ScheduleAdapter.this.dateFormatter.parse(expire_date))) {
//                        ScheduleAdapter.this.noteVaccDate.setText(ScheduleAdapter.this.dateFormatter.format(newCalendar.getTime()));
//                    } else
//                        if (ScheduleAdapter.this.dateFormatter.parse(selectdate).after(ScheduleAdapter.this.dateFormatter.parse(expire_date))) {
//                                                final Dialog dialog = new Dialog(ScheduleAdapter.this.context);
//                     dialog.setContentView(R.layout.dob_validate_dialog);
//                        ((Button) dialog.findViewById(R.id.okBtn)).setOnClickListener(new OnClickListener() {
//                            public void onClick(View v) {
//                                dialog.dismiss();
//                            }
//                        });
//                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
//                        dialog.show();
//                    }
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        this.datePickerDialog = datePickerDialog2;
        this.datePickerDialog.show();
    }

    private Date convertStringToDate(String senddate1) {
        Date date = null;
        Date formatteddate = null;
        DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        try{
            date = df.parse(senddate1);
        }
        catch ( Exception ex ){
            System.out.println(ex);
        }
        return date;
    }

    /* access modifiers changed from: private */
    public void completeVaccine(final String id) {
        new AsyncTask<String, String, String>() {
            /* access modifiers changed from: protected */
            public String doInBackground(String... params) {
                ScheduleAdapter.this.object = new JSONObject();
                JSONArray jsonArray = new JSONArray();
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put(ShareConstants.WEB_DIALOG_PARAM_ID, id);
                    jsonObject.put("color", ScheduleAdapter.this.bgcolor);
                    jsonArray.put(jsonObject);
                    ScheduleAdapter.this.object.put("CompleteVaccine", jsonArray);
                    Log.e("response : ", ScheduleAdapter.this.object.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            /* access modifiers changed from: protected */
            public void onPreExecute() {
                super.onPreExecute();
//                ScheduleAdapter.this.loading =new ProgressDialog(ScheduleAdapter.this.context,R.style.DialogTheme);
//                ScheduleAdapter.this.loading.setMessage("Please wait...");
//                ScheduleAdapter.this.loading.setTitle("Uploading...");
//                ScheduleAdapter.this.loading.show( );
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(String s) {
                super.onPostExecute(s);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.COMPLETE_VACCINE_URL, ScheduleAdapter.this.object, new Listener<JSONObject>() {
                    public void onResponse(JSONObject response) {
                        try {
                            boolean status = response.getBoolean("status");
                            String string = response.getString(NotificationCompat.CATEGORY_MESSAGE);
                            if (status) {
//                                ScheduleAdapter.this.loading.dismiss();
                                PrintStream printStream = System.out;
                                StringBuilder sb = new StringBuilder();
                                sb.append("Response :");
                                sb.append(response);
                                printStream.println(sb.toString());
                                return;
                            }
//                            ScheduleAdapter.this.loading.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                    }
                });
                AppController.getInstance().addToRequestQueue(jsonObjectRequest);
            }
        }.execute(new String[0]);
    }

    public void sendNotes(final String complete_vacc_id, final String parent_id, final String message,final String date) {
        new AsyncTask<String, String, String>() {
            /* access modifiers changed from: protected */
            public String doInBackground(String... params) {
                ScheduleAdapter.this.object = new JSONObject();
                JSONArray jsonArray = new JSONArray();
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("complete_vacc_id", complete_vacc_id);
                    jsonObject.put("parent_id", parent_id);
                    jsonObject.put("vaccine_date", date);
                    jsonObject.put(ShareConstants.WEB_DIALOG_PARAM_MESSAGE, message);
                    jsonArray.put(jsonObject);
                    ScheduleAdapter.this.object.put("note", jsonArray);
                    Log.e("response : ", ScheduleAdapter.this.object.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            /* access modifiers changed from: protected */
            public void onPreExecute() {
                super.onPreExecute();
                ScheduleAdapter.this.loading = new ProgressDialog(ScheduleAdapter.this.context,R.style.DialogTheme);
                ScheduleAdapter.this.loading.setMessage("Please wait...");
                ScheduleAdapter.this.loading.setTitle("Uploading...");
                ScheduleAdapter.this.loading.show( );
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(String s) {
                super.onPostExecute(s);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.NOTE_URL, ScheduleAdapter.this.object, new Listener<JSONObject>() {
                    public void onResponse(JSONObject response) {
                        try {
                            boolean status = response.getBoolean("status");
                            String msg = response.getString(NotificationCompat.CATEGORY_MESSAGE);
                            vaccineDetailFragment.getSchedule();
                            if (status) {
                                ScheduleAdapter.this.loading.dismiss();

                                PrintStream printStream = System.out;
                                StringBuilder sb = new StringBuilder();
                                sb.append("Response :");
                                sb.append(response);
                                printStream.println(sb.toString());
                                ScheduleAdapter.this.dialog.dismiss();
                               // vaccineDetailFragment.getSchedule();
                               //    ScheduleAdapter.this.getSchedule();
                                Toast.makeText(ScheduleAdapter.this.context, msg, Toast.LENGTH_LONG).show();
                                return;
                            }
                            ScheduleAdapter.this.loading.dismiss();
                            Toast.makeText(ScheduleAdapter.this.context, msg, Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                    }
                });
                AppController.getInstance().addToRequestQueue(jsonObjectRequest);
            }
        }.execute(new String[0]);
    }

    private void getSchedule() {
        vaccineDetailFragment.getSchedule();
        ScheduleAdapter.this.loading.dismiss();

    }

//    private  void getSchedule(){
//        new AsyncTask<String, String, String>() {
//            /* access modifiers changed from: protected */
//            public String doInBackground(String... params) {
//                ScheduleAdapter.this.updateVaccine = new JSONObject();
//                JSONArray jsonArray = new JSONArray();
//                JSONObject jsonObject = new JSONObject();
//                try {
//                    jsonObject.put(ShareConstants.WEB_DIALOG_PARAM_ID, Constant.ChildId);
//                    jsonArray.put(jsonObject);
//                    ScheduleAdapter.this.updateVaccine.put("child", jsonArray);
//                    Log.e("response vaccine- ", ScheduleAdapter.this.updateVaccine.toString());
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                return null;
//            }
//
//            /* access modifiers changed from: protected */
//            public void onPreExecute() {
//                super.onPreExecute();
//                ScheduleAdapter.this.loading = new ProgressDialog(ScheduleAdapter.this.context,R.style.DialogTheme);
//                ScheduleAdapter.this.loading.setMessage("Please wait...");
//                ScheduleAdapter.this.loading.setTitle("Uploading...");
//                ScheduleAdapter.this.loading.show( );
//            }
//
//            /* access modifiers changed from: protected */
//            public void onPostExecute(String s) {
//                super.onPostExecute(s);
//                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.VACCINE_DETAIL_URL,ScheduleAdapter.this.updateVaccine, new Listener<JSONObject>() {
//                    public void onResponse(JSONObject response) {
//                        JSONObject jSONObject = response;
//                        try {
//                            boolean status = jSONObject.getBoolean("status");
//                            String string = jSONObject.getString(NotificationCompat.CATEGORY_MESSAGE);
//                            if (status) {
//                                ScheduleAdapter.this.loading.dismiss();
//                                PrintStream printStream = System.out;
//                                StringBuilder sb = new StringBuilder();
//                                sb.append("Response :");
//                                sb.append(jSONObject);
//                                printStream.println(sb.toString());
//                                try {
//                                    ScheduleAdapter.this.loading.dismiss();
//                                    Log.e("response Vaccine ", jSONObject.getJSONArray("Complete Vaccine").toString());
//                                    JSONArray jsonArray = jSONObject.getJSONArray("Complete Vaccine");
//                                    ArrayList<String> scheduleDetail = new ArrayList<>();
//                                    for (int i = 0; i < jsonArray.length(); i++) {
//                                        JSONObject object1 = jsonArray.getJSONObject(i);
//                                       ScheduleAdapter.this.scheduleArrayList.add(new Gson().fromJson(object1.toString(), ScheduleModel.class));
//                                        if (!ScheduleAdapter.this.dbVaccination.checkIsDataAlreadyInDBorNot(object1.getString(ShareConstants.WEB_DIALOG_PARAM_ID))) {
//                                            ScheduleModel scheduleModel = new ScheduleModel(object1.getString(ShareConstants.WEB_DIALOG_PARAM_ID), object1.getString("expire_date"), object1.getString("status"), object1.getString("vaccine_name"), object1.getString("child_id"), object1.getString("vaccine_description"), object1.getString("vaccine_image"), object1.getString("vaccine_date"));
//                                            ScheduleAdapter.this.dbVaccination.addVaccinations(scheduleModel);
//                                        }
//                                        scheduleDetail.add(((ScheduleModel) ScheduleAdapter.this.scheduleArrayList.get(i)).getId());
//                                        scheduleDetail.add(((ScheduleModel) ScheduleAdapter.this.scheduleArrayList.get(i)).getVaccine_name());
//                                        scheduleDetail.add(((ScheduleModel) ScheduleAdapter.this.scheduleArrayList.get(i)).getExpire_date());
//                                        scheduleDetail.add(((ScheduleModel) ScheduleAdapter.this.scheduleArrayList.get(i)).getVaccine_description());
//                                    }
//                                    ScheduleAdapter.this.loading.dismiss();
//                                    //  Collections.sort(VaccineDetailFragment.this.scheduleArrayList);
//                                    ScheduleAdapter.this.scheduleAdapter = new ScheduleAdapter(ScheduleAdapter.this.context, ScheduleAdapter.this.scheduleArrayList,ScheduleAdapter.this.vaccineDetailFragment);
//                                    ScheduleAdapter.this.scheduleLV.setAdapter(ScheduleAdapter.this.scheduleAdapter);
//                                    ScheduleAdapter.this.scheduleAdapter.notifyDataSetChanged();
//                                } catch (JSONException e) {
//                                    ScheduleAdapter.this.loading.dismiss();
//                                    e.printStackTrace();
//                                }
//                                return;
//                            }
//                            ScheduleAdapter.this.loading.dismiss();
//                        } catch (JSONException e2) {
//                            ScheduleAdapter.this.loading.dismiss();
//                            Log.e("response Vaccinedd ", e2.toString());
//
//                            e2.printStackTrace();
//                        }
//                    }
//                }, new ErrorListener() {
//                    public void onErrorResponse(VolleyError error) {
//                        ScheduleAdapter.this.loading.dismiss();
//                        Log.e("response Vaccine ", error.toString());
//                    }
//                });
//                AppController.getInstance().addToRequestQueue(jsonObjectRequest);
//            }
//        }.execute(new String[0]);
//    }
}
