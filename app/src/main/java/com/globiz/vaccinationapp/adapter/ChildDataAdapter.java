package com.globiz.vaccinationapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;


import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.globiz.vaccinationapp.R;
import com.globiz.vaccinationapp.customViews.CaseSensitiveLetters;
import com.globiz.vaccinationapp.model.ChildDetailModel;

import java.util.List;

public class ChildDataAdapter extends RecyclerView.Adapter {
    private static final int MULTIPLE = 0;
    private static final int SINGLE = 1;
    private static int sModo = 0;
    private static int sPosition;
    /* access modifiers changed from: private */
    public OnItemClickListener _onItemClickListener;
    private Context context;
    private List<ChildDetailModel> modelArrayList;
    private int row_index = 0;
    CaseSensitiveLetters convert;

    private class ItemListHolder extends RecyclerView.ViewHolder {
        /* access modifiers changed from: private */
        public TextView childName;

        ItemListHolder(View itemView) {
            super(itemView);
            this.childName = (TextView) itemView.findViewById(R.id.childName);
            this.childName.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    ChildDataAdapter.this._onItemClickListener.inItemClick(ItemListHolder.this.getLayoutPosition());
                }
            });
        }
    }

    public interface OnItemClickListener {
        void inItemClick(int i);
    }

    public ChildDataAdapter(Context context2, List<ChildDetailModel> childDetailList, OnItemClickListener onItemClickListener) {
        this.context = context2;
        convert=new CaseSensitiveLetters();
        this.modelArrayList = childDetailList;

    }

    public ItemListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemListHolder(LayoutInflater.from(this.context).inflate(R.layout.child_view, parent, false));
    }

    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ChildDetailModel detailModel = (ChildDetailModel) this.modelArrayList.get(position);
        ItemListHolder itemListHolder = (ItemListHolder) holder;
        TextView access$000 = itemListHolder.childName;
        StringBuilder sb = new StringBuilder();
        sb.append(convert.caseSensitive(detailModel.getFirst_name()));
        sb.append(" ");
        sb.append(convert.caseSensitive(detailModel.getLast_name()));
        access$000.setText(sb.toString());
        if (this.row_index == position) {
            itemListHolder.childName.setBackgroundColor(ContextCompat.getColor(this.context,R.color.colorPrimary));
        } else {
            itemListHolder.childName.setBackgroundColor(ContextCompat.getColor(this.context,R.color.grey));
        }
    }

    public int getItemCount() {
        return this.modelArrayList.size();
    }
}
