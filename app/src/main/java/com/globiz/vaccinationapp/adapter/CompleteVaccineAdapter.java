package com.globiz.vaccinationapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.globiz.vaccinationapp.R;
import com.globiz.vaccinationapp.model.ScheduleModel;

import java.util.ArrayList;

public class CompleteVaccineAdapter extends BaseAdapter {
    Context context;
    private ArrayList<ScheduleModel> vaccineDetailList = new ArrayList<>();

    public class VaccineDetailHolder {
        public TextView vaccineDate;
        public TextView vaccineName;

        public VaccineDetailHolder() {
        }
    }

    public CompleteVaccineAdapter(Context context2, ArrayList<ScheduleModel> scheduleArrayList) {
        this.context = context2;
        this.vaccineDetailList = scheduleArrayList;
    }

    public int getCount() {
        return this.vaccineDetailList.size();
    }

    public Object getItem(int position) {
        return this.vaccineDetailList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        VaccineDetailHolder vaccinedetailHolder = new VaccineDetailHolder();
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.complete_vaccine_row, parent, false);
            vaccinedetailHolder.vaccineName = (TextView) convertView.findViewById(R.id.vaccineName);
            vaccinedetailHolder.vaccineDate = (TextView) convertView.findViewById(R.id.vaccineDate);
            convertView.setTag(vaccinedetailHolder);
        } else {
            vaccinedetailHolder = (VaccineDetailHolder) convertView.getTag();
        }
        ScheduleModel scheduleModel = (ScheduleModel) this.vaccineDetailList.get(position);
        vaccinedetailHolder.vaccineName.setText(scheduleModel.getVaccine_name());
        vaccinedetailHolder.vaccineDate.setText(scheduleModel.getExpire_date());
        return convertView;
    }
}
