package com.globiz.vaccinationapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.globiz.vaccinationapp.R;
import com.globiz.vaccinationapp.model.FaqModel;

import java.util.ArrayList;

public class FAQAdapter extends BaseExpandableListAdapter {
    private Context context;
    private ArrayList<FaqModel> groups;

    public FAQAdapter(Context context2, ArrayList<FaqModel> groups2) {
        this.context = context2;
        this.groups = groups2;
    }

    public Object getChild(int i, int i2) {
        return this.groups.get(i);
    }

    public long getChildId(int i, int i2) {
        return (long) i2;
    }

    public View getChildView(int i, int i2, boolean z, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.faq_single_row, viewGroup, false);
        }

        ImageView  img=view.findViewById(R.id.icon);
        LinearLayout layout=view.findViewById(R.id.layout);
        img.setVisibility(View.GONE);
        TextView textView = (TextView) view.findViewById(R.id.question_row);
        layout.setBackgroundColor(viewGroup.getContext().getColor(R.color.white));
        textView.setTextColor(viewGroup.getContext().getResources().getColor(R.color.black));
        textView.setText(((FaqModel) this.groups.get(i)).getAnswer());
        textView.setTextSize(16.0f);

        return view;
    }

    public int getChildrenCount(int i) {
        return 1;
    }

    public Object getGroup(int i) {
        return this.groups.get(i);
    }

    public int getGroupCount() {
        return this.groups.size();
    }

    public long getGroupId(int i) {
        return (long) i;
    }

    public View getGroupView(int i, boolean z, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.faq_single_row, null);
        }
        TextView textView = (TextView) view.findViewById(R.id.question_row);
        ImageView img = (ImageView) view.findViewById(R.id.icon);
        textView.setText(((FaqModel) this.groups.get(i)).getQuestion());
        textView.setTextColor(viewGroup.getContext().getResources().getColor(R.color.white));
        textView.setTextSize(16.0f);
            if (z) {
                img.setImageResource(R.drawable.up_arrow);
            } else {
                img.setImageResource(R.drawable.down);
            }

        return view;
    }

    public boolean hasStableIds() {
        return false;
    }

    public boolean isChildSelectable(int i, int i2) {
        return false;
    }
}
