package com.globiz.vaccinationapp.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.share.internal.ShareConstants;
import com.globiz.vaccinationapp.R;
import com.globiz.vaccinationapp.activity.EditChildActivity;
import com.globiz.vaccinationapp.controller.AppController;
import com.globiz.vaccinationapp.customViews.CaseSensitiveLetters;
import com.globiz.vaccinationapp.customViews.CircularImageView;
import com.globiz.vaccinationapp.database.DBVaccination;
import com.globiz.vaccinationapp.model.ChildDetailModel;
import com.globiz.vaccinationapp.util.Constant;
import com.globiz.vaccinationapp.util.Functions;
import com.squareup.picasso.Picasso;
import java.io.PrintStream;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ChildDetailAdapter extends BaseAdapter {
    /* access modifiers changed from: private */
    public ArrayList<ChildDetailModel> childDetailList = new ArrayList<>();
    Context context;
    /* access modifiers changed from: private */
    public DBVaccination dbVaccination;
    /* access modifiers changed from: private */
    public ProgressDialog loading;
    /* access modifiers changed from: private */
    public JSONObject object;
    public String gender;
CaseSensitiveLetters convert;
    public class ChildDetailHolder {
        public TextView childDOB;
        public CircularImageView childImage;
        public TextView childName;
        public ImageView delete_child;
        public ImageView edit_child;

        public ChildDetailHolder() {
        }
    }

    public ChildDetailAdapter(Context context2, ArrayList<ChildDetailModel> childDetailList2) {
        this.context = context2;
        convert=new CaseSensitiveLetters();
        this.childDetailList = childDetailList2;
    }

    public int getCount() {
        return this.childDetailList.size();
    }

    public Object getItem(int i) {
        return this.childDetailList.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ChildDetailHolder detailHolder = new ChildDetailHolder();
        this.dbVaccination = new DBVaccination(this.context);
        if (view == null) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.child_single_row, viewGroup, false);
            detailHolder.childName = (TextView) view.findViewById(R.id.child_name);
            detailHolder.childDOB = (TextView) view.findViewById(R.id.child_dob);
            detailHolder.edit_child = (ImageView) view.findViewById(R.id.edit_child);
            detailHolder.delete_child = (ImageView) view.findViewById(R.id.delete_child);
            detailHolder.childImage = (CircularImageView) view.findViewById(R.id.child_pic);
            view.setTag(detailHolder);
        } else {
            detailHolder = (ChildDetailHolder) view.getTag();
        }
        final ChildDetailModel detailModel = (ChildDetailModel) this.childDetailList.get(i);
        TextView textView = detailHolder.childName;
        StringBuilder sb = new StringBuilder();
        sb.append(convert.caseSensitive(detailModel.getFirst_name()));
        sb.append(" ");
        sb.append(convert.caseSensitive(detailModel.getLast_name()));
        textView.setText(sb.toString());
        detailHolder.childDOB.setText(detailModel.getDob());
        StringBuilder sb2 = new StringBuilder();
        sb2.append(Constant.BASE_IMAGE_URL);
        sb2.append("childimg/");
        String image = sb2.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append(image);
        sb3.append(detailModel.getImage());
        String finalImage = sb3.toString().replaceAll(" ", "%20");
        PrintStream printStream = System.out;
        StringBuilder sb4 = new StringBuilder();
        sb4.append("image :");
        sb4.append(finalImage);
        printStream.println(sb4.toString());
        gender=detailModel.getGender();
        if(this.gender.matches("Female")) {
            Picasso.with(context).load(finalImage).placeholder((int) R.drawable.profile_logo1).into((ImageView) detailHolder.childImage);
        }else {
            Picasso.with(context).load(finalImage).placeholder((int) R.drawable.profile_logo).into((ImageView) detailHolder.childImage);
        }
        detailHolder.edit_child.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(ChildDetailAdapter.this.context, EditChildActivity.class);
                intent.putExtra("child_id", detailModel.getId());
                intent.putExtra("first_name", detailModel.getFirst_name());
                intent.putExtra("last_name", detailModel.getLast_name());
                intent.putExtra("mother_name", detailModel.getMother_name());
                intent.putExtra("place_birth", detailModel.getPlace_birth());
                intent.putExtra("child_dob", detailModel.getDob());
                intent.putExtra("child_aadhar", detailModel.getAadhar());
                intent.putExtra("blood_group",detailModel.getBlood_group());
                Log.d("ccc",detailModel.getBlood_group()+detailModel.getDob());
                if (detailModel.getGender().equals("Male")) {
                    intent.putExtra("gender", detailModel.getGender());
                } else {
                    intent.putExtra("gender", detailModel.getGender());
                }
                intent.putExtra("image", detailModel.getImage());
                ChildDetailAdapter.this.context.startActivity(intent);
            }
        });
        final String childID = detailModel.getId();
        detailHolder.delete_child.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                final Dialog dialogReminder = new Dialog(ChildDetailAdapter.this.context);
                dialogReminder.setContentView(R.layout.child_delete_dialog);
                dialogReminder.getWindow().setLayout(-1, -2);
                Button noBtn = (Button) dialogReminder.findViewById(R.id.btn_no);
                ((Button) dialogReminder.findViewById(R.id.btn_yes)).setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        if (Functions.isNetworkConnected(ChildDetailAdapter.this.context)) {
                            ChildDetailAdapter.this.deleteChild(childID);
                            ChildDetailAdapter.this.dbVaccination.deleteChild(Integer.valueOf(childID).intValue());
                            ChildDetailAdapter.this.childDetailList.remove(detailModel);
                            ChildDetailAdapter.this.notifyDataSetChanged();
                        } else {
                            Toast.makeText(ChildDetailAdapter.this.context, R.string.check_your_internet_connectivity, Toast.LENGTH_SHORT).show();
                        }
                        dialogReminder.dismiss();
                    }
                });
                noBtn.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        dialogReminder.dismiss();
                    }
                });
                dialogReminder.getWindow().setBackgroundDrawable(new ColorDrawable(0));
                dialogReminder.show();
            }
        });
        return view;
    }

    /* access modifiers changed from: private */
    public void deleteChild(final String childId) {
        new AsyncTask<String, String, String>() {
            /* access modifiers changed from: protected */
            public String doInBackground(String... params) {
                ChildDetailAdapter.this.object = new JSONObject();
                JSONArray jsonArray = new JSONArray();
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put(ShareConstants.WEB_DIALOG_PARAM_ID, childId);
                    jsonArray.put(jsonObject);
                    ChildDetailAdapter.this.object.put("child", jsonArray);
                    Log.e("response : ", ChildDetailAdapter.this.object.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            /* access modifiers changed from: protected */
            public void onPreExecute() {
                super.onPreExecute();
                ChildDetailAdapter.this.loading =new ProgressDialog(ChildDetailAdapter.this.context,R.style.DialogTheme);
                ChildDetailAdapter.this.loading.setMessage("Please wait...");
                ChildDetailAdapter.this.loading.setTitle("Uploading...");
                ChildDetailAdapter.this.loading.show( );
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(String s) {
                super.onPostExecute(s);
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(1, Constant.DELETE_CHILD, ChildDetailAdapter.this.object, new Listener<JSONObject>() {
                    public void onResponse(JSONObject response) {
                        try {
                            boolean status = response.getBoolean("status");
                            String msg = response.getString(NotificationCompat.CATEGORY_MESSAGE);
                            if (status) {
                                ChildDetailAdapter.this.loading.dismiss();
                                PrintStream printStream = System.out;
                                StringBuilder sb = new StringBuilder();
                                sb.append("Response :");
                                sb.append(response);
                                printStream.println(sb.toString());
                                ChildDetailAdapter.this.notifyDataSetChanged();
                                return;
                            }
                            Toast.makeText(ChildDetailAdapter.this.context, msg, Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        ChildDetailAdapter.this.loading.dismiss();
                        Toast.makeText(ChildDetailAdapter.this.context, error.getMessage().toString(), Toast.LENGTH_LONG).show();
                        error.printStackTrace();
                    }
                });
                AppController.getInstance().addToRequestQueue(jsonObjectRequest);
            }
        }.execute(new String[0]);
    }
}
