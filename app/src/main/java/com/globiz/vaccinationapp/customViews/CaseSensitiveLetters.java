package com.globiz.vaccinationapp.customViews;

public class CaseSensitiveLetters {
    public String caseSensitive(String myString){
//        String upperString = myString.substring(0, 1).toUpperCase() + myString.substring(1).toLowerCase();
//        return(upperString);

        String[] arr = myString.split(" ");
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < arr.length; i++) {
            sb.append(Character.toUpperCase(arr[i].charAt(0)))
                    .append(arr[i].substring(1)).append(" ");
        }
        return sb.toString().trim();
    }
}
