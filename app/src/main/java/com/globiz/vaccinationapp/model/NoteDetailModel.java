package com.globiz.vaccinationapp.model;

public class NoteDetailModel {
    private String child_first_name = "";
    private String child_last_name = "";
    private String complete_vacc_id = "";

    /* renamed from: id */
    private String id;
    private String note = "";
    private String parentId = "";
    private String parent_first_name = "";
    private String parent_last_name = "";
    private String vaccine_name = "";

    public NoteDetailModel() {
    }

    public NoteDetailModel(String id, String complete_vacc_id2, String parentId2, String note2, String child_first_name2, String child_last_name2, String vaccine_name2, String parent_first_name2, String parent_last_name2) {
        this.id = id;
        this.complete_vacc_id = complete_vacc_id2;
        this.parentId = parentId2;
        this.note = note2;
        this.child_first_name = child_first_name2;
        this.child_last_name = child_last_name2;
        this.vaccine_name = vaccine_name2;
        this.parent_first_name = parent_first_name2;
        this.parent_last_name = parent_last_name2;
    }
public NoteDetailModel(String id, String complete_vacc_id2, String parentId2,  String vaccine_name2,  String child_first_name2, String child_last_name2,String note2) {
    this.id = id;
    this.complete_vacc_id = complete_vacc_id2;
    this.parentId = parentId2;
    this.note = note2;
    this.child_first_name = child_first_name2;
    this.child_last_name = child_last_name2;
    this.vaccine_name = vaccine_name2;
}
    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComplete_vacc_id() {
        return this.complete_vacc_id;
    }

    public void setComplete_vacc_id(String complete_vacc_id2) {
        this.complete_vacc_id = complete_vacc_id2;
    }

    public String getParentId() {
        return this.parentId;
    }

    public void setParentId(String parentId2) {
        this.parentId = parentId2;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note2) {
        this.note = note2;
    }

    public String getChild_first_name() {
        return this.child_first_name;
    }

    public void setChild_first_name(String child_first_name2) {
        this.child_first_name = child_first_name2;
    }

    public String getChild_last_name() {
        return this.child_last_name;
    }

    public void setChild_last_name(String child_last_name2) {
        this.child_last_name = child_last_name2;
    }

    public String getVaccine_name() {
        return this.vaccine_name;
    }

    public void setVaccine_name(String vaccine_name2) {
        this.vaccine_name = vaccine_name2;
    }

    public String getParent_first_name() {
        return this.parent_first_name;
    }

    public void setParent_first_name(String parent_first_name2) {
        this.parent_first_name = parent_first_name2;
    }

    public String getParent_last_name() {
        return this.parent_last_name;
    }

    public void setParent_last_name(String parent_last_name2) {
        this.parent_last_name = parent_last_name2;
    }
}

