package com.globiz.vaccinationapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("id")
    @Expose

    /* renamed from: id */
    private String f49id;
    @SerializedName("image")
    @Expose
    private Object image;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("no_of_child")
    @Expose
    private String noOfChild;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("socialid")
    @Expose
    private Object socialid;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("token")
    @Expose
    private Object token;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("verify_status")
    @Expose
    private String verifyStatus;

    public String getId() {
        return this.f49id;
    }

    public void setId(String id) {
        this.f49id = id;
    }

    public String getFirstname() {
        return this.firstname;
    }

    public void setFirstname(String firstname2) {
        this.firstname = firstname2;
    }

    public String getLastname() {
        return this.lastname;
    }

    public void setLastname(String lastname2) {
        this.lastname = lastname2;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address2) {
        this.address = address2;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone2) {
        this.phone = phone2;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email2) {
        this.email = email2;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password2) {
        this.password = password2;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status2) {
        this.status = status2;
    }

    public String getVerifyStatus() {
        return this.verifyStatus;
    }

    public void setVerifyStatus(String verifyStatus2) {
        this.verifyStatus = verifyStatus2;
    }

    public String getUserType() {
        return this.userType;
    }

    public void setUserType(String userType2) {
        this.userType = userType2;
    }

    public String getNoOfChild() {
        return this.noOfChild;
    }

    public void setNoOfChild(String noOfChild2) {
        this.noOfChild = noOfChild2;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender2) {
        this.gender = gender2;
    }

    public String getDob() {
        return this.dob;
    }

    public void setDob(String dob2) {
        this.dob = dob2;
    }

    public Object getImage() {
        return this.image;
    }

    public void setImage(Object image2) {
        this.image = image2;
    }

    public Object getSocialid() {
        return this.socialid;
    }

    public void setSocialid(Object socialid2) {
        this.socialid = socialid2;
    }

    public Object getToken() {
        return this.token;
    }

    public void setToken(Object token2) {
        this.token = token2;
    }
}

