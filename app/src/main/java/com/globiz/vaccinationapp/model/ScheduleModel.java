package com.globiz.vaccinationapp.model;

public class ScheduleModel implements Comparable<ScheduleModel> {
    String child_id;
    String expire_date;

    /* renamed from: id */
    String id;
    String status;
    String vaccine_description;
    String vaccine_image;
    String vaccine_name;
    String vaccine_date;
    String child_name;

    public ScheduleModel() {
    }

    public ScheduleModel(String id, String expire_date2, String status2, String vaccine_name2, String child_id2, String vaccine_description2, String vaccine_image2,String vaccine_date2, String child_name2) {
        this.id = id;
        this.expire_date = expire_date2;
        this.status = status2;
        this.vaccine_name = vaccine_name2;
        this.child_id = child_id2;
        this.vaccine_description = vaccine_description2;
        this.vaccine_image = vaccine_image2;
        this.vaccine_date=vaccine_date2;
        this.child_name= child_name2;
    }

    public String getChild_name() {
        return child_name;
    }

    public void setChild_name(String child_name) {
        this.child_name = child_name;
    }

    public String getVaccine_date() {
        return vaccine_date;
    }

    public void setVaccine_date(String vaccine_date) {
        this.vaccine_date = vaccine_date;
    }

    public String getChild_id() {
        return this.child_id;
    }

    public void setChild_id(String child_id2) {
        this.child_id = child_id2;
    }

    public String getVaccine_description() {
        return this.vaccine_description;
    }

    public void setVaccine_description(String vaccine_description2) {
        this.vaccine_description = vaccine_description2;
    }

    public String getVaccine_name() {
        return this.vaccine_name;
    }

    public void setVaccine_name(String vaccine_name2) {
        this.vaccine_name = vaccine_name2;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status2) {
        this.status = status2;
    }

    public String getExpire_date() {
        return this.expire_date;
    }

    public void setExpire_date(String expire_date2) {
        this.expire_date = expire_date2;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVaccine_image() {
        return this.vaccine_image;
    }

    public void setVaccine_image(String vaccine_image2) {
        this.vaccine_image = vaccine_image2;
    }

    public int compareTo(ScheduleModel o) {
        if (getExpire_date() == null || o.getExpire_date() == null) {
            return 0;
        }
        return getExpire_date().compareTo(o.getExpire_date());
    }
}
