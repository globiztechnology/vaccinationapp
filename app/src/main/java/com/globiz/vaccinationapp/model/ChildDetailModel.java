package com.globiz.vaccinationapp.model;

public class ChildDetailModel {
    String aadhar;
    String dob;
    String first_name;
    String gender;
    String blood_group;
    String relation;

    /* renamed from: id */
    String id;
    String image;
    String last_name;
    String mother_name;
    String place_birth;

    public ChildDetailModel() {
    }

    public ChildDetailModel(String id, String first_name2, String last_name2, String image2, String dob2, String mother_name2, String place_birth2, String gender2, String aadhar2, String relation2, String blood_group2) {
        this.id = id;
        this.first_name = first_name2;
        this.last_name = last_name2;
        this.image = image2;
        this.dob = dob2;
        this.mother_name = mother_name2;
        this.place_birth = place_birth2;
        this.gender = gender2;
        this.aadhar = aadhar2;
        this.blood_group = blood_group2;
        this.relation = relation2;
    }

    public String getBlood_group() {
        return blood_group;
    }

    public void setBlood_group(String blood_group) {
        this.blood_group = blood_group;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getPlace_birth() {
        return this.place_birth;
    }

    public void setPlace_birth(String place_birth2) {
        this.place_birth = place_birth2;
    }

    public String getMother_name() {
        return this.mother_name;
    }

    public void setMother_name(String mother_name2) {
        this.mother_name = mother_name2;
    }

    public String getFirst_name() {
        return this.first_name;
    }

    public void setFirst_name(String first_name2) {
        this.first_name = first_name2;
    }

    public String getLast_name() {
        return this.last_name;
    }

    public void setLast_name(String last_name2) {
        this.last_name = last_name2;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender2) {
        this.gender = gender2;
    }

    public String getDob() {
        return this.dob;
    }

    public void setDob(String dob2) {
        this.dob = dob2;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image2) {
        this.image = image2;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAadhar() {
        return this.aadhar;
    }

    public void setAadhar(String aadhar2) {
        this.aadhar = aadhar2;
    }
}

