package com.globiz.vaccinationapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Child {
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("id")
    @Expose

    /* renamed from: id */
    private String f45id;
    @SerializedName("image")
    @Expose
    private Object image;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("mother_name")
    @Expose
    private String motherName;
    @SerializedName("parent_id")
    @Expose
    private String parentId;
    @SerializedName("place_birth")
    @Expose
    private String placeBirth;

    public Child() {
    }

    public Child(String id, String parentId2, String firstName2, String lastName2, String dob2, String gender2, Object image2, String motherName2, String placeBirth2) {
        this.f45id = id;
        this.parentId = parentId2;
        this.firstName = firstName2;
        this.lastName = lastName2;
        this.dob = dob2;
        this.gender = gender2;
        this.image = image2;
        this.motherName = motherName2;
        this.placeBirth = placeBirth2;
    }

    public String getId() {
        return this.f45id;
    }

    public void setId(String id) {
        this.f45id = id;
    }

    public String getParentId() {
        return this.parentId;
    }

    public void setParentId(String parentId2) {
        this.parentId = parentId2;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName2) {
        this.firstName = firstName2;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName2) {
        this.lastName = lastName2;
    }

    public String getDob() {
        return this.dob;
    }

    public void setDob(String dob2) {
        this.dob = dob2;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender2) {
        this.gender = gender2;
    }

    public Object getImage() {
        return this.image;
    }

    public void setImage(Object image2) {
        this.image = image2;
    }

    public String getMotherName() {
        return this.motherName;
    }

    public void setMotherName(String motherName2) {
        this.motherName = motherName2;
    }

    public String getPlaceBirth() {
        return this.placeBirth;
    }

    public void setPlaceBirth(String placeBirth2) {
        this.placeBirth = placeBirth2;
    }
}
