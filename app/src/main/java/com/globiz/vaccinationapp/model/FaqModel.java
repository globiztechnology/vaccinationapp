package com.globiz.vaccinationapp.model;

public class FaqModel {
    private String answer;
    private String question;

    public FaqModel(String ques, String ans) {
        this.question = ques;
        this.answer = ans;
    }

    public String getQuestion() {
        return this.question;
    }

    public String setQuestion(String question2) {
        this.question = question2;
        return question2;
    }

    public String getAnswer() {
        return this.answer;
    }

    public String setAnswer(String answer2) {
        this.answer = answer2;
        return answer2;
    }
}
