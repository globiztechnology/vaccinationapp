package com.globiz.vaccinationapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.globiz.vaccinationapp.model.ChildDetailModel;
import com.globiz.vaccinationapp.model.NoteDetailModel;
import com.globiz.vaccinationapp.model.ScheduleModel;

import java.util.ArrayList;

public class DBVaccination extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "DB_Vaccination";
    private static final int DATABASE_VERSION = 3;
    private static final String KEY_TC_AADHAR = "tc_aadhar";
    private static final String KEY_TC_BIRTH_PLACE = "tc_place_birth";
    private static final String KEY_TC_DOB = "tc_dob";
    private static final String KEY_TC_FIRST_NAME = "tc_first_name";
    private static final String KEY_TC_GENDER = "tc_gender";
    private static final String KEY_TC_ID = "tc_id";
    private static final String KEY_TC_IMAGE = "tc_image";
    private static final String KEY_TC_LAST_NAME = "tc_last_name";
    private static final String KEY_TC_MOTHER_NAME = "tc_mother_name";
    private static final String KEY_TC_PARENT_ID = "tc_parent_id";
    private static final String KEY_TN_CHILD_FIRST_NAME = "tn_child_first_name";
    private static final String KEY_TN_CHILD_LAST_NAME = "tn_child_last_name";
    private static final String KEY_TN_COMPLETE_VACC_ID = "tn_complete_vacc_id";
    private static final String KEY_TN_ID = "tn_id";
    private static final String KEY_TN_NOTE = "tn_note";
    private static final String KEY_TN_PARENT_ID = "tn_parent_id";
    private static final String KEY_TN_VACCINE_NAME = "tn_vaccine_name";
    private static final String KEY_TV_CHILD_ID = "tv_child_id";
    private static final String KEY_TV_CHILD_NAME = "tv_child_name";
    private static final String KEY_TV_VACCINE_DATE="tv_vaccine_date";
    private static final String KEY_TV_CUSTOM_VACC_ID = "tv_custom_vacc_id";
    private static final String KEY_TV_EXPIRE_DATE = "tv_expire_date";
    private static final String KEY_TV_ID = "tv_id";
    private static final String KEY_TV_STATUS = "tv_status";
    private static final String KEY_TV_VACCINE_DESCRIPTION = "tv_vaccine_description";
    private static final String KEY_TV_VACCINE_NAME = "tv_vaccine_name";
    private static final String KEY_TV_V_ID = "tv_v_id";
    private static final String TABLE_CHILDS = "Table_CHILDS";
    private static final String TABLE_NOTES = "Table_NOTES";
    private static final String TABLE_VACCINATION = "Table_Vaccination";

    public DBVaccination(Context context) {
        super(context, DATABASE_NAME, null, 3);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE Table_Vaccination(tv_id INTEGER,tv_expire_date DATETIME,tv_status INTERGER,tv_vaccine_name TEXT,tv_child_id INTEGER,tv_vaccine_description TEXT,tv_vaccine_date DATETIME )");
        db.execSQL("CREATE TABLE Table_CHILDS(tc_id INTEGER PRIMARY KEY,tc_first_name TEXT,tc_last_name TEXT,tc_image TEXT,tc_dob DATETIME, tc_mother_name TEXT, tc_place_birth TEXT,tc_gender TEXT,tc_aadhar TEXT )");
        db.execSQL("CREATE TABLE Table_NOTES(tn_id INTEGER PRIMARY KEY,tn_complete_vacc_id INTEGER,tn_parent_id INTEGER,tn_vaccine_name TEXT,tn_child_first_name TEXT,tn_child_last_name TEXT,tn_note TEXT )");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS Table_CHILDS");
        db.execSQL("DROP TABLE IF EXISTS Table_Vaccination");
        db.execSQL("DROP TABLE IF EXISTS Table_NOTES");
        onCreate(db);
    }

    public boolean checkIsChildAlreadyInDBorNot(String fieldValue) {
        SQLiteDatabase sqldb = getReadableDatabase();
        StringBuilder sb = new StringBuilder();
        sb.append("Select * from Table_CHILDS where tc_id = ");
        sb.append(fieldValue);
        Cursor cursor = sqldb.rawQuery(sb.toString(), null);
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }
    public boolean checkIsNoteAlreadyInDBorNot(String fieldValue) {
        SQLiteDatabase sqldb = getReadableDatabase();
        StringBuilder sb = new StringBuilder();
        sb.append("Select * from Table_NOTES where tn_id = ");
        sb.append(fieldValue);
        Cursor cursor = sqldb.rawQuery(sb.toString(), null);
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public boolean checkIsDataAlreadyInDBorNot(String fieldValue) {
        SQLiteDatabase sqldb = getReadableDatabase();
        StringBuilder sb = new StringBuilder();
        sb.append("Select * from Table_Vaccination where tv_id = ");
        sb.append(fieldValue);
        Cursor cursor = sqldb.rawQuery(sb.toString(), null);
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public void addVaccinations(ScheduleModel scheduleModel) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TV_ID, scheduleModel.getId());
        values.put(KEY_TV_EXPIRE_DATE, scheduleModel.getExpire_date());
        values.put(KEY_TV_STATUS, scheduleModel.getStatus());
        values.put(KEY_TV_VACCINE_NAME, scheduleModel.getVaccine_name());
        values.put(KEY_TV_CHILD_ID, scheduleModel.getChild_id());
        values.put(KEY_TV_VACCINE_DESCRIPTION, scheduleModel.getVaccine_description());
        values.put(KEY_TV_VACCINE_DATE, scheduleModel.getVaccine_date());
        db.insertWithOnConflict(TABLE_VACCINATION, null, values, 4);
        db.close();
    }

    public void addAllChilds(ChildDetailModel apps) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TC_ID, apps.getId());
        values.put(KEY_TC_FIRST_NAME, apps.getFirst_name());
        values.put(KEY_TC_LAST_NAME, apps.getLast_name());
        values.put(KEY_TC_IMAGE, apps.getImage());
        values.put(KEY_TC_DOB, apps.getDob());
        values.put(KEY_TC_MOTHER_NAME, apps.getMother_name());
        values.put(KEY_TC_BIRTH_PLACE, apps.getPlace_birth());
        values.put(KEY_TC_GENDER, apps.getGender());
        db.insert(TABLE_CHILDS, null, values);
        db.close();
    }

    public ArrayList<ScheduleModel> getVaccine(int id) {
        ArrayList<ScheduleModel> scheduleModelArrayList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        SQLiteDatabase sQLiteDatabase = db;
        Cursor cursor = sQLiteDatabase.query(TABLE_VACCINATION, new String[]{KEY_TV_ID, KEY_TV_EXPIRE_DATE, KEY_TV_STATUS, KEY_TV_VACCINE_NAME, KEY_TV_CHILD_ID, KEY_TV_VACCINE_DESCRIPTION,KEY_TV_VACCINE_DATE}, "tv_child_id=?", new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                ScheduleModel scheduleModel = new ScheduleModel();
                scheduleModel.setId(cursor.getString(0));
                scheduleModel.setExpire_date(cursor.getString(1));
                scheduleModel.setStatus(cursor.getString(2));
                scheduleModel.setVaccine_name(cursor.getString(3));
                scheduleModel.setChild_id(cursor.getString(4));
                scheduleModel.setVaccine_description(cursor.getString(5));
                scheduleModel.setVaccine_date(cursor.getString(6));
                scheduleModelArrayList.add(scheduleModel);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return scheduleModelArrayList;
    }

    public void addNote(NoteDetailModel noteDetailModel) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TN_ID, noteDetailModel.getId());
        values.put(KEY_TN_COMPLETE_VACC_ID, noteDetailModel.getComplete_vacc_id());
        values.put(KEY_TN_PARENT_ID, noteDetailModel.getParentId());
        values.put(KEY_TN_VACCINE_NAME, noteDetailModel.getVaccine_name());
        values.put(KEY_TN_CHILD_FIRST_NAME, noteDetailModel.getChild_first_name());
        values.put(KEY_TN_CHILD_LAST_NAME, noteDetailModel.getChild_last_name());
        values.put(KEY_TN_NOTE, noteDetailModel.getNote());
        db.insert(TABLE_NOTES, null, values);
        db.close();
    }

    public ArrayList<NoteDetailModel> getAllNotes() {
        ArrayList<NoteDetailModel> noteDetailModelArrayList = new ArrayList<>();
        Cursor cursor = getWritableDatabase().rawQuery("SELECT  * FROM Table_NOTES", null);
        if (cursor.moveToFirst()) {
            do {
                NoteDetailModel noteDetailModel = new NoteDetailModel();
                noteDetailModel.setId(cursor.getString(0));
                noteDetailModel.setComplete_vacc_id(cursor.getString(1));
                noteDetailModel.setParentId(cursor.getString(2));
                noteDetailModel.setVaccine_name(cursor.getString(3));
                noteDetailModel.setChild_first_name(cursor.getString(4));
                noteDetailModel.setChild_last_name(cursor.getString(5));
                noteDetailModel.setNote(cursor.getString(6));
                noteDetailModelArrayList.add(noteDetailModel);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return noteDetailModelArrayList;
    }

    public ArrayList<ChildDetailModel> getAllChilds() {
        ArrayList<ChildDetailModel> childArrayList = new ArrayList<>();
        Cursor cursor = getWritableDatabase().rawQuery("SELECT  * FROM Table_CHILDS", null);
        if (cursor.moveToFirst()) {
            do {
                ChildDetailModel child = new ChildDetailModel();
                child.setId(cursor.getString(0));
                child.setFirst_name(cursor.getString(1));
                child.setLast_name(cursor.getString(2));
                child.setImage(cursor.getString(3));
                child.setDob(cursor.getString(4));
                child.setMother_name(cursor.getString(5));
                child.setPlace_birth(cursor.getString(6));
                child.setGender(cursor.getString(7));
                child.setAadhar(cursor.getString(8));
                Log.e("k",cursor.getString(9));
                child.setRelation(cursor.getString(9));
                child.setBlood_group(cursor.getString(10));
                childArrayList.add(child);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return childArrayList;
    }

    public ArrayList<ScheduleModel> getAllTheSchedule() {
        ArrayList<ScheduleModel> scheduleModelArrayList = new ArrayList<>();
        Cursor cursor = getWritableDatabase().rawQuery("SELECT  * FROM Table_Vaccination", null);
        if (cursor.moveToFirst()) {
            do {
                ScheduleModel scheduleModel = new ScheduleModel();
                scheduleModel.setId(cursor.getString(0));
                scheduleModel.setExpire_date(cursor.getString(1));
                scheduleModel.setStatus(cursor.getString(2));
                scheduleModel.setVaccine_name(cursor.getString(3));
                scheduleModel.setChild_id(cursor.getString(4));
                scheduleModel.setVaccine_description(cursor.getString(5));
                scheduleModel.setVaccine_date(cursor.getString(6));
                scheduleModelArrayList.add(scheduleModel);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return scheduleModelArrayList;
    }
    public void deleteAllTables() {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_CHILDS, null, null);
        db.delete(TABLE_VACCINATION, null, null);
        db.delete(TABLE_NOTES, null, null);
        db.close();
    }

    public void deleteTables() {
        SQLiteDatabase db = getWritableDatabase();
       // db.execSQL("delete from "+TABLE_VACCINATION);
        db.delete(TABLE_VACCINATION, null, null);
        db.close();
    }

    public void deleteChild(int g_id) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_CHILDS, "tc_id = ?", new String[]{String.valueOf(g_id)});
        db.close();
    }

    public int getCount() {
        Cursor cursor = getReadableDatabase().rawQuery("SELECT  * FROM Table_CHILDS", null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }
}
